package com.islami.app.SalahContent;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.SwitchCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import Manage.GPS;
import Manage.PrayTime;
import com.islami.app.R;


public class SalahAlarmActivity extends Fragment {
    LocationManager locationManager;
    GPS gps;
    Location l;
    ArrayList<String>arrayList=new ArrayList<>();
    String Lang="arabic";
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    Typeface custom_font_arabic;
    TextView textring;
    Context context;
    View view;
    public SalahAlarmActivity()
    {

    }
    @Override
    public void onCreate( Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.activity_salah_alarm, container, false);
        context = view.getContext();
        textring=(TextView)view.findViewById(R.id.alarm_tune_salah);
        sharedPreferences=context.getSharedPreferences("Salah_Alarm", Context.MODE_PRIVATE);
     //   textring.setText(sharedPreferences.getString("alarmTune",""));
        custom_font_arabic= Typeface.createFromAsset(context.getAssets(), "fonts/GESSTwoLight.otf");
        locationManager= (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        gps=new GPS(locationManager);
        Log.v("testloc",gps.get_Latitude()+" "+gps.get_Longitude());
        setAlarmTimes();
        setSwitchAction();
    return view;
    }

    public void setSwitchAction()
    {
        android.support.v7.widget.SwitchCompat switchCompat1=(SwitchCompat)view.findViewById(R.id.switch_compat1);
        android.support.v7.widget.SwitchCompat switchCompat2=(SwitchCompat)view.findViewById(R.id.switch_compat2);
        android.support.v7.widget.SwitchCompat switchCompat3=(SwitchCompat)view.findViewById(R.id.switch_compat3);
        android.support.v7.widget.SwitchCompat switchCompat4=(SwitchCompat)view.findViewById(R.id.switch_compat4);
        android.support.v7.widget.SwitchCompat switchCompat5=(SwitchCompat)view.findViewById(R.id.switch_compat5);
        android.support.v7.widget.SwitchCompat switchCompatb1=(SwitchCompat)view.findViewById(R.id.switch_compatbefore1);
        android.support.v7.widget.SwitchCompat switchCompatb2=(SwitchCompat)view.findViewById(R.id.switch_compatbefore2);
        android.support.v7.widget.SwitchCompat switchCompatb3=(SwitchCompat)view.findViewById(R.id.switch_compatbefore3);
        android.support.v7.widget.SwitchCompat switchCompatb4=(SwitchCompat)view.findViewById(R.id.switch_compatbefore4);
        android.support.v7.widget.SwitchCompat switchCompatb5=(SwitchCompat)view.findViewById(R.id.switch_compatbefore5);

        sharedPreferences=context.getSharedPreferences("Salah_Alarm", Context.MODE_PRIVATE);
        editor=sharedPreferences.edit();
        switchCompat1.setChecked(sharedPreferences.getBoolean("fagr",false));
        switchCompat2.setChecked(sharedPreferences.getBoolean("dahr",false));
        switchCompat3.setChecked(sharedPreferences.getBoolean("asr",false));
        switchCompat4.setChecked(sharedPreferences.getBoolean("maghrb",false));
        switchCompat5.setChecked(sharedPreferences.getBoolean("isha",false));
        switchCompatb1.setChecked(sharedPreferences.getBoolean("fagr_b",false));
        switchCompatb2.setChecked(sharedPreferences.getBoolean("dahr_b",false));
        switchCompatb3.setChecked(sharedPreferences.getBoolean("asr_b",false));
        switchCompatb4.setChecked(sharedPreferences.getBoolean("maghrb_b",false));
        switchCompatb5.setChecked(sharedPreferences.getBoolean("isha_b",false));
        switchCompat1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                editor.putBoolean("fagr", isChecked);
                editor.commit();
            }
        });
        switchCompat2.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                editor.putBoolean("dahr", isChecked);
                editor.commit();
            }
        });

        switchCompat3.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                editor.putBoolean("asr", isChecked);
                editor.commit();
            }
        });

        switchCompat4.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                editor.putBoolean("maghrb", isChecked);
                editor.commit();
            }
        });

        switchCompat5.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                editor.putBoolean("isha", isChecked);
                editor.commit();
                Log.v("isha",isChecked+"");
            }
        });
        switchCompatb1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                editor.putBoolean("fagr_b", isChecked);
                editor.commit();
            }
        });
        switchCompatb2.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                editor.putBoolean("dahr_b", isChecked);
                editor.commit();
            }
        });
        switchCompatb3.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                editor.putBoolean("asr_b", isChecked);
                editor.commit();
            }
        });
        switchCompatb4.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                editor.putBoolean("maghrb_b", isChecked);
                editor.commit();
            }
        });

        switchCompatb5.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                editor.putBoolean("isha_b", isChecked);
                editor.commit();
            }
        });
    }
    public void setAlarmTimes()
    {
    TextView fagrtext=(TextView)view.findViewById(R.id.Azantext1);
    TextView dahrtext=(TextView)view.findViewById(R.id.Azantext2);
    TextView asrtext=(TextView)view.findViewById(R.id.Azantext3);
    TextView maghrbtext=(TextView)view.findViewById(R.id.Azantext4);
    TextView ashatext=(TextView)view.findViewById(R.id.Azantext5);
    TextView Alarm =(TextView)view.findViewById(R.id.alarm_tune_salah_text);

        TextView fagrtextbefore=(TextView)view.findViewById(R.id.Azantextbefore1);
        TextView dahrtextbefore=(TextView)view.findViewById(R.id.Azantextbefore2);
        TextView asrtextbefore=(TextView)view.findViewById(R.id.Azantextbefore3);
        TextView maghrbtextbefore=(TextView)view.findViewById(R.id.Azantextbefore4);
        TextView ashatextbefore=(TextView)view.findViewById(R.id.Azantextbefore5);
        TextView Alarmbefore=(TextView)view.findViewById(R.id.alarm_tune_salah_text_before);
    if(Lang.equals("english"))
    {
        fagrtext.setText("Daybreak");
        fagrtextbefore.setText("Daybreak");
        dahrtext.setText("Afternoon");
        dahrtextbefore.setText("Afternoon");
        asrtext.setText("Asr");
        asrtextbefore.setText("Asr");
        maghrbtext.setText("Maghreb");
        maghrbtextbefore.setText("Maghreb");
        ashatext.setText("Isha");
        ashatextbefore.setText("Isha");
        Alarm.setText("Alarm ");
        Alarmbefore.setText("Alarm");
    }
    else
    {
        fagrtextbefore.setTypeface(custom_font_arabic);
        dahrtextbefore.setTypeface(custom_font_arabic);
        asrtextbefore.setTypeface(custom_font_arabic);
        maghrbtextbefore.setTypeface(custom_font_arabic);
        ashatextbefore.setTypeface(custom_font_arabic);
        fagrtext.setTypeface(custom_font_arabic);
        dahrtext.setTypeface(custom_font_arabic);
        asrtext.setTypeface(custom_font_arabic);
        maghrbtext.setTypeface(custom_font_arabic);
        ashatext.setTypeface(custom_font_arabic);
        Alarm.setTypeface(custom_font_arabic);
    }
    TextView fagr=(TextView)view.findViewById(R.id.Azantime1);
    TextView dahr=(TextView)view.findViewById(R.id.Azantime2);
    TextView asr=(TextView)view.findViewById(R.id.Azantime3);
    TextView maghrb=(TextView)view.findViewById(R.id.Azantime4);
    TextView asha=(TextView)view.findViewById(R.id.Azantime5);
    double timezone = (Calendar.getInstance().getTimeZone()
            .getOffset(Calendar.getInstance().getTimeInMillis()))
            / (1000 * 60 * 60);
    PrayTime prayers = new PrayTime();

    prayers.setTimeFormat(prayers.Time12);
    prayers.setCalcMethod(prayers.Makkah);
    prayers.setAsrJuristic(prayers.Shafii);
    prayers.setAdjustHighLats(prayers.AngleBased);
    int[] offsets = { 0, 0, 0, 0, 0, 0, 0 }; // {Fajr,Sunrise,Dhuhr,Asr,Sunset,Maghrib,Isha}
    prayers.tune(offsets);

    Date now = new Date();
//        now.setDate(32);

    Log.v("Date1", now.toString());
    Calendar cal = Calendar.getInstance();
    cal.setTime(now);
    arrayList = prayers.getPrayerTimes(cal, gps.get_Latitude(),
            gps.get_Longitude(), timezone);
    ArrayList prayerNames = prayers.getTimeNames();
    fagr.setText(arrayList.get(0));
    dahr.setText(arrayList.get(2));
    asr.setText(arrayList.get(3));
    maghrb.setText(arrayList.get(5));
    asha.setText(arrayList.get(6));
}
   }
/*    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_salah_alarm);
        textring=(TextView)findViewById(R.id.alarm_tune_salah);
        sharedPreferences=getSharedPreferences("Salah_Alarm", Context.MODE_PRIVATE);
        textring.setText(sharedPreferences.getString("alarmTune",""));
        custom_font_arabic= Typeface.createFromAsset(getAssets(), "fonts/GESSTwoLight.otf");
        locationManager= (LocationManager) getApplicationContext().getSystemService(Context.LOCATION_SERVICE);
        gps=new GPS(locationManager);
        Log.v("testloc",gps.get_Latitude()+" "+gps.get_Longitude());
        setAlarmTimes();
        setSwitchAction();
    }*/
   /* public void SelectAudio(View view)
    {
        Intent intent;
        intent = new Intent();
        intent.setAction(Intent.ACTION_GET_CONTENT);
        intent.setType("audio/mpeg");
        startActivityForResult(Intent.createChooser(intent, "select alarm tune"), 1);
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1 && resultCode == Activity.RESULT_OK){
            if ((data != null) && (data.getData() != null)){
                Uri audioFileUri = data.getData();
                String path=audioFileUri.getPath();
                Cursor returnCursor =
                        context.getContentResolver().query(audioFileUri, null, null, null, null);

                int nameIndex = returnCursor.getColumnIndex(OpenableColumns.DISPLAY_NAME);
                returnCursor.moveToFirst();
                textring.setText(returnCursor.getString(nameIndex));
                sharedPreferences=context.getSharedPreferences("Salah_Alarm", Context.MODE_PRIVATE);
                editor=sharedPreferences.edit();
                editor.putString("alarmPath",path);
                editor.putString("alarmTune", returnCursor.getString(nameIndex));
                editor.commit();
                Log.v("test",path+" "+returnCursor.getString(nameIndex));
                audioPlayer(audioFileUri,returnCursor.getString(nameIndex));
                // Now you can use that Uri to get the file path, or upload it, ...
            }
        }
    }
    public void audioPlayer(Uri path, String fileName){
        //set up MediaPlayer
     //   Environment.getDataDirectory().getp
        try {
            MediaPlayer mp = new MediaPlayer();
            mp.setDataSource(this, path);
            mp.start();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }*/