package com.islami.app.elsomsection;

import org.joda.time.Chronology;
import org.joda.time.DateTime;
import org.joda.time.chrono.ISOChronology;
import org.joda.time.chrono.IslamicChronology;

/**
 * Created by FCI on 1/10/2016.
 */
public class ConverterDate {


    public  int GetGeoMonth(int HijriYear ,int Month , int day){
        DateTime t= DateTime.now();
        DateTime dtIslamic = t.withChronology(IslamicChronology.getInstance());
        Chronology iso = ISOChronology.getInstanceUTC();
        Chronology hijri = IslamicChronology.getInstanceUTC();
        DateTime dtHijri = new DateTime(HijriYear,Month,day,22,22,hijri);
        DateTime dtIso = new DateTime(dtHijri, iso);
        return  dtIso.getMonthOfYear();
    }

    public  int GetGeoDay(int HijriYear , int Month , int day){
        DateTime t= DateTime.now();
        DateTime dtIslamic = t.withChronology(IslamicChronology.getInstance());
        Chronology iso = ISOChronology.getInstanceUTC();
        Chronology hijri = IslamicChronology.getInstanceUTC();
        DateTime dtHijri = new DateTime(HijriYear,Month,day,22,22,hijri);
        DateTime dtIso = new DateTime(dtHijri, iso);
        return  dtIso.getDayOfMonth();
    }
    public  int GetGeoYear(int HijriYear , int Month , int day ){
        DateTime t= DateTime.now();
        DateTime dtIslamic = t.withChronology(IslamicChronology.getInstance());
        Chronology iso = ISOChronology.getInstanceUTC();
        Chronology hijri = IslamicChronology.getInstanceUTC();
        DateTime dtHijri = new DateTime(HijriYear,Month,day,22,22,hijri);
        DateTime dtIso = new DateTime(dtHijri, iso);
        return  dtIso.getYear();
    }

    public int GetHijriYear(int GeoYear){
        DateTime dtISO = new DateTime(GeoYear, 1, 1, 12, 0, 0, 0);
        DateTime dtIslamic = dtISO.withChronology(IslamicChronology.getInstance());
        return  dtIslamic.getYear();
    }
}
