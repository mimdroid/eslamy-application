package com.islami.app.ZakahContent;

import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.joda.time.Chronology;
import org.joda.time.LocalDate;
import org.joda.time.chrono.ISOChronology;
import org.joda.time.chrono.IslamicChronology;

import java.util.Calendar;
import java.util.Date;

import DataBase.DBInterface;
import com.islami.app.R;


public class Zakah_Recording_Statistics extends Fragment {
    TextView english_date;
    TextView arabic_date;
    EditText value;
    Calendar calendar=Calendar.getInstance();
    Date now=new Date();
    int dayOfMonth;
    int indexOfDay;
    ArabicCalendar arabicCalendar;
    GridView gridView;
    Typeface custom_font_arabic;
    String[] arabic_G_Months={"يناير","فبراير","مارس","ابرايل","مايو","يونيو","يوليو","أغسطس","ستمبر","أكتوبر","نوفمبر","ديسمبر"};
    String[] english_G_Months={"Jun","Feb","March","April","May","June","July","August","September","October","November","Dec"};
    String[] arabic_H_Months={"محرم","صفر","ربيع الأول","ربيع الثاني","جمادي الأول","جمادي الثاني","رجب","شعبان","رمضان","شوال","ذو القعدة","ذو الحجة"};
    String[] english_H_Months={"MuHarram","Safar","Raby` al-awal","Raby` al-THaany","Jumaada al-awal","Jumaada al-THaany","Rajab","SHa`baan","RamaDHaan","SHawwal","Thul Qa`dah","Thul Hijjah"};
    Chronology hijri;
    Chronology iso;
    int indexOfZakahType=0;
    Context context;
    View view;
    public Zakah_Recording_Statistics()
    {
    }
   /* @Override
    public void onCreate( Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }*/

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.activity_zakah__recording__statistics, container, false);
        context = view.getContext();

        return view;
    }
    @Override
    public void onResume() {
        super.onResume();
        custom_font_arabic= Typeface.createFromAsset(context.getAssets(), "fonts/GESSTwoLight.otf");
        english_date=(TextView)view.findViewById(R.id.english_date);
        arabic_date=(TextView)view.findViewById(R.id.arabic_date);
        value=(EditText)view.findViewById(R.id.zakah_cost);
        english_date.setTypeface(custom_font_arabic);
        arabic_date.setTypeface(custom_font_arabic);
        value.setTypeface(custom_font_arabic);
        now.setDate(1);
        now.setMonth(calendar.get(Calendar.MONTH));
        english_date.setText(now.getDate()+"   "+arabic_G_Months[now.getMonth()]+"  "+(now.getYear()+1900));
        dayOfMonth=calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
        indexOfDay=now.getDay();
        int day=now.getDate();
        int m=now.getMonth();
        int y=now.getYear()+1900;
        iso = ISOChronology.getInstanceUTC();
        hijri = IslamicChronology.getInstanceUTC();
        LocalDate todayIso = new LocalDate(now.getYear()+1900, now.getMonth()+1, now.getDate(), iso);
        LocalDate todayHijri = new LocalDate(todayIso.toDateTimeAtStartOfDay(),
                hijri);
        int arabicDay=todayHijri.getDayOfMonth();
        int arabicMonth=todayHijri.getMonthOfYear();
        int arabicYear=todayHijri.getYear();
        arabic_date.setText(arabicDay+"  "+arabic_H_Months[arabicMonth]+"   "+arabicYear);
        arabicCalendar=new ArabicCalendar(context,indexOfDay,dayOfMonth,english_date,arabic_date,day,m,y);
        gridView = (GridView)view.findViewById(R.id.grid_view);
        gridView.setAdapter(arabicCalendar);
        Spinner spinner_environment=(Spinner)view.findViewById(R.id.zakah_type);
        ArrayAdapter adapter = ArrayAdapter.createFromResource(context, R.array.spinner_array_zakah_type_arabic, R.layout.spinner_phone);
        adapter.setDropDownViewResource(R.layout.dropitem);
        spinner_environment.setAdapter(adapter);

        spinner_environment.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                indexOfZakahType=position;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        Call();
    }
    /*@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_zakah__recording__statistics);
        custom_font_arabic= Typeface.createFromAsset(getAssets(), "fonts/GESSTwoLight.otf");
        english_date=(TextView)findViewById(R.id.english_date);
        arabic_date=(TextView)findViewById(R.id.arabic_date);
        value=(EditText)findViewById(R.id.zakah_cost);
        english_date.setTypeface(custom_font_arabic);
        arabic_date.setTypeface(custom_font_arabic);
        value.setTypeface(custom_font_arabic);
        now.setDate(1);
        now.setMonth(calendar.get(Calendar.MONTH));
        english_date.setText(now.getDate()+"   "+arabic_G_Months[now.getMonth()]+"  "+(now.getYear()+1900));
        dayOfMonth=calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
        indexOfDay=now.getDay();
        int day=now.getDate();
        int m=now.getMonth();
        int y=now.getYear()+1900;
        iso = ISOChronology.getInstanceUTC();
        hijri = IslamicChronology.getInstanceUTC();
        LocalDate todayIso = new LocalDate(now.getYear()+1900, now.getMonth()+1, now.getDate(), iso);
        LocalDate todayHijri = new LocalDate(todayIso.toDateTimeAtStartOfDay(),
                hijri);
        int arabicDay=todayHijri.getDayOfMonth();
        int arabicMonth=todayHijri.getMonthOfYear();
        int arabicYear=todayHijri.getYear();
        arabic_date.setText(arabicDay+"  "+arabic_H_Months[arabicMonth]+"   "+arabicYear);
        arabicCalendar=new ArabicCalendar(this,indexOfDay,dayOfMonth,english_date,arabic_date,day,m,y);
        gridView = (GridView) findViewById(R.id.grid_view);
        gridView.setAdapter(arabicCalendar);
        Spinner spinner_environment=(Spinner)findViewById(R.id.zakah_type);
        ArrayAdapter adapter = ArrayAdapter.createFromResource(this, R.array.spinner_array_zakah_type_arabic, R.layout.spinner_phone);
        adapter.setDropDownViewResource(R.layout.dropitem);
        spinner_environment.setAdapter(adapter);

        spinner_environment.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                indexOfZakahType=position;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }*/
    public void Call()
    {
        final ImageButton NextMonth=(ImageButton)view.findViewById(R.id.NextMonth);
        ImageButton PrevMonth=(ImageButton)view.findViewById(R.id.PrevMonth);
        Button Save=(Button)view.findViewById(R.id.Save);
        NextMonth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NextMonth();
            }
        });
        PrevMonth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PrevMonth();
            }
        });
        Save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Save();
            }
        });
    }
    public void NextMonth()
    {
        now.setMonth(now.getMonth() + 1);
        now.setDate(1);
        calendar.set(Calendar.MONTH,now.getMonth());
        dayOfMonth=calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
        indexOfDay=now.getDay();
        int day=now.getMonth();
        int m=now.getMonth();
        int y=now.getYear()+1900;
        iso = ISOChronology.getInstanceUTC();
        hijri = IslamicChronology.getInstanceUTC();
        LocalDate todayIso = new LocalDate(now.getYear()+1900, now.getMonth()+1, now.getDate(), iso);
        LocalDate todayHijri = new LocalDate(todayIso.toDateTimeAtStartOfDay(),
                hijri);
        int arabicDay=todayHijri.getDayOfMonth();
        int arabicMonth=(todayHijri.getMonthOfYear())%12;
        int arabicYear=todayHijri.getYear();
        arabic_date.setText(arabicDay+"  "+arabic_H_Months[arabicMonth]+"   "+arabicYear);
        english_date.setText(now.getDate()+"   "+arabic_G_Months[now.getMonth()]+"  "+(now.getYear()+1900));
        arabicCalendar=new ArabicCalendar(context,indexOfDay,dayOfMonth,english_date,arabic_date,day,m,y);
        gridView.setAdapter(arabicCalendar);
        Log.v("testday",indexOfDay+"");
    }
    public void PrevMonth()
    {
        now.setMonth((now.getMonth() - 1));
        now.setDate(1);
        calendar.set(Calendar.MONTH,now.getMonth());
        dayOfMonth=calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
        indexOfDay=now.getDay();
        int day=now.getMonth();
        int m=now.getMonth();
        int y=now.getYear()+1900;
        iso = ISOChronology.getInstanceUTC();
        hijri = IslamicChronology.getInstanceUTC();
        LocalDate todayIso = new LocalDate(now.getYear()+1900, now.getMonth()+1, now.getDate(), iso);
        LocalDate todayHijri = new LocalDate(todayIso.toDateTimeAtStartOfDay(),
                hijri);
        int arabicDay=todayHijri.getDayOfMonth();
        int arabicMonth=(todayHijri.getMonthOfYear())%12;
        int arabicYear=todayHijri.getYear();
        arabic_date.setText(arabicDay+"  "+arabic_H_Months[arabicMonth]+"   "+arabicYear);
        english_date.setText(now.getDate()+"   "+arabic_G_Months[now.getMonth()]+"  "+(now.getYear()+1900));
        arabicCalendar=new ArabicCalendar(context,indexOfDay,dayOfMonth,english_date,arabic_date,day,m,y);
        gridView.setAdapter(arabicCalendar);
        Log.v("testday",indexOfDay+"");
    }

    public void Save()
    {
         double valueDouble=Double.valueOf(value.getText().toString());
        if(valueDouble==0)
        {
            Toast.makeText(context,"من فضلك ادخل قيمة الزكاة",Toast.LENGTH_LONG).show();
        }
        else
        {
            DBInterface dbInterface=new DBInterface(context);
            Log.v("test", dbInterface.InsertZakah(valueDouble, indexOfZakahType, now.getYear() + 1900) + "");
        }
    }
}
