package com.islami.app.ZakahContent;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import Manage.Reader;
import com.islami.app.R;


public class ZakahCalculator extends Fragment {
    Typeface custom_font_arabic;
    Typeface custom_font_english;
    String Lang="arabic";
    TextView zakah_blessings;
    ImageView GoldZakah,SilverZakah,MoneyZakah,GattleZakah,CropsZakah;
    Context context;

    public ZakahCalculator()
    {

    }
    @Override
    public void onCreate( Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.activity_zakah_calculator, container, false);
        context=view.getContext();
        custom_font_arabic= Typeface.createFromAsset(context.getAssets(), "fonts/GESSTwoLight.otf");
        zakah_blessings=(TextView)view.findViewById(R.id.zakah_blessings);
        Lang="english";
        Reader reader=new Reader(context);
        if(Lang.equals("english"))
        {
            zakah_blessings.setText(reader.readBlessingEnglish());
        }
        else
        {

            zakah_blessings.setTypeface(custom_font_arabic);
            zakah_blessings.setText(reader.readBlessingsArabic());
        }
        CropsZakah=(ImageView)view.findViewById(R.id.CropsZakah);
        MoneyZakah=(ImageView)view.findViewById(R.id.MoneyZakah);
        GattleZakah=(ImageView)view.findViewById(R.id.GattleZakah);
        GoldZakah=(ImageView)view.findViewById(R.id.GoldZakah);
        SilverZakah=(ImageView)view.findViewById(R.id.SilverZakah);
        Call();
        return view;
    }
public void Call()
{
    CropsZakah.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent i=new Intent(context, CropsZakah.class);
            startActivity(i);
        }
    });
    MoneyZakah.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent i=new Intent(context, MoneyZakah.class);
            startActivity(i);
        }
    });
    SilverZakah.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent i=new Intent(context, SilverZakah.class);
            startActivity(i);
        }
    });
    GattleZakah.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent i=new Intent(context, CamelsZakah.class);
            startActivity(i);
        }
    });
    GoldZakah.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent i=new Intent(context,GoldZakah.class);
                    startActivity(i);
        }
    });
}
   /* @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_zakah_calculator);
        custom_font_arabic=Typeface.createFromAsset(getAssets(),  "fonts/GESSTwoLight.otf");
     //   custom_font_english=Typeface.createFromAsset(getAssets(),  "fonts/font.TTF");
        zakah_blessings=(TextView)findViewById(R.id.zakah_blessings);
        Lang="english";
        Reader reader=new Reader(this);
        if(Lang.equals("english"))
        {
            zakah_blessings.setTypeface(custom_font_english);
            zakah_blessings.setText(reader.readBlessingEnglish());
        }
        else
        {

            zakah_blessings.setTypeface(custom_font_arabic);
            zakah_blessings.setText(reader.readBlessingsArabic());
        }
    }
*/
    /*
    public void MoneyZakah(View view)
    {
        Intent intent=new Intent(this,MoneyZakah.class);
        startActivity(intent);
    }
    public void SilverZakah(View view)
    {
        Intent intent=new Intent(this,SilverZakah.class);
        startActivity(intent);
    }
    public void GoldZakah(View view)
    {
        Intent intent=new Intent(this,GoldZakah.class);
        startActivity(intent);
    }

    public void CropsZakah(View view)
    {
        Intent intent=new Intent(this,CropsZakah.class);
        startActivity(intent);
    }
    public void GattleZakah (View view)
    {
        Intent intent=new Intent(this,CamelsZakah.class);
        startActivity(intent);
    }
    */
}
