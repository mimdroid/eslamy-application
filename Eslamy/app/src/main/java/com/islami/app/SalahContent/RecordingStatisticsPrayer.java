package com.islami.app.SalahContent;

import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import org.joda.time.Chronology;
import org.joda.time.LocalDate;
import org.joda.time.chrono.ISOChronology;
import org.joda.time.chrono.IslamicChronology;

import java.util.Date;

import DataBase.DBInterface;
import com.islami.app.R;


public class RecordingStatisticsPrayer extends Fragment {
    Typeface custom_font_arabic;
    String Lang="english";
    char[] arabicNumbers = {'٠','١','٢','٣','٤','٥','٦','٧','٨','٩'};
    String[] arabic_G_Months={"يناير","فبراير","مارس","ابرايل","مايو","يونيو","يوليو","أغسطس","ستمبر","أكتوبر","نوفمبر","ديسمبر"};
    String[] english_G_Months={"Jun","Feb","March","April","May","June","July","August","September","October","November","Dec"};
    String[] arabic_H_Months={"محرم","صفر","ربيع الأول","ربيع الثاني","جمادي الأول","جمادي الثاني","رجب","شعبان","رمضان","شوال","ذو القعدة","ذو الحجة"};
    String[] english_H_Months={"MuHarram","Safar","Raby` al-awal","Raby` al-THaany","Jumaada al-awal","Jumaada al-THaany","Rajab","SHa`baan","RamaDHaan","SHawwal","Thul Qa`dah","Thul Hijjah"};
    Chronology hijri;
    Chronology iso;
    Date now=new Date();
    int fagrInt=0,dahrInt=0,asrInt=0,maghrbInt=0,ishaInt=0;
    Context context;
    View view;
    public RecordingStatisticsPrayer()
    {
    }
    @Override
    public void onCreate( Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.recording_statistics_prayer, container, false);
        context = view.getContext();
        custom_font_arabic= Typeface.createFromAsset(context.getAssets(), "fonts/GESSTwoLight.otf");
        iso = ISOChronology.getInstanceUTC();
        hijri = IslamicChronology.getInstanceUTC();
        LocalDate todayIso = new LocalDate(now.getYear()+1900, now.getMonth()+1, now.getDate(), iso);
        LocalDate todayHijri = new LocalDate(todayIso.toDateTimeAtStartOfDay(),
                hijri);
        Log.v("test", todayHijri.toString());
        if(Lang.equals("arabic"))
        {
            arabicDate();
        }
        else
        {
            englishDate();
        }
        changeLange();
        Call();
        return view;
    }
        /*@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.recording_statistics_prayer);
        custom_font_arabic= Typeface.createFromAsset(getAssets(), "fonts/GESSTwoLight.otf");
         iso = ISOChronology.getInstanceUTC();
         hijri = IslamicChronology.getInstanceUTC();
        LocalDate todayIso = new LocalDate(now.getYear()+1900, now.getMonth()+1, now.getDate(), iso);
        LocalDate todayHijri = new LocalDate(todayIso.toDateTimeAtStartOfDay(),
                hijri);
        Log.v("test", todayHijri.toString());
        if(Lang.equals("arabic"))
        {
            arabicDate();
        }
        else
        {
            englishDate();
        }
        changeLange();

    }*/
    public void arabicDate()
    {
        LocalDate todayIso = new LocalDate(now.getYear()+1900, now.getMonth()+1, now.getDate(), iso);
        LocalDate todayHijri = new LocalDate(todayIso.toDateTimeAtStartOfDay(),
                hijri);
        int arabicDay=todayHijri.getDayOfMonth();
        int arabicMonth=todayHijri.getMonthOfYear();
        int arabicYear=todayHijri.getYear();
        String sArabicDay=String.valueOf(arabicDay);
        String sArabicYear=String.valueOf(arabicYear);
        String sArabicMonth=arabic_H_Months[arabicMonth-1];
        String stArabicMonth=arabic_G_Months[now.getMonth()];
        String stArabicDay=String.valueOf(now.getDate());
        String stArabicYear=String.valueOf(now.getYear()+1900);
        int count=0;
        for(char i='0';i<='9';i++)
        {
            sArabicDay=sArabicDay.replace(i,arabicNumbers[count]);
            sArabicYear=sArabicYear.replace(i,arabicNumbers[count]);
            stArabicDay=stArabicDay.replace(i,arabicNumbers[count]);
            stArabicYear=stArabicYear.replace(i,arabicNumbers[count]);
            Log.v("test","١١١١١١١١١١١١"+i);
            count++;
        }
        TextView hijriDate=(TextView)view.findViewById(R.id.hijriDate);
        TextView month_r=(TextView)view.findViewById(R.id.month_r);
        TextView day_r=(TextView)view.findViewById(R.id.day_r);
        TextView year_r=(TextView)view.findViewById(R.id.year_r);
        String r=sArabicDay+" "+sArabicMonth+" "+sArabicYear;
        hijriDate.setText(r);
        month_r.setText(stArabicMonth);
        year_r.setText(stArabicYear);
        day_r.setText(stArabicDay);
        Log.v("test",r);
    }

    public void englishDate()
    {
        LocalDate todayIso = new LocalDate(now.getYear()+1900, now.getMonth()+1, now.getDate(), iso);
        LocalDate todayHijri = new LocalDate(todayIso.toDateTimeAtStartOfDay(),
                hijri);
        int arabicDay=todayHijri.getDayOfMonth();
        int arabicMonth=todayHijri.getMonthOfYear();
        int arabicYear=todayHijri.getYear();
        String sArabicDay=String.valueOf(arabicDay);
        String sArabicYear=String.valueOf(arabicYear);
        String sArabicMonth=english_H_Months[arabicMonth-1];
        String stArabicMonth=english_G_Months[now.getMonth()];
        String stArabicDay=String.valueOf(now.getDate());
        String stArabicYear=String.valueOf(now.getYear()+1900);
        int count=0;
        TextView hijriDate=(TextView)view.findViewById(R.id.hijriDate);
        TextView month_r=(TextView)view.findViewById(R.id.month_r);
        TextView day_r=(TextView)view.findViewById(R.id.day_r);
        TextView year_r=(TextView)view.findViewById(R.id.year_r);
        String r=sArabicDay+" "+sArabicMonth+" "+sArabicYear;
        hijriDate.setText(r);
        month_r.setText(stArabicMonth);
        year_r.setText(stArabicYear);
        day_r.setText(stArabicDay);
        Log.v("test",r);
    }
    public void Save()
    {
        EditText sann=(EditText)view.findViewById(R.id.fagr_Before);
        int fagrs=0,dahrs=0,asrs=0,maghrbs=0,ishas=0;
        if(sann.getText().toString().isEmpty()==false)
        {
            fagrs+=Integer.valueOf(sann.getText().toString());

        }
        sann=(EditText)view.findViewById(R.id.dahrAfter);
        if(sann.getText().toString().isEmpty()==false)
        {
            dahrs+=Integer.valueOf(sann.getText().toString());

        }
        sann=(EditText)view.findViewById(R.id.dahrBefore);
        if(sann.getText().toString().isEmpty()==false)
        {
            dahrs+=Integer.valueOf(sann.getText().toString());

        }
        sann=(EditText)view.findViewById(R.id.maghrbAfter);
        if(sann.getText().toString().isEmpty()==false)
        {
            maghrbs+=Integer.valueOf(sann.getText().toString());

        }
        sann=(EditText)view.findViewById(R.id.maghrbBefore);
        if(sann.getText().toString().isEmpty()==false)
        {
            maghrbs+=Integer.valueOf(sann.getText().toString());

        }
        sann=(EditText)view.findViewById(R.id.IshaBefore);
        if(sann.getText().toString().isEmpty()==false)
        {
            ishas+=Integer.valueOf(sann.getText().toString());

        }sann=(EditText)view.findViewById(R.id.IshaAfter);
        if(sann.getText().toString().isEmpty()==false)
        {
            ishas+=Integer.valueOf(sann.getText().toString());

        }
        EditText editText=(EditText)view.findViewById(R.id.Addoha);
        EditText editText1=(EditText)view.findViewById(R.id.watr);
        int addoha=0;
        if(editText.getText().toString().isEmpty()==false)
        {
            addoha+=Integer.valueOf(editText.getText().toString());
        }
        int watr=0;
        if(editText1.getText().toString().isEmpty()==false)
        {
            watr+=Integer.valueOf(editText1.getText().toString());
        }
        DBInterface dbInterface=new DBInterface(context);
        Log.v("test",dbInterface.InsertNawafl(addoha,watr,convertDateToDay())+"");
        Log.v("test",dbInterface.InsertSalahSann(fagrs,dahrs,maghrbs,ishas,convertDateToDay())+"");
        Log.v("test", dbInterface.InsertSalahFard(fagrInt, dahrInt, asrInt, maghrbInt, ishaInt, convertDateToDay()) + "");
    }
    public long convertDateToDay()
    {
        return now.getDate()+((now.getMonth()+1)*30)+((now.getYear()+1900)*365);
    }
    public void Call()
    {
        Button Save=(Button)view.findViewById(R.id.save);
        Save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Save();
            }
        });
        ImageView PervDay=(ImageView)view.findViewById(R.id.PervDay);
        PervDay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PervDay();
            }
        });
        ImageView NextDay=(ImageView)view.findViewById(R.id.NextDay);
        NextDay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NextDay();
            }
        });
        ImageView IshaInMosque=(ImageView)view.findViewById(R.id.IshaInMosque);
        ImageView IshaOutMosque=(ImageView)view.findViewById(R.id.IshaOutMosque);
        ImageView fagrInMosque=(ImageView)view.findViewById(R.id.fagrInMosque);
        ImageView fagrOutMosque=(ImageView)view.findViewById(R.id.fagrOutMosque);
        ImageView dahrInMosque=(ImageView)view.findViewById(R.id.dahrInMosque);
        ImageView dahrOutMosque=(ImageView)view.findViewById(R.id.dahrOutMosque);
        ImageView asrInMosque=(ImageView)view.findViewById(R.id.asrInMosque);
        ImageView asrOutMosque=(ImageView)view.findViewById(R.id.asrOutMosque);
        ImageView maghrbInMosque=(ImageView)view.findViewById(R.id.maghrbInMosque);
        ImageView maghrbOutMosque=(ImageView)view.findViewById(R.id.maghrbOutMosque);
        IshaInMosque.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                IshaInMosque();
            }
        });
        IshaOutMosque.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                IshaOutMosque();
            }
        });
        maghrbInMosque.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            maghrbInMosque();
        }
    });
        maghrbOutMosque.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            maghrbOutMosque();
        }
    });
        asrInMosque.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                asrInMosque();
            }
        });
        asrOutMosque.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                asrOutMosque();
            }
        });
        dahrInMosque.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dahrInMosque();
            }
        });
        dahrOutMosque.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dahrOutMosque();
            }
        });
        fagrInMosque.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fagrInMosque();
            }
        });
        fagrOutMosque.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fagrOutMosque();
            }
        });
    }
    public void changeLange()
    {
        Button save=(Button)view.findViewById(R.id.save);
        TextView textView1=(TextView)view.findViewById(R.id.text_s_r_s1);
        TextView textView2=(TextView)view.findViewById(R.id.text_s_r_s2);
        TextView textView3=(TextView)view.findViewById(R.id.text_s_r_s3);
        TextView textView4=(TextView)view.findViewById(R.id.text_s_r_s4);
        TextView textView5=(TextView)view.findViewById(R.id.text_s_r_s5);
        TextView textView6=(TextView)view.findViewById(R.id.text_s_r_s6);
        TextView textView7=(TextView)view.findViewById(R.id.text_s_r_s7);
        TextView textView8=(TextView)view.findViewById(R.id.text_s_r_s8);
        TextView textView9=(TextView)view.findViewById(R.id.text_s_r_s9);
        TextView textView10=(TextView)view.findViewById(R.id.text_s_r_s10);
        TextView textView11=(TextView)view.findViewById(R.id.text_s_r_s11);
        TextView textView12=(TextView)view.findViewById(R.id.text_s_r_s12);
        if(Lang.equals("arabic"))
        {
            textView1.setTypeface(custom_font_arabic);
            textView2.setTypeface(custom_font_arabic);
            textView3.setTypeface(custom_font_arabic);
            textView4.setTypeface(custom_font_arabic);
            textView5.setTypeface(custom_font_arabic);
            textView6.setTypeface(custom_font_arabic);
            textView7.setTypeface(custom_font_arabic);
            textView8.setTypeface(custom_font_arabic);
            textView9.setTypeface(custom_font_arabic);
            textView10.setTypeface(custom_font_arabic);
            textView11.setTypeface(custom_font_arabic);
            textView12.setTypeface(custom_font_arabic);
            save.setTypeface(custom_font_arabic);
        }
        else
        {
            save.setText("Save");
            textView1.setText("Prayer");
            textView2.setText("fagr");
            textView3.setText("Dohha");
            textView4.setText("dahr");
            textView5.setText("asr");
            textView6.setText("maghrb");
            textView7.setText("Isha");
            textView8.setText("watr");
            textView9.setText("In \n mosque");
            textView10.setText("Out");
            textView11.setText("Sunan \n after ");
            textView12.setText("Sunan \n Before");
        }

    }
    public void PervDay()
    {
        now.setDate(now.getDate()-1);
        if(Lang.equals("arabic"))
        {
            arabicDate();
        }
        else
        {
            englishDate();
        }
    }
    public void NextDay()
    {

        now.setDate(now.getDate()+1);
        if(Lang.equals("arabic"))
        {
            arabicDate();
        }
        else
        {
            englishDate();
        }
    }
    public void IshaInMosque()
    {
        ImageView IshaInMosque=(ImageView)view.findViewById(R.id.IshaInMosque);
        ImageView IshaOutMosque=(ImageView)view.findViewById(R.id.IshaOutMosque);
        IshaOutMosque.setImageResource(R.drawable.circle);
        IshaInMosque.setImageResource(R.drawable.circleselected);
        ishaInt=2;
    }
    public void IshaOutMosque()
    {
        ImageView IshaInMosque=(ImageView)view.findViewById(R.id.IshaInMosque);
        ImageView IshaOutMosque=(ImageView)view.findViewById(R.id.IshaOutMosque);
        IshaOutMosque.setImageResource(R.drawable.circleselected);
        IshaInMosque.setImageResource(R.drawable.circle);
        ishaInt=1;
    }
    public void maghrbOutMosque()
    {

        ImageView In=(ImageView)view.findViewById(R.id.maghrbInMosque);
        ImageView Out=(ImageView)view.findViewById(R.id.maghrbOutMosque);
        In.setImageResource(R.drawable.circle);
        Out.setImageResource(R.drawable.circleselected);
        maghrbInt=1;
    }
    public void maghrbInMosque()
    {
        maghrbInt=2;
        ImageView In=(ImageView)view.findViewById(R.id.maghrbInMosque);
        ImageView Out=(ImageView)view.findViewById(R.id.maghrbOutMosque);
        In.setImageResource(R.drawable.circleselected);
        Out.setImageResource(R.drawable.circle);

    }
    public void asrOutMosque()
    {
        asrInt=1;
        ImageView In=(ImageView)view.findViewById(R.id.asrInMosque);
        ImageView Out=(ImageView)view.findViewById(R.id.asrOutMosque);
        In.setImageResource(R.drawable.circle);
        Out.setImageResource(R.drawable.circleselected);
    }
    public void asrInMosque()
    {
        asrInt=2;
        ImageView In=(ImageView)view.findViewById(R.id.asrInMosque);
        ImageView Out=(ImageView)view.findViewById(R.id.asrOutMosque);
        In.setImageResource(R.drawable.circleselected);
        Out.setImageResource(R.drawable.circle);
    }
    public void dahrOutMosque()
    {
        dahrInt=1;
        ImageView In=(ImageView)view.findViewById(R.id.dahrInMosque);
        ImageView Out=(ImageView)view.findViewById(R.id.dahrOutMosque);
        In.setImageResource(R.drawable.circle);
        Out.setImageResource(R.drawable.circleselected);
    }
    public void dahrInMosque()
    {
        dahrInt=2;
        ImageView In=(ImageView)view.findViewById(R.id.dahrInMosque);
        ImageView Out=(ImageView)view.findViewById(R.id.dahrOutMosque);
        In.setImageResource(R.drawable.circleselected);
        Out.setImageResource(R.drawable.circle);
    }
    public void fagrOutMosque()
    {
        fagrInt=1;
        ImageView In=(ImageView)view.findViewById(R.id.fagrInMosque);
        ImageView Out=(ImageView)view.findViewById(R.id.fagrOutMosque);
        In.setImageResource(R.drawable.circle);
        Out.setImageResource(R.drawable.circleselected);
    }
    public void fagrInMosque()
    {
        fagrInt=2;
        ImageView In=(ImageView)view.findViewById(R.id.fagrInMosque);
        ImageView Out=(ImageView)view.findViewById(R.id.fagrOutMosque);
        In.setImageResource(R.drawable.circleselected);
        Out.setImageResource(R.drawable.circle);
    }
}
