package com.islami.app.Qur2anContent;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.ExpandableListView;

import com.islami.app.R;

public class SearchResult extends AppCompatActivity {

    ExpandableListView result;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_result);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        result=(ExpandableListView)findViewById(R.id.Searchresult);

        SearchExpandableListAdapter adapter=new SearchExpandableListAdapter(this,result);
        result.setAdapter(adapter);

    }


}
