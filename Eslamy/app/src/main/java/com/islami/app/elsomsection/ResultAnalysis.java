package com.islami.app.elsomsection;

import android.database.Cursor;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.joda.time.chrono.IslamicChronology;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import com.islami.app.R;

public class ResultAnalysis extends Fragment implements AdapterView.OnItemSelectedListener {

    String fontPath = "fonts/GESSTwoLight.otf";
    final String [] MonthNamesArabic={"يناير"  , "فبراير ","مارس", "ابريل","مايو", "يونيو","يوليو","اغسطس","سبتمبر","أكتوبر","نوفمبر","ديسمبر"};
    final String [] MonthNamesEnglish={"January","February","March","April","May","June","July","August","September","October","November","December"};
    final String [] HijriMonthArabic={"محرم","ذى الحجه","شوال","محرم","شعبان"};
    final String [] HijriMonthEnglish={"Moharam","Ze El-haga","Shwal","Moharam","Shaaban"};
    private Spinner frommonth;
    private Spinner tomonth;
    private Spinner today,fromday;
    private Spinner fromyear,toyear;
    String fmonth,tmonth , fday= "1" ,tday="1" , fyear="2016",tyear="2016";
    int numofdays=0;
    String lan="A";
    Button show;
    List<String> listMonthArabic = new ArrayList<String>();
    List<String> listMonthEnglis = new ArrayList<String>();
    List<String> listdays = new ArrayList<String>();
    List<String> listyears = new ArrayList<String>();
    Database MYDP;
    LocalDate  startdate;
    LocalDate  enddate;
    LocalDate  Moharam;
    LocalDate  Shwal;
    LocalDate  Shaaban;
    LocalDate  Ramadan;
    LocalDate  ashora;
    LocalDate  arfa;
    int otherdaysyear;
    TextView fard,mokaed,motlk;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.resultanalysis, container, false);
        Typeface tf = Typeface.createFromAsset(getResources().getAssets(), fontPath);
        frommonth = (Spinner) view.findViewById(R.id.frommonth);
        tomonth = (Spinner) view.findViewById(R.id.tomonth);
        today=(Spinner)view.findViewById(R.id.today);
        fromday=(Spinner)view.findViewById(R.id.fromday);
        fromyear=(Spinner)view.findViewById(R.id.fromyear);
        toyear=(Spinner)view.findViewById(R.id.toyear);
        fard=(TextView)view.findViewById(R.id.fardtext);
        mokaed=(TextView)view.findViewById(R.id.moedtext);
        motlk=(TextView)view.findViewById(R.id.motlaktext);
        show=(Button)view.findViewById(R.id.show);
        show.setTypeface(tf);
        show.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                show();
            }
        });
        MYDP=new Database(getContext());
        Moharam =new LocalDate(1437,1,1);
        Shwal=new LocalDate(1473,10,1);
        Shaaban=new LocalDate(1437,8,1);
        Ramadan=new LocalDate(1437,9,1);
        ashora=new LocalDate(1473,1,10);
        arfa=new LocalDate(1473,12,9);
        loadmonths();
        loadYear(fromyear);
        loadYear(toyear);
        if (lan.equals("A")) {
            tmonth = "يناير";
            fmonth = "يناير";
        } else {
            tmonth = "January";
            fmonth = "January";
        }
        Calendar c =new GregorianCalendar(2016,1,1);
        numofdays=c.get(Calendar.DAY_OF_MONTH);
        addListenerOnSpinnerItemSelection();
        return view;

    }

    public void addListenerOnSpinnerItemSelection(){
        frommonth.setOnItemSelectedListener(this);
        tomonth.setOnItemSelectedListener(this);
    }

    public void loadYear(Spinner sp1 ){
    listyears.clear();
    for( int i=2016 ; i<2060;i++){
        listyears.add(String.valueOf(i));
    }
   setadapter(listyears, sp1 );
    }

    public void loadmonths(){

    for( int i=0 ; i<MonthNamesArabic.length;i++){
        listMonthArabic.add(MonthNamesArabic[i]);
        listMonthEnglis.add(MonthNamesEnglish[i]);
    }
        if(lan.equals("A")){
       setadapter(listMonthArabic,frommonth);
        setadapter(listMonthArabic,tomonth);
        }
        else{
            setadapter(listMonthEnglis,frommonth);
            setadapter(listMonthEnglis,tomonth);
        }
       addListenerOnSpinnerItemSelection();

  }

    public void loadtodays(int days,Spinner sp1  ){
   listdays.clear();
    for (int i=1;i<=days;i++){
        listdays.add(String.valueOf(i));
    }
    setadapter(listdays , sp1 );
}

    public void setadapter(List<String>list , Spinner sp1  ){
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(),
                android.R.layout.simple_spinner_item, list) {
           public View getView(int position, View convertView, ViewGroup parent) {
                View v = super.getView(position, convertView, parent);
                Typeface externalFont=Typeface.createFromAsset(getContext().getAssets(), fontPath);
                ((TextView) v).setTypeface(externalFont);
                return v;
    }
            public View getDropDownView(int position,  View convertView,  ViewGroup parent) {
                View v =super.getDropDownView(position, convertView, parent);
                Typeface externalFont=Typeface.createFromAsset(getContext().getAssets(), fontPath);
                ((TextView) v).setTypeface(externalFont);
                return v;
            }
        };
        adapter.setDropDownViewResource
                (android.R.layout.simple_spinner_dropdown_item);
        sp1.setAdapter(adapter);
     }

    public void updatedays(int month,int year,Spinner sp1){
       Calendar c =Calendar.getInstance();
       c.set(Calendar.YEAR,year);
       c.set(Calendar.MONTH,month);
       numofdays=  c.getActualMaximum(Calendar.DAY_OF_MONTH);
       loadtodays(numofdays,sp1);
   }

    public  int Search(ArrayList<String> list,String strsearch){
      int i=0;
      for (String s:list){
          if(s.equals(strsearch))return i;
          i++;
      }
      return -1;
  }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        fmonth= frommonth.getSelectedItem().toString();
        tmonth= tomonth.getSelectedItem().toString();
        fyear=fromyear.getSelectedItem().toString();
        tyear=toyear.getSelectedItem().toString();
        updatedays(listMonthArabic.indexOf(fmonth)+1,Integer.parseInt(fyear),fromday );
        updatedays(listMonthArabic.indexOf(tmonth)+1,Integer.parseInt(tyear),today);
        tday=today.getSelectedItem().toString();
        fday=fromday.getSelectedItem().toString();
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }


    public int GetHijriYear(int GeoYear , int Geomonth, int Geoday){
        DateTime dtISO = new DateTime(GeoYear, Geomonth, Geoday, 12, 0, 0, 0);
        DateTime dtIslamic = dtISO.withChronology(IslamicChronology.getInstance());
        return  dtIslamic.getYear();
    }

    public int GetHijriMonth(int GeoYear , int Geomonth, int Geoday){
        DateTime dtISO = new DateTime(GeoYear, Geomonth, Geoday, 12, 0, 0, 0);
        DateTime dtIslamic = dtISO.withChronology(IslamicChronology.getInstance());
        return  dtIslamic.getMonthOfYear();
    }
    public int GetHijriDay(int GeoYear , int Geomonth, int Geoday) {
        DateTime dtISO = new DateTime(GeoYear, Geomonth, Geoday, 12, 0, 0, 0);
        DateTime dtIslamic = dtISO.withChronology(IslamicChronology.getInstance());
        return dtIslamic.getDayOfMonth();
    }




    public void show(){

        fmonth= frommonth.getSelectedItem().toString();
        fyear=fromyear.getSelectedItem().toString();
        fday=fromday.getSelectedItem().toString();
        startdate=new LocalDate(GetHijriYear(Integer.parseInt(fyear), listMonthArabic.indexOf(fmonth)+1,Integer.parseInt(fday)),GetHijriMonth(Integer.parseInt(fyear),listMonthArabic.indexOf(fmonth)+1,Integer.parseInt(fday)),
                GetHijriDay(Integer.parseInt(fyear), listMonthArabic.indexOf(fmonth) + 1, Integer.parseInt(fday)));
        Toast.makeText(getContext(),startdate.getYear() + " "+ startdate.getMonthOfYear() +" "+startdate.getDayOfMonth(),Toast.LENGTH_LONG).show();
        tmonth= tomonth.getSelectedItem().toString();
        tyear=toyear.getSelectedItem().toString();
        tday=today.getSelectedItem().toString();
        enddate=new LocalDate(GetHijriYear(Integer.parseInt(tyear), listMonthArabic.indexOf(tmonth)+1
                ,Integer.parseInt(tday)),GetHijriMonth(Integer.parseInt(tyear), listMonthArabic.indexOf(tmonth)+1,Integer.parseInt(tday)),
         GetHijriDay(Integer.parseInt(tyear), listMonthArabic.indexOf(tmonth)+1 ,Integer.parseInt(tday)));

        LocalDate start=new LocalDate (startdate.getYear(),startdate.getMonthOfYear(),startdate.getDayOfMonth());
        LocalDate  end=new LocalDate (enddate.getYear(),enddate.getMonthOfYear(),enddate.getDayOfMonth());
        Cursor  [] date =new  Cursor[end.getYear()-start.getYear() +1];
        int numofd=0 , ramadandays=0 , otherdays=0;

        int  j=0;
        for(int i=start.getYear() ; i <= end.getYear() ;i++ ){
            date[j]=MYDP.getDatabyhijri(i+"");
            date[j].moveToFirst();
            if (date[j].getCount()<=0){
                j++;
                break;
            }
            Moharam =new LocalDate(i,1,1);
            Shwal=new LocalDate(i ,10,1);
            Shaaban=new LocalDate(i,8,1);
            Ramadan=new LocalDate(i,9,1);
            ashora=new LocalDate(i,1,10);
            arfa=new LocalDate(i,12,9);
            otherdaysyear=i;

            if (!Ramadan.isBefore(start) && !Ramadan.isAfter(end)) {
                ramadandays+=Integer.parseInt(date[j].getString(date[j].getColumnIndex("ramadan")));
            }
            if (!Moharam.isBefore(start) && !Moharam.isAfter(end))
                numofd+=Integer.parseInt(date[j].getString(date[j].getColumnIndex("moharam")));
            if (!Shwal.isBefore(start) && !Shwal.isAfter(end))
                numofd+=Integer.parseInt(date[j].getString(date[j].getColumnIndex("shwal")));
            if (!Shaaban.isBefore(start) && !Shaaban.isAfter(end))
                numofd+=Integer.parseInt(date[j].getString(date[j].getColumnIndex("shaaban")));
            if (!arfa.isBefore(start) && !arfa.isAfter(end))
                numofd+=Integer.parseInt(date[j].getString(date[j].getColumnIndex("arfa")));
            if (!ashora.isBefore(start) && !ashora.isAfter(end))
                numofd+=Integer.parseInt(date[j].getString(date[j].getColumnIndex("ashora")));
            if(start.getYear()<=otherdaysyear && end.getYear()>=otherdaysyear){
                otherdays+=Integer.parseInt(date[j].getString(date[j].getColumnIndex("other")));
            }
           j++;

        }
        mokaed.setText(numofd+"");
        fard.setText(ramadandays+"");
        motlk.setText(otherdays+"");

    }

}


