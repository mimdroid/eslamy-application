package com.islami.app.Rosary;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import java.util.ArrayList;

import com.islami.app.R;

public class RosaryMain extends Fragment {

    ArrayList<Fragment> fragments;
    ViewPager viewPager;
    MypagerAdapter pageAdapter;
    private TabLayout tabLayout;
    private int[] tabIconsArabic = {
            R.drawable.zaker_ar_tab1,
            R.drawable.zaker_ar_tab2,
            R.drawable.zaker_ar_tab3,
    };
    private int[]tabIconsEnglish ={
            R.drawable.zaker_en_tab1,
            R.drawable.zaker_en_tab2,
            R.drawable.zaker_en_tab3
    };

    @Override
    public void onCreate( Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
    int tabnum;

    public RosaryMain()
    {

    }

    public void setTabnum(int tabnum) {
        this.tabnum = tabnum;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.rosary_main, container, false);

        tabLayout = (TabLayout) view.findViewById(R.id.tabs);
        viewPager=(ViewPager)view.findViewById(R.id.viewPagerContainer);
        setupViewPager(viewPager);
        setuptabLayout();
     //   Log.e("bef", "called");
        tabLayout.getTabAt(tabnum).select();
        return view;

    }
    public void setupViewPager(ViewPager viewPager) {
        pageAdapter = new MypagerAdapter(viewPager.getContext(),getActivity().getSupportFragmentManager());
        pageAdapter.addFragment(new RosaryStatistics(),tabIconsArabic[2]);
        pageAdapter.addFragment(new RosaryAlarm(),tabIconsArabic[1]);
        pageAdapter.addFragment(new RosaryActivity(),tabIconsArabic[0]);
        viewPager.setAdapter(pageAdapter);
    }
    public void setuptabLayout(){
        try {
            tabLayout.setupWithViewPager(viewPager);
            for (int i = 0; i < tabLayout.getTabCount(); i++) {
                TabLayout.Tab tab = tabLayout.getTabAt(i);
                tab.setCustomView(pageAdapter.getTabView(i));
            }
            tabLayout.requestFocus();
        }
        catch(Exception ex){
            Log.e("errr2", ex.getMessage());
        }
    }
    public class MypagerAdapter extends FragmentStatePagerAdapter {
        Context mContext;
        ArrayList<Fragment>fragments=new ArrayList<>();
        ArrayList<Integer>Icons=new ArrayList<>();

        @Override
        public Fragment getItem(int position) {
            return fragments.get(position);
        }

        @Override
        public int getCount() {
            return fragments.size();
        }

        public MypagerAdapter(Context context, FragmentManager fm) {
            super(fm);
            this.mContext = context;
        }

        public void addFragment(Fragment fragment,  int drawable) {
            fragments.add(fragment);
            Icons.add(drawable);
        }

        @Override
        public int getItemPosition(Object object)
        {
            return POSITION_UNCHANGED;
        }



        @Override
        public CharSequence getPageTitle(int position) {
            return null;
        }

        public View getTabView(int position) {
            View tab = LayoutInflater.from(mContext).inflate(R.layout.tabindicatorsoom, null);
            ImageView tabImage = (ImageView) tab.findViewById(R.id.tab_Image);
            tabImage.setBackgroundResource(Icons.get(position));
            return tab;
        }

    }
}
