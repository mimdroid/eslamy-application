package com.islami.app.SalahContent;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import com.islami.app.R;

/**
 * Created by Ibrahim on 12/29/2015.
 */
public class AzkarSalahAdapter extends BaseAdapter {
    ArrayList<String> title_arabic=new ArrayList<>();
    ArrayList<String>title_english=new ArrayList<>();
    String Lang="arabic";
    Context context;
    Typeface custom_font_arabic;
    private static LayoutInflater inflater=null;
    public AzkarSalahAdapter(Context c,ArrayList<String>a,ArrayList<String>a1)
    {
        context=c;
        title_arabic=a;
        title_english=a1;
        custom_font_arabic= Typeface.createFromAsset(context.getAssets(), "fonts/GESSTwoLight.otf");
        //Lang;
        inflater = ( LayoutInflater )context.
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }
    @Override
    public int getCount() {
        return title_arabic.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Holder holder=new Holder();
        View rowView;
        rowView = inflater.inflate(R.layout.zkaar_item, null);
        holder.tv=(TextView) rowView.findViewById(R.id.zkaar_item_textview);
        if(Lang.equals("arabic")) {
            holder.tv.setText(title_arabic.get(position));
            holder.tv.setTypeface(custom_font_arabic);
        }
        else
            holder.tv.setText(title_english.get(position));
        if(position%2==0)
        {
            holder.tv.setBackgroundColor(Color.parseColor("#80B0B0B0"));
        }

        return rowView;
    }
    public class Holder
    {
        TextView tv;
    }
}
