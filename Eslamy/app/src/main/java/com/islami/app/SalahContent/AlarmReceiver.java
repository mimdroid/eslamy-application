package com.islami.app.SalahContent;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Location;
import android.location.LocationManager;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import Manage.GPS;
import com.islami.app.R;

/**
 * Created by Ibrahim on 12/25/2015.
 */
public class AlarmReceiver extends BroadcastReceiver {
    String Massage="أذان الفجر";
    Context mContext;
    Timer timer;
    TimerTask timerTask;
    ArrayList<Integer> arrayList;
    double lat=0,lon=0;
    GPS gps;
    LocationManager locationManager;
    Location l;
    SharedPreferences sharedPreferences;

    public AlarmReceiver()
    {

    }
    @Override
    public void onReceive(Context context, Intent intent) {

        int index=intent.getIntExtra("index",-1);
        //Log.v("index++++",+"");
        mContext=context;
        boolean ring=false;
        String massage="";
        sharedPreferences=context.getSharedPreferences("Salah_Alarm", Context.MODE_PRIVATE);
        if(index==0)
        {
            massage="وقت أذان الفجر بعد 10 دقائق";
            ring=sharedPreferences.getBoolean("fagr_b",false);
        }
        else if(index==1)
        {
            massage="وقت أذان الظهر بعد 10 دقائق";
            ring=sharedPreferences.getBoolean("dahr_b",false);
        }
        else if(index==2)
        {

            massage="وقت أذان العصر بعد 10 دقائق";
            ring=sharedPreferences.getBoolean("asr_b",false);
        }

        else if(index==3)
        {

            massage="وقت أذان المغرب بعد 10 دقائق";
            ring=sharedPreferences.getBoolean("maghrb_b",false);
        }
        else if(index==4)
        {

            massage="وقت أذان العشاء بعد 10 دقائق";
            ring=sharedPreferences.getBoolean("isha_b",false);
        }
        else if(index==5)
        {

            massage="حان الان موعد اذان الفجر";
            ring=sharedPreferences.getBoolean("fagr",false);
        }
        else if(index==6)
        {

            massage="حان الان موعد اذان الظهر";
            ring=sharedPreferences.getBoolean("dahr",false);
        }
        else if(index==7)
        {

            massage="حان الان موعد اذان العصر";
            ring=sharedPreferences.getBoolean("asr",false);
        }
        else if(index==9)
        {

            massage="حان الان موعد اذان المغرب";
            ring=sharedPreferences.getBoolean("maghrb",false);
        }
        else if(index==9)
        {

            massage="حان الان موعد اذان العشاء";
            ring=sharedPreferences.getBoolean("isha",false);
        }
        if(ring==true)
        {
            Notify(massage);
        }
    }

    public void Notify(String massage)
    {
        NotificationManager mNotificationManager =
                (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);//Notification manager getSystemService(Notification) to notify the system
// Sets an ID for the notification, so it can be updated
        int notifyID = 1;
        Intent i=new Intent(mContext,Azkaar_section_salah.class);//intent which will start when click on Notification
        String file="raw/testsound.mp3";
        Log.v("test assest", mContext.getAssets().toString() + file);
        PendingIntent p=PendingIntent.getActivity(mContext.getApplicationContext(),notifyID,i,0);//to start intent from outside application
        NotificationCompat.Builder mNotifyBuilder = new NotificationCompat.Builder(mContext)//to build Notification
                .setContentTitle("New Message")//to set title of Notification
                .setContentText("You've received new messages.")//set massage of Notification
                .setSmallIcon(R.drawable.ic_launcher)//set icon of Notification
                .setAutoCancel(true)// make Notification Auto cancel when user click on notification
                .setContentIntent(p)//set which intent will start when user click on Notification
                .addAction(R.drawable.ic_launcher, "Notify", p);//to Add Button and make start Activity when click it
        Uri soundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        Uri path = Uri.parse("android.resource://com.islami.app/" + R.raw.testsound);
        mNotifyBuilder.setSound(path);
        mNotificationManager.notify(notifyID,mNotifyBuilder.build());//notify user
    }
}
