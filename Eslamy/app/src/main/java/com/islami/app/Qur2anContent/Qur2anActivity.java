package com.islami.app.Qur2anContent;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import java.util.ArrayList;

import com.islami.app.R;

/**
 * Created by Admin on 1/3/2016.
 */
public class Qur2anActivity extends Fragment{



    ArrayList<Fragment>fragments;
    ViewPager viewPager;
    MypagerAdapter pageAdapter;
    private TabLayout tabLayout;
    private int[] tabIcons = {
            R.drawable.quran_ar_tab1,
            R.drawable.quran_ar_tab2,
            R.drawable.quran_ar_tab3,
            R.drawable.quran_ar_tab4,
            R.drawable.quran_ar_tab5
    };

    /**
     * The {@link ViewPager} that will host the section contents.
     */

    @Override
    public void onCreate( Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
    int tabnum;

    public void setTabnum(int tabnum) {
        this.tabnum = tabnum;
    }

    public    Qur2anActivity (){}

public void MakeToast(final String text, final Activity c){
    c.runOnUiThread(new Runnable() {
        @Override
        public void run() {

            Toast.makeText(c,text,Toast.LENGTH_LONG).show();
        }
    });
}




    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_qur2an, container, false);

        tabLayout = (TabLayout) view.findViewById(R.id.tabs);
        viewPager=(ViewPager)view.findViewById(R.id.viewPagerContainer);
        setupViewPager(viewPager);
        setuptabLayout();
        tabLayout.getTabAt(tabnum).select();
        AdView mAdView = (AdView) view.findViewById(R.id.adViewmain);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);
        return view;


    }

    public void setuptabLayout(){
        try {
            tabLayout.setupWithViewPager(viewPager);
            for (int i = 0; i < tabLayout.getTabCount(); i++) {
                TabLayout.Tab tab = tabLayout.getTabAt(i);
                tab.setCustomView(pageAdapter.getTabView(i));
            }
            tabLayout.requestFocus();
        }
        catch(Exception ex){
            Log.e("errr2", ex.getMessage());
        }
    }

    public void setupViewPager(ViewPager viewPager) {
       pageAdapter = new MypagerAdapter(getContext(),getActivity().getSupportFragmentManager());
       Stats f1= new Stats();
        f1.setRetainInstance(true);
        pageAdapter.addFragment(f1,  tabIcons[0]);
        Search f2=new Search();
        f2.setRetainInstance(true);
        pageAdapter.addFragment(f2,tabIcons[1]);
        Targama f3=new Targama();
        f3.setRetainInstance(true);
        pageAdapter.addFragment(f3, tabIcons[2]);
         Read f4=new Read();
        f4.setRetainInstance(true);
        pageAdapter.addFragment(f4, tabIcons[3]);
        Listen f5=new Listen();
        f5.setRetainInstance(true);
        pageAdapter.addFragment(f5,tabIcons[4]);
        viewPager.setAdapter(pageAdapter);
    }

    public void suraselected(View v){

        TextView id=(TextView) v.findViewById(R.id.suraid);

        if (tabLayout.getSelectedTabPosition()==4){
            Intent i=new Intent(getActivity(),ListenToSura.class);
            i.putExtra("suraid",id.getText().toString());
            startActivity(i);
        }
        else {

            Intent i=new Intent(getContext(),ReadSura.class);
          //  Log.e("aa",id.getText().toString());
            i.putExtra("suraid",id.getText().toString());
            startActivity(i);
        }


    }




    public class MypagerAdapter extends FragmentStatePagerAdapter{
        Context mContext;
        ArrayList<Fragment>fragments=new ArrayList<>();
        ArrayList<Integer>Icons=new ArrayList<>();

        @Override
        public Fragment getItem(int position) {
            return fragments.get(position);
        }

        @Override
        public int getCount() {
            return fragments.size();
        }

        public MypagerAdapter(Context context, FragmentManager fm) {
            super(fm);
            this.mContext = context;
        }

        public void addFragment(Fragment fragment,  int drawable) {
            fragments.add(fragment);
            Icons.add(drawable);
        }




        @Override
        public int getItemPosition(Object object)
        {
            return POSITION_UNCHANGED;
        }



        @Override
        public CharSequence getPageTitle(int position) {
            return null;
        }

        public View getTabView(int position) {
            View tab = LayoutInflater.from(mContext).inflate(R.layout.tabindicatorsoom, null);
            ImageView tabImage = (ImageView) tab.findViewById(R.id.tab_Image);
            tabImage.setBackgroundResource(Icons.get(position));

            return tab;
        }

    }



}
