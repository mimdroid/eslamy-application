package com.islami.app;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import com.islami.app.R;

import ConnectionHandler.Connector;
import ConnectionHandler.RadioChannel;
import Manage.GPS;
import Manage.PrayTime;
import Manage.Reader;
import com.islami.app.BaseDrawerContent.MyExpandableListAdapter;
import com.islami.app.BaseDrawerContent.navitemdata;
import com.islami.app.BaseDrawerContent.submenuitem;
import com.islami.app.CarouselContent.MyPagerAdapter;
import com.islami.app.Qur2anContent.ListenToSura;
import com.islami.app.Qur2anContent.Qur2anActivity;
import com.islami.app.Qur2anContent.ReadSura;
import com.islami.app.Rosary.RosaryMain;
import com.islami.app.SalahContent.SalahMain;
import com.islami.app.ZakahContent.MainZakah;
import com.islami.app.elsomsection.SoomActivity;

public class HomeActivity extends AppCompatActivity {

    private DrawerLayout mDrawerLayout;
    MyExpandableListAdapter mMenuAdapter;
    ExpandableListView expandableList;
    List<navitemdata> listDataHeader;
    HashMap<String, List<submenuitem>> listDataChild;

    protected Fragment currentFragment;

    /********************************************************/
  public MyPagerAdapter adapter=null;
    public ViewPager pager;

    MyRadio RadioThread=null;
    Button play,Stop,RadioTimer;
    Thread th;
    Connector c;
    SimpleDateFormat Dtime,Ddate;
    TextView time;
    TextView date;
    Thread TimerThreadRadio;
    public static int PAGES ;
    public  static int LOOPS ;
    public  static int FIRST_PAGE ;
    public final static float BIG_SCALE = 1.0f;
    public final static float SMALL_SCALE = 0.8f;
    public final static float DIFF_SCALE = BIG_SCALE - SMALL_SCALE;
    ArrayList<RadioChannel>channels;
/*************************************************************************//////////



   protected void onCreateDrawer(){
    expandableList=(ExpandableListView)findViewById(R.id.navigationmenu);
    Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
    setSupportActionBar(toolbar);
    ImageView Home=(ImageView) findViewById(R.id.gotoHome);
    Home.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            getSupportFragmentManager().popBackStack();
        }
    });


    final DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
    ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
            this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
    drawer.setDrawerListener(toggle);
    toggle.syncState();
    prepareListData();
    mMenuAdapter = new MyExpandableListAdapter (this,listDataHeader ,listDataChild, expandableList);

    View footerView =getLayoutInflater(). inflate(R.layout.nav_footer_home, expandableList, false);
    LinearLayout footer=(LinearLayout) footerView.findViewById(R.id.footerlayout);
    TextView callus=(TextView)footerView.findViewById(R.id.callus);
    callus.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Toast.makeText(getApplicationContext(),"aaaa",Toast.LENGTH_LONG).show();
        }
    });
    expandableList.addFooterView(footer);
    expandableList.setAdapter(mMenuAdapter);
    expandableList.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
        @Override
        public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {

            android.support.v4.app.FragmentManager fragmentManager = getSupportFragmentManager();
            android.support.v4.app.FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

            if (groupPosition==0){//salah section
                Qur2anActivity fragment = new Qur2anActivity();
                currentFragment=fragment;

                // fragment.setRetainInstance(true);
                if(childPosition==0){
                    fragment.setTabnum(4);

                }else if (childPosition==1){//2ebla
                    fragment.setTabnum(3);

                }else if (childPosition==2){//msaged
                    fragment.setTabnum(2);

                }else if (childPosition==3){//azkar salah
                    fragment.setTabnum(1);

                }else if (childPosition==4){//e7sa2yat
                    fragment.setTabnum(0);

                }
                getSupportFragmentManager().popBackStack();
                fragmentTransaction.replace(R.id.contentFrame, fragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
                drawer.closeDrawer(GravityCompat.START);

            }
            else if (groupPosition==1){//salah section
                SalahMain fragment=new SalahMain();
                if(childPosition==0){//mnabeh
                    fragment.setTabnum(4);
                }else if (childPosition==1){//2ebla
                    fragment.setTabnum(3);
                }else if (childPosition==2){//msaged
                    fragment.setTabnum(2);
                }else if (childPosition==3){//azkar salah
                    fragment.setTabnum(1);
                }else if (childPosition==4){//e7sa2yat
                    fragment.setTabnum(0);
                }

                fragmentTransaction.replace(R.id.contentFrame, fragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
                drawer.closeDrawer(GravityCompat.START);

            }else if (groupPosition==2) {//soom
                SoomActivity fragment = new SoomActivity();
                currentFragment=fragment;
                if (childPosition == 0) {
                    fragment.setTabnum(2);
                } else if (childPosition == 1) {
                    fragment.setTabnum(1);
                }
                else if (childPosition==2){
                    fragment.setTabnum(0);
                }
                fragmentTransaction.replace(R.id.contentFrame, fragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
                drawer.closeDrawer(GravityCompat.START);

            }else if (groupPosition==3) {//Rosary
                RosaryMain fragment = new RosaryMain();
                currentFragment=fragment;
                if (childPosition == 0) {//mnabeh
                    fragment.setTabnum(2);
                } else if (childPosition == 1) {//2ebla
                    fragment.setTabnum(1);
                }else if (childPosition==2){
                    fragment.setTabnum(0);
                }
                fragmentTransaction.replace(R.id.contentFrame, fragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
                drawer.closeDrawer(GravityCompat.START);

            }
            else if (groupPosition==4) {//Zakah
                MainZakah fragment = new MainZakah();
                currentFragment=fragment;
                if (childPosition == 0) {
                    fragment.setTabnum(2);
                } else if (childPosition == 1) {
                    fragment.setTabnum(1);
                }else if (childPosition==2){
                    fragment.setTabnum(0);
                }
                fragmentTransaction.replace(R.id.contentFrame, fragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
                drawer.closeDrawer(GravityCompat.START);

            }
            return false;
        }
    });

}

    private void prepareListData() {
        listDataHeader = new ArrayList<navitemdata>();
        listDataChild = new HashMap<String,List<submenuitem> >();

        // Adding data header
        listDataHeader.add(new navitemdata("القران الكريم", R.drawable.quranslider));
        listDataHeader.add(new navitemdata("الصلاه", R.drawable.salahslider) );
        listDataHeader.add(new navitemdata("الصوم", R.drawable.soomslider));
        listDataHeader.add(new navitemdata("الذكر", R.drawable.zekrslider));
        listDataHeader.add(new navitemdata("الزكاه", R.drawable.zakahslider));
        listDataHeader.add(new navitemdata("ادوات", R.drawable.adwatslider));
        listDataHeader.add(new navitemdata("محادثات", R.drawable.chatslider));
        listDataHeader.add(new navitemdata("ادعو صديق", R.drawable.addfriendslider));

        // listDataHeader.add(new navitemdata("محادثات",R.drawable.chatslider));
        // listDataHeader.add(new navitemdata("اضافة صديق",R.drawable.addfriendslider));

        // Adding child data
        List<submenuitem> heading1= new ArrayList<submenuitem>();
        heading1.add(new submenuitem("المصحف الصوتي", R.drawable.play));
        heading1.add(new submenuitem("المصحف قراءه", R.drawable.quranabyad));
        heading1.add(new submenuitem("الترجمه و التفسير", R.drawable.translation));
        heading1.add(new submenuitem("بحث عن السور  و الايات", R.drawable.search));
        heading1.add(new submenuitem("احصائيات", R.drawable.stat));


        List<submenuitem> heading2= new ArrayList<submenuitem>();
        heading2.add(new submenuitem("اتجاه القبله", R.drawable.kebla));
        heading2.add(new submenuitem("اذكار الصلاه", R.drawable.seb7abeda));
        heading2.add(new submenuitem("مساجد حولي", R.drawable.mosques));
        heading2.add(new submenuitem("منبه مواعيد الصلاه", R.drawable.alarm));
        heading2.add(new submenuitem("احصائيات", R.drawable.stat));


        List<submenuitem> heading3= new ArrayList<submenuitem>();
        heading3.add(new submenuitem("شهر رمضان", R.drawable.helal));
        heading3.add(new submenuitem("ايام السنه", R.drawable.calender));
        heading3.add(new submenuitem("احصائيات", R.drawable.stat));

        List<submenuitem> heading4= new ArrayList<submenuitem>();
        heading4.add(new submenuitem("المسبحه الالكترونيه", R.drawable.seb7abeda));
        heading4.add(new submenuitem("منبه التسبيح", R.drawable.alarm));
        heading4.add(new submenuitem("احصائيات", R.drawable.stat));

        List<submenuitem> heading5= new ArrayList<submenuitem>();
        heading5.add(new submenuitem("حاسبة الزكاه", R.drawable.calculator));
        heading5.add(new submenuitem("مواضيع الزكاه", R.drawable.zakahslider));
        heading5.add(new submenuitem("احصائيات", R.drawable.stat));

        List<submenuitem> heading6= new ArrayList<submenuitem>();
        heading6.add(new submenuitem("حصن المسلم", R.drawable.hesnmoslem));
        heading6.add(new submenuitem("تفسير الاحلام", R.drawable.tafseera7lam));
        heading6.add(new submenuitem("الرقيه الشرعيه", R.drawable.quranabyad));
        heading6.add(new submenuitem("التاريخ الهجري", R.drawable.calender));
        heading6.add(new submenuitem("اسلاميات اناشيد", R.drawable.play));
        heading6.add(new submenuitem("احائيث مختاره", R.drawable.search));
        heading6.add(new submenuitem("مناسك الحج و العمره", R.drawable.hegwe3omra));



        listDataChild.put(listDataHeader.get(0).toString(), heading1);// Header, Child data
        listDataChild.put(listDataHeader.get(1).toString(), heading2);
        listDataChild.put(listDataHeader.get(2).toString(), heading3);
        listDataChild.put(listDataHeader.get(3).toString(), heading4);
        listDataChild.put(listDataHeader.get(4).toString(), heading5);
        listDataChild.put(listDataHeader.get(5).toString(), heading6);

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.e("detached","ssssssssssssssssssssssssssssssss");

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FragmentManager.enableDebugLogging(true);

        setContentView(R.layout.activity_home);
        onCreateDrawer();
         c=new Connector(this);
        initializeTimer();
        initializedoaaSlider();
        updateSalahtime();
        updateCarousel();
        initializeRadio();
        Dtime = new SimpleDateFormat("HH:mm");
        Ddate = new SimpleDateFormat("yyyy-MM-dd");
       System.gc();
        AdView mAdView = (AdView) findViewById(R.id.adViewmain);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);

    }


/*
    @Override
    protected void onResume() {
       System.gc();
        super.onResume();
        stopped=false;
    }

    @Override
    protected void onPause() {
        super.onPause();
        System.gc();
        Log.e("paaaaaaause","paaaaaaaaaaaaause");

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    Log.e("maaaaaaaat","maaaaaaaat");
    }
*/


    public void suraselected(View v){

        TextView id=(TextView) v.findViewById(R.id.suraid);
            String text=id.getText().toString();
       Intent i=null;
        if (text.contains(" r")) {
            text=text.replace(" r", "");
            i = new Intent(this, ReadSura.class);
        }  else if (text.contains(" l")){
            text=text.replace(" l","");
            i=new Intent(this,ListenToSura.class);
        }
            i.putExtra("suraid",text);
            startActivity(i);



    }
    public void initializeTimer(){
    time=(TextView)findViewById(R.id.time);
    date=(TextView)findViewById(R.id.date);
    th=new Thread(){
    public void run(){
        while (!isInterrupted()){
        try {
            runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Date d=new Date();
                time.setText(getTime()+" ");
                date.setText(getDate());
            }
            });
            sleep(5000); } catch (InterruptedException e) {
            e.printStackTrace();
        }
        }

    }};
    th.start();
}
    String getTime(){
        Date d=new Date();
       return  Dtime.format(d);

    }
    String getDate(){
        Date d=new Date();
        return  Ddate.format(d);

    }
    public class TimerThread extends Thread{
   int mins;
    public TimerThread(int mins){
        this.mins=mins;

    }
    @Override
    public void run(){
        try {
            sleep(mins*60);
            RadioThread.stop();
        }catch (InterruptedException e){

        }


    }
}
    public void StartTimerThreadRadio(int mins){
        if (mins<=0)return;
        if (TimerThreadRadio!=null&&TimerThreadRadio.isAlive()){
            Thread moribund = TimerThreadRadio;
            TimerThreadRadio = null;
            moribund.interrupt();
        }
        else {
            TimerThreadRadio=new TimerThread(mins);
            TimerThreadRadio.start();

        }


    }
    public void initializedoaaSlider(){

        Reader r=new Reader(getApplicationContext());
        String doaa=r.readBlessingEnglish();
        TextView movingDoaa=(TextView)findViewById(R.id.movingtext);
        movingDoaa.setText(doaa);


    }

    public void initializeRadio(){

        play=(Button) findViewById(R.id.playradio);
        Stop=(Button) findViewById(R.id.stopradio);
        RadioTimer=(Button)findViewById(R.id.radiotime);

        RadioTimer.setOnClickListener(new View.OnClickListener() {

            EditText input;
            @Override
            public void onClick(View v) {
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(HomeActivity.this);

                //AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.this).create();

                // Setting Dialog Title
                alertDialog.setTitle("Radio Timer");

                // Setting Dialog Message
                alertDialog.setMessage("Stop Radio After ...mins");
                 input = new EditText(getApplicationContext());
                input.setInputType(InputType.TYPE_NUMBER_FLAG_DECIMAL);
                LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.MATCH_PARENT,
                        LinearLayout.LayoutParams.MATCH_PARENT);
                input.setLayoutParams(lp);

                alertDialog.setView(input);

                // Setting Icon to Dialog
                alertDialog.setIcon(R.drawable.alarm);

                // Setting Positive "Yes" Button
                alertDialog.setPositiveButton("set",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int which) {
                              int Mins=  Integer.valueOf(input.getText().toString());


                            }
                        });
                // Setting Negative "NO" Button
                alertDialog.setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                // Write your code here to execute after dialog
                                dialog.cancel();
                            }
                        });

                alertDialog.show();
            }
        });

        Stop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(RadioThread==null)return;
                RadioThread.stop();
                play.setBackgroundResource(R.drawable.play);
            }
        });

        play.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
       if ((channels==null||channels.size()==0)&&!c.isOnline()){
           Toast.makeText(getApplicationContext(),"No Radio chanells Available...\nPlease open internet Connection and try again",Toast.LENGTH_LONG).show();
          return;
       }else if ((channels==null||channels.size()==0)&&c.isOnline()){
           updateCarousel();
      return;
       }
                if (RadioThread==null||RadioThread.mediaPlayer==null){
                     int index= pager.getCurrentItem();
                    Log.e("moq",channels.get(index).moqre2);
                    RadioThread=new MyRadio(getApplicationContext(),
                            channels.get(index).url,channels.get(index).moqre2);
                    RadioThread.play();
                    play.setBackgroundResource(R.drawable.pause);
                }else if ( RadioThread.playing() ) {
                    RadioThread.Pause();
                    play.setBackgroundResource(R.drawable.play);

                }else if (! RadioThread.playing() ) {
                    RadioThread.play();
                    play.setBackgroundResource(R.drawable.pause);

                }
            }
        });


    }

    public void ChannelsReady(){
        if(adapter!=null)return;



        adapter = new MyPagerAdapter(this, this.getSupportFragmentManager(),channels);
        pager.setAdapter(adapter);
        pager.setOnPageChangeListener(adapter);

        pager.setCurrentItem(channels.size()/2);

        pager.setOffscreenPageLimit(3);

        pager.setPageMargin(Integer.parseInt(getString(R.string.pagermargin)));


    }

    public void updateCarousel() {
        pager = (ViewPager) findViewById(R.id.myviewpager);

        Thread RadioGetter=new Thread(){

         public void run(){
             if (channels!=null&&channels.size()!=0)return;
           //  Connector c=new Connector(getApplicationContext());
            if (!c.isOnline())return;
             channels=c.getenRadios();
             runOnUiThread(new Runnable() {
                 @Override
                 public void run() {
                     ChannelsReady();

                 }
             });
         }
     };
        RadioGetter.start();

    }

    public void updateSalahtime(){
        final TextView fagr,dohr,assr,ma8rb,esha2,salahName,temp;
        fagr=(TextView)findViewById(R.id.fagrtime);
        dohr=(TextView)findViewById(R.id.dohrtime);
        assr=(TextView)findViewById(R.id.assrtime);
        ma8rb=(TextView)findViewById(R.id.ma8rbtime);
        esha2=(TextView)findViewById(R.id.esha2time);
        salahName=(TextView)findViewById(R.id.salahname);
        temp=(TextView)findViewById(R.id.temp);

        Thread thread=new Thread(){
            ArrayList prayerTimes;
            ArrayList prayerNames;
            int gaw;
            int index;
            public void run (){
                LocationManager locationManager = (LocationManager) getApplicationContext().getSystemService(Context.LOCATION_SERVICE);

                GPS gps = new GPS(locationManager);
                double latitude = gps.get_Latitude();
                double longitude = gps.get_Longitude();


                double timezone = (Calendar.getInstance().getTimeZone()
                        .getOffset(Calendar.getInstance().getTimeInMillis()))
                        / (1000 * 60 * 60);
                PrayTime prayers = new PrayTime();
                gaw=prayers.getWeather(latitude,longitude);
/*********************************************************************/
                prayers.setTimeFormat(prayers.Time12);
                prayers.setCalcMethod(prayers.Makkah);
                prayers.setAsrJuristic(prayers.Shafii);
                prayers.setAdjustHighLats(prayers.AngleBased);
                int[] offsets = { 0, 0, 0, 0, 0, 0, 0 }; // {Fajr,Sunrise,Dhuhr,Asr,Sunset,Maghrib,Isha}
                prayers.tune(offsets);

                Date now = new Date();
                // Toast.makeText(getApplicationContext(),now.toString(),Toast.LENGTH_LONG).show();

                Calendar cal = Calendar.getInstance();
                cal.setTime(now);
                 prayerTimes = prayers.getPrayerTimes(cal, latitude,
                        longitude, timezone);
                 prayerNames = prayers.getTimeNames();
                 index=prayers.getNextPreyer(cal,prayerTimes);


                String next="";


                switch (index){
                    case 0:
                        next="Fajr";
                        break;
                    case 2:
                        next="Duhr";
                        break;
                    case 3:
                        next="Assr";
                        break;
                    case 4:
                        next="Maghreb";
                        break;
                    case 5:
                        next="Eshaa";
                        break;

                    default:
                        next=prayerNames.get(index).toString();
                }
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        salahName.setText("Next Prayer"+"\n"+prayerNames.get(index).toString());
                        fagr.setText(prayerTimes.get(0)+"");
                        dohr.setText(prayerTimes.get(2)+"");
                        assr.setText(prayerTimes.get(3)+"");
                        ma8rb.setText(prayerTimes.get(4)+"");
                        esha2.setText(prayerTimes.get(6)+"");
                        temp.setText(gaw+"°");

                    }
                });


            }
        };
        thread.run();



    }





}
