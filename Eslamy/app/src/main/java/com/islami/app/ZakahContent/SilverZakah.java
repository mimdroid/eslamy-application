package com.islami.app.ZakahContent;

import android.app.Activity;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.islami.app.R;


public class SilverZakah extends Activity {
    EditText editTextWeight;
    EditText editTextPrice;
    TextView result_view;
    String Lang="arabic";
    TextView textArabicWeight;
    TextView textEnglishWeight;
    TextView textArabicPrice;
    TextView textEnglishPrice;
    TextView resultArabic;
    TextView resultEnglish;
    TextView calview;
    Typeface custom_font_arabic;
    Typeface custom_font_english;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.silver_zakah);
        textArabicWeight=(TextView)findViewById(R.id.weight_silver_text_arabic);
        textEnglishWeight=(TextView)findViewById(R.id.weight_silver_text_english);
        textArabicPrice=(TextView)findViewById(R.id.silver_price_arabic);
        textEnglishPrice=(TextView)findViewById(R.id.silver_price_english);
        resultArabic=(TextView)findViewById(R.id.silver_result_arabic);
        resultEnglish=(TextView)findViewById(R.id.silver_result_english);
        calview=(TextView)findViewById(R.id.silver_calculate);
        custom_font_arabic= Typeface.createFromAsset(getAssets(), "fonts/GESSTwoLight.otf");
        textArabicWeight.setTypeface(custom_font_arabic);
        textArabicPrice.setTypeface(custom_font_arabic);
        resultArabic.setTypeface(custom_font_arabic);
        if(Lang.equals("arabic"))
        {
            calview.setTypeface(custom_font_arabic);
        }
        // Lang="english";
        if(Lang.equals("english"))
        {
            changeLang();
        }
    }
    public void changeLang()
    {
        textArabicWeight.setVisibility(View.GONE);
        textEnglishWeight.setVisibility(View.VISIBLE);
        textArabicPrice.setVisibility(View.GONE);
        textEnglishPrice.setVisibility(View.VISIBLE);
        resultArabic.setVisibility(View.GONE);
        resultEnglish.setVisibility(View.VISIBLE);
        calview.setText("Calculate");

    }

    public void silver_calculate(View view)
    {
        float price=0,weight=0.0f;
        editTextPrice=(EditText)findViewById(R.id.edit_silver_price);
        editTextWeight=(EditText)findViewById(R.id.edit_weight_silver);
        result_view=(TextView)findViewById(R.id.text_result_silver);
        String sPrice=editTextPrice.getText().toString();
        String sWeight=editTextWeight.getText().toString();
        if(Lang.equals("arabic"))
        {
            result_view.setTypeface(custom_font_arabic);
        }
        if(!sPrice.isEmpty()&&!sWeight.isEmpty())
        {
            price=Float.valueOf(sPrice);
            weight=Float.valueOf(sWeight);
        }
        if(weight<595)
        {
            if(Lang.equals("arabic"))
            {
                Toast.makeText(this, "لا يوجد زكاة اقل من 595 جرام فضة", Toast.LENGTH_LONG).show();
            }
            else
            {
                Toast.makeText(this,"there is no alms for less than 595 gram",Toast.LENGTH_LONG).show();
            }
        }
        float r=(price*weight)/40;
        result_view.setText(r+"");
    }
}
