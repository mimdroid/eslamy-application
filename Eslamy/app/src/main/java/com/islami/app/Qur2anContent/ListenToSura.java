package com.islami.app.Qur2anContent;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.viewpagerindicator.CirclePageIndicator;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import ConnectionHandler.Connector;
import ConnectionHandler.Qare2;
import DataBase.DBInterface;
import com.islami.app.R;

public class ListenToSura extends AppCompatActivity
        implements  View.OnTouchListener,MediaPlayer.OnCompletionListener,  MediaPlayer.OnBufferingUpdateListener{

    MediaPlayer mediaPlayer;
    SeekBar seekBar;
    private final Handler handler = new Handler();
    Connector c;
    private long mediafilelengthinmilliseconeds;

    LinearLayout parent;
    ViewPager viewPager;
    List<LinearLayout> fragments=new ArrayList<>();
    CirclePageIndicator mIndicator;
    LayoutInflater inflater;
    Thread thread;
    ArrayList<Qare2> allQura2;
    ListenToSura thisActivity;
    Button mute,play,stop;
    int suraid;
    String ssuraid;
    int SM;
    int SH;
    DBInterface dbInterface;
    surasPagerAdapter adapter=null;

    int mashaher[]={4,205,5,9,17,21,30,31,53,52,51,54,70,81,102,106,111 ,112,114,113,118,122,121,120,119,124 ,123 ,125 };
    Spinner categories;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.listen_to_sura);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        viewPager = (ViewPager) findViewById(R.id.pager);
        mIndicator = (CirclePageIndicator)findViewById(R.id.indicator);
        TextView suraname=(TextView)findViewById(R.id.suraname);
        ssuraid=getIntent().getStringExtra("suraid");
        categories=(Spinner)findViewById(R.id.qura2spinner);
        dbInterface=new DBInterface(this);

        suraid=Integer.valueOf(ssuraid);
        suraname.setText(DataNeeded.EnSuras[suraid-1]);
        thisActivity=this;
        this.inflater=getLayoutInflater();

        initializeComponents();



        Toast.makeText(thisActivity,"Preparing all Quraa2 list",Toast.LENGTH_SHORT).show();

        if (allQura2==null){
        thread=new Thread(new Runnable() {
            @Override
            public void run() {
                c=new Connector(thisActivity);
                if(!c.isOnline()){
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(getApplicationContext(), "no Internet Connection ...", Toast.LENGTH_SHORT).show();
                        }
                    });
                    return;
                }

                allQura2=c.getAllQura2byLangURL(EnQura2URL);
               if (thread==null)return;
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        thisActivity.PrepareQura2();
                        LoadPager(0,"");
                    }
                });

            }
        });
        thread.start();
        }

        AdView mAdView = (AdView) findViewById(R.id.adViewmain);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);
    }


    public void initializeComponents() {

       // play = (Button) findViewById(R.id.playmp3);
        play=(Button)findViewById(R.id.playqare2);
        stop=(Button)findViewById(R.id.stopqare2);
        mute=(Button)findViewById(R.id.mute);

        mute.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mediaPlayer==null||mediaPlayer.isPlaying()==false)return;

               if (mediaPlayer.isPlaying()){
                   mute.setBackgroundResource(R.drawable.mic);
                   //mute
                   mediaPlayer.setVolume(0,0);

               }else {
                   mute.setBackgroundResource(R.drawable.mute);
                   mediaPlayer.setVolume(1,1);
                   //unmute
               }
               //mute

            }
        });

        if (mediaPlayer!=null&&mediaPlayer.isPlaying())mediaPlayer.stop();
        mediaPlayer = new MediaPlayer();
        seekBar = (SeekBar) findViewById(R.id.seekbar);

        seekBar.setOnTouchListener(this);
        mediaPlayer.setOnCompletionListener(this);
        mediaPlayer.setOnBufferingUpdateListener(this);

        ArrayList<String> content=new ArrayList<>();
        content.add("Famous Recitations");
        content.add("Almusshaf Al Mo'lim");
        content.add("Rewayat Hafs A'n Assem");
        content.add("Rewayat Warsh A'n Nafi'");
        content.add("Rewayat Assosi A'n Abi Amr");
        content.add("Rewayat Khalaf A'n Hamzah");
        content.add("Rewayat Qalon A'n Nafi'");
        content.add("Rewayat Albizi and Qunbol A'n Ibn Katheer");
        content.add("Rewayat AlDorai A'n Al-Kisa'ai");


        for (char i='A';i<='Z';i++){
            content.add(i+"");
        }

      //  ArrayAdapter adapter=ArrayAdapter.createFromResource(this,content,R.layout.spinner_item);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                R.layout.spinner_item,content);//setting the country_array to spinner
        adapter.setDropDownViewResource(R.layout.spinner_item);
        categories.setAdapter(adapter);

        categories.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (allQura2!=null&&allQura2.size()!=0){
                if (position==0)
                    LoadPager(0,"");
                else if (position>=1&&position<=8){
                        LoadPager(1,categories.getSelectedItem().toString());
                    }
                else LoadPager(2,categories.getSelectedItem().toString());
                }
else {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(getApplicationContext(),"no qura2 loaded ...",Toast.LENGTH_LONG).show();
                        }
                    });
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        stop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mediaPlayer!=null)
                    mediaPlayer.stop();
                play.setBackgroundResource(R.drawable.play);
                mediaPlayer=null;
                Date d=new Date();
                int ch= d.getHours();
                int cm=d.getMinutes();
                int total;
                if (ch==SH&&cm==SM)return;
                if (ch==SH)total=cm-SM;
                else total=((ch-SH)*60)+(cm-SM);
                Date from=new Date();
                dbInterface.insertQuranListen(total,(long)(from.getDate())+((from.getMonth()+1)*12)+(from.getYear()*365));
                SM=cm;
                SH=ch;

            }
        });
        play.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (c==null||c.isOnline()==false){
                    Toast.makeText(thisActivity,"No Internet Connection",Toast.LENGTH_SHORT).show();return;}
                if((thread!=null&&thread.isAlive()))
                    Toast.makeText(thisActivity,"please wait until loading",Toast.LENGTH_SHORT).show();
                else if (allQura2==null||allQura2.size()==0)Toast.makeText(thisActivity,"please wait until loading",Toast.LENGTH_SHORT).show();

                else if (mediaPlayer==null){
                    int index=viewPager.getCurrentItem();
                    play.setBackgroundResource(R.drawable.pause);
                    startStreaming(index);
                }
                else if (mediaPlayer.isPlaying()){
                    play.setBackgroundResource(R.drawable.play);
                    mediaPlayer.pause();
                    Date d=new Date();
                    int ch= d.getHours();
                    int cm=d.getMinutes();
                    int total;
                    if (ch==SH&&cm==SM)return;
                    if (ch==SH)total=cm-SM;
                    else total=((ch-SH)*60)+(cm-SM);
                    Date from=new Date();
                    dbInterface.insertQuranListen(total,(long)(from.getDate())+((from.getMonth()+1)*12)+(from.getYear()*365));

                }else if (!mediaPlayer.isPlaying()){
                    mediaPlayer.start();
                    play.setBackgroundResource(R.drawable.pause);
                    Date d=new Date();
                    SH= d.getHours();
                    SM=d.getMinutes();

                }
            }
        });


    }
int oldindex=-1;
    public void startStreaming(int index){
        if (oldindex!=index){
            if(mediaPlayer!=null&&mediaPlayer.isPlaying())mediaPlayer.stop();
           oldindex=index;
            mediaPlayer=new MediaPlayer();
        }
        while (ssuraid.length()!=3)ssuraid="0"+ssuraid;
        try {
            Log.e("link",current.get(index).Server+"/"+ssuraid);
            mediaPlayer.setDataSource(current.get(index).Server+"/"+ssuraid+".mp3"); // setup song from http://www.hrupin.com/wp-content/uploads/mp3/testsong_20_sec.mp3 URL to mediaplayer data source
            mediaPlayer.prepareAsync(); // you must call this method after setup the datasource in setDataSource method. After calling prepare() the instance of MediaPlayer starts load data from URL to internal buffer.
         mediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
             @Override
             public void onPrepared(MediaPlayer mp) {
                 mediafilelengthinmilliseconeds = mediaPlayer.getDuration(); // gets the song length in milliseconds from URL
                 mediaPlayer.start();
                 Date d=new Date();
                 SH=d.getHours();
                 SM=d.getMinutes();
                 primarySeekBarProgressUpdater();
             }
         });
            Log.e("awel","preb");
        } catch (Exception e) {
            e.printStackTrace();
        }


        if (!mediaPlayer.isPlaying()) {
            mediaPlayer.start();
            //play.setImageResource(R.drawable.button_pause);
        } else {
            mediaPlayer.pause();
            //  buttonPlayPause.setImageResource(R.drawable.button_play);
        }


    }
    private void primarySeekBarProgressUpdater() {

        seekBar.setProgress((int) (((float) mediaPlayer.getCurrentPosition() / mediafilelengthinmilliseconeds) * 100)); // This math construction give a percentage of "was playing"/"song length"
        if (mediaPlayer.isPlaying()) {
            Runnable notification = new Runnable() {
                public void run() {
                    primarySeekBarProgressUpdater();
                }
            };
            handler.postDelayed(notification, 1000);
        }
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        if(v.getId() == R.id.seekbar){
            /** Seekbar onTouch event handler. Method which seeks MediaPlayer to seekBar primary progress position*/
            if(mediaPlayer.isPlaying()){
                SeekBar sb = (SeekBar)v;
                long playPositionInMillisecconds = (mediafilelengthinmilliseconeds / 100) * sb.getProgress();
                mediaPlayer.seekTo((int)playPositionInMillisecconds);
            }
        }
        return false;
    }
    @Override
    public void onCompletion(MediaPlayer mp) {
        /** MediaPlayer onCompletion event handler. Method which calls then song playing is complete*/
        //   buttonPlayPause.setImageResource(R.drawable.button_play);
    }

    @Override
    public void onBufferingUpdate(MediaPlayer mp, int percent) {
        /** Method which updates the SeekBar secondary progress by current song loading from URL position*/
        seekBar.setSecondaryProgress(percent);
    }

    String EnQura2URL="http://www.mp3quran.net/api/_english.json";
    String ArQura2URL="http://www.mp3quran.net/api/_arabic.json";
    public void PrepareQura2(){
        Toast.makeText(thisActivity,"Done Loading Quraa2",Toast.LENGTH_LONG).show();
    }


    public Bitmap LoadQare2Image(int qid){
        Log.e("loading image",qid+"");
        try {
         InputStream s=getAssets().open("sheokh/"+qid +".jpg");
            byte[]im=new byte[s.available()];
            s.read(im);
          Bitmap b=  BitmapFactory.decodeByteArray(im,0,im.length);
      return b;
        }catch (IOException e){
            return null;
        }

      }
    public boolean is_famous(int id){
        for (int i=0;i<mashaher.length;i++){
            if (mashaher[i]==id)return true;
        }
        return false;

    }
    ArrayList<Qare2>current;
    public void LoadPager(int type,String typeparam){
        fragments=new ArrayList<>();
        current=new ArrayList<>();
        Log.e("ee","tmam");
        if (type==0){//mashaheer
            Qare2 q;
            for (int i=0;i<allQura2.size();i++) {
                q=allQura2.get(i);
                if (q.ContainSura(suraid)&&is_famous(q.id)) {
                  Log.e("qare2",q.name);
                    parent = (LinearLayout) inflater.inflate(R.layout.qare2_pager_item, null);
                    TextView qare2Name = (TextView) parent.findViewById(R.id.qare2Name);
                    qare2Name.setText(q.name + "\n" + q.rewaya);
                    ImageView qare2Image = (ImageView) parent.findViewById(R.id.qare2Sora);
                    qare2Image.setImageBitmap(LoadQare2Image(q.id));
                    //suraname.setText(x+"");
                    current.add(q);
                    fragments.add(parent);
                }
            }

        }else if (type==1){
            Qare2 q;
            for (int i=0;i<allQura2.size();i++){
                q=allQura2.get(i);
                if (q.rewaya.equals(typeparam)){
                    parent = (LinearLayout) inflater.inflate(R.layout.qare2_pager_item, null);
                    TextView qare2Name = (TextView) parent.findViewById(R.id.qare2Name);
                    qare2Name.setText(q.name + "\n" + q.rewaya);
                    ImageView qare2Image = (ImageView) parent.findViewById(
                            R.id.qare2Sora);
                    if (is_famous(q.id))
                    qare2Image.setImageBitmap(LoadQare2Image(q.id));
                    fragments.add(parent);
                    current.add(q);

                }
            }



        }else {
            Qare2 q;
                for (int i=0;i<allQura2.size();i++) {
                    q=allQura2.get(i);
                    if (q.ContainSura(suraid)&&q.letter.equals(typeparam)) {
                        parent = (LinearLayout) inflater.inflate(R.layout.qare2_pager_item, null);
                        TextView qare2Name = (TextView) parent.findViewById(R.id.qare2Name);
                        qare2Name.setText(allQura2.get(i).name + "\n" + allQura2.get(i).rewaya);
                        ImageView qare2Image = (ImageView) parent.findViewById(R.id.qare2Sora);
                        if (is_famous(q.id))
                            qare2Image.setImageBitmap(LoadQare2Image(q.id));
                        fragments.add(parent);
                        current.add(q);
                    }
            }

        }

            adapter=new surasPagerAdapter();
            viewPager.setAdapter(adapter);
            mIndicator.setViewPager(viewPager);

    }

    public void BuildNotification(){



    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (thread!=null&&thread.isAlive()){
            Thread another=thread;
            thread=null;
            another.interrupt();
        }
  if (mediaPlayer!=null&&mediaPlayer.isPlaying()){
    //  BuildNotification();
  }
        StopMedia();

    }

    @Override
    protected void onStop() {
        super.onStop();
        StopMedia();
    }

    public void StopMedia(){
        if (mediaPlayer!=null)
        mediaPlayer.stop();
    }

    private class surasPagerAdapter extends PagerAdapter {


        @Override
        public int getCount() {
            return fragments.size();
        }


        @Override
        public Object instantiateItem(ViewGroup collection, int position) {



            collection.addView(fragments.get(position),0);

            return fragments.get(position);
        }

        @Override
        public void destroyItem(ViewGroup collection, int position, Object view) {
            collection.removeView((LinearLayout) view);
        }

   @Override
        public boolean isViewFromObject(View view, Object object) {
            return (view==object);
        }


        @Override
        public void finishUpdate(ViewGroup arg0) {}


        @Override
        public void restoreState(Parcelable arg0, ClassLoader arg1) {}

        @Override
        public Parcelable saveState() {
            return null;
        }

        @Override
        public void startUpdate(ViewGroup arg0) {}
    }

}
