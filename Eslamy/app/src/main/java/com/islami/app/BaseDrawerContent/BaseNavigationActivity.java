package com.islami.app.BaseDrawerContent;

import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.islami.app.Qur2anContent.Qur2anActivity;
import com.islami.app.R;
import com.islami.app.Rosary.RosaryMain;
import com.islami.app.SalahContent.SalahMain;
import com.islami.app.ZakahContent.MainZakah;
import com.islami.app.elsomsection.SoomActivity;


/**
 * Created by Admin on 12/23/2015.
 */
public class BaseNavigationActivity extends AppCompatActivity{
    private DrawerLayout mDrawerLayout;
    MyExpandableListAdapter mMenuAdapter;
    ExpandableListView expandableList;
    List<navitemdata> listDataHeader;
    HashMap<String, List<submenuitem>> listDataChild;

   protected Fragment currentFragment;

    protected void onCreateDrawer(){
        expandableList=(ExpandableListView)findViewById(R.id.navigationmenu);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ImageView Home=(ImageView) findViewById(R.id.gotoHome);
        Home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getSupportFragmentManager().popBackStack();
            }
        });


        final DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();
        prepareListData();
        mMenuAdapter = new MyExpandableListAdapter (this,listDataHeader ,listDataChild, expandableList);

        View footerView =getLayoutInflater(). inflate(R.layout.nav_footer_home, expandableList, false);
        LinearLayout footer=(LinearLayout) footerView.findViewById(R.id.footerlayout);
        TextView callus=(TextView)footerView.findViewById(R.id.callus);
        callus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(),"aaaa",Toast.LENGTH_LONG).show();
            }
        });
        expandableList.addFooterView(footer);
        expandableList.setAdapter(mMenuAdapter);
        expandableList.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {

                android.support.v4.app.FragmentManager fragmentManager = getSupportFragmentManager();
                android.support.v4.app.FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

                if (groupPosition==0){//salah section
                    Qur2anActivity fragment = new Qur2anActivity();
                    currentFragment=fragment;

                   // fragment.setRetainInstance(true);
                    if(childPosition==0){
                        fragment.setTabnum(4);

                    }else if (childPosition==1){//2ebla
                        fragment.setTabnum(3);

                    }else if (childPosition==2){//msaged
                        fragment.setTabnum(2);

                    }else if (childPosition==3){//azkar salah
                        fragment.setTabnum(1);

                    }else if (childPosition==4){//e7sa2yat
                        fragment.setTabnum(0);

                    }
                    getSupportFragmentManager().popBackStack();
                    fragmentTransaction.replace(R.id.contentFrame, fragment);
                    fragmentTransaction.addToBackStack(null);
                    fragmentTransaction.commit();
                    drawer.closeDrawer(GravityCompat.START);

                }
               else if (groupPosition==1){//salah section
                    SalahMain fragment=new SalahMain();
                  if(childPosition==0){//mnabeh
                        fragment.setTabnum(4);
                  }else if (childPosition==1){//2ebla
                      fragment.setTabnum(3);
                  }else if (childPosition==2){//msaged
                      fragment.setTabnum(2);
                  }else if (childPosition==3){//azkar salah
                      fragment.setTabnum(1);
                  }else if (childPosition==4){//e7sa2yat
                      fragment.setTabnum(0);
                  }

                    fragmentTransaction.replace(R.id.contentFrame, fragment);
                    fragmentTransaction.addToBackStack(null);
                    fragmentTransaction.commit();
                    drawer.closeDrawer(GravityCompat.START);

                }else if (groupPosition==2) {//soom
                    SoomActivity fragment = new SoomActivity();
                    currentFragment=fragment;
                    if (childPosition == 0) {//mnabeh
                            fragment.setTabnum(0);
                    } else if (childPosition == 1) {//2ebla
                        fragment.setTabnum(1);
                    }
                    fragmentTransaction.replace(R.id.contentFrame, fragment);
                    fragmentTransaction.addToBackStack(null);
                    fragmentTransaction.commit();
                    drawer.closeDrawer(GravityCompat.START);

                }else if (groupPosition==3) {//Rosary
                    RosaryMain fragment = new RosaryMain();
                    currentFragment=fragment;
                    if (childPosition == 0) {//mnabeh
                        fragment.setTabnum(2);
                    } else if (childPosition == 1) {//2ebla
                        fragment.setTabnum(1);
                    }else if (childPosition==2){
                        fragment.setTabnum(0);
                    }
                    fragmentTransaction.replace(R.id.contentFrame, fragment);
                    fragmentTransaction.addToBackStack(null);
                    fragmentTransaction.commit();
                    drawer.closeDrawer(GravityCompat.START);

                }
                else if (groupPosition==4) {//Zakah
                    MainZakah fragment = new MainZakah();
                    currentFragment=fragment;
                    if (childPosition == 0) {
                        fragment.setTabnum(2);
                    } else if (childPosition == 1) {
                        fragment.setTabnum(1);
                    }else if (childPosition==2){
                        fragment.setTabnum(0);
                    }
                    fragmentTransaction.replace(R.id.contentFrame, fragment);
                    fragmentTransaction.addToBackStack(null);
                    fragmentTransaction.commit();
                    drawer.closeDrawer(GravityCompat.START);

                }
                return false;
            }
        });

    }

    private void prepareListData() {
        listDataHeader = new ArrayList<navitemdata>();
        listDataChild = new HashMap<String,List<submenuitem> >();

        // Adding data header
        listDataHeader.add(new navitemdata("القران الكريم",R.drawable.quranslider));
        listDataHeader.add(new navitemdata("الصلاه",R.drawable.salahslider) );
        listDataHeader.add(new navitemdata("الصوم",R.drawable.soomslider));
        listDataHeader.add(new navitemdata("الذكر",R.drawable.zekrslider));
        listDataHeader.add(new navitemdata("الزكاه",R.drawable.zakahslider));
        listDataHeader.add(new navitemdata("ادوات",R.drawable.adwatslider));
        listDataHeader.add(new navitemdata("محادثات",R.drawable.chatslider));
        listDataHeader.add(new navitemdata("ادعو صديق",R.drawable.addfriendslider));

        // listDataHeader.add(new navitemdata("محادثات",R.drawable.chatslider));
        // listDataHeader.add(new navitemdata("اضافة صديق",R.drawable.addfriendslider));

        // Adding child data
        List<submenuitem> heading1= new ArrayList<submenuitem>();
        heading1.add(new submenuitem("المصحف الصوتي",R.drawable.play));
        heading1.add(new submenuitem("المصحف قراءه",R.drawable.quranabyad));
        heading1.add(new submenuitem("الترجمه و التفسير",R.drawable.translation));
        heading1.add(new submenuitem("بحث عن السور  و الايات",R.drawable.search));
        heading1.add(new submenuitem("احصائيات",R.drawable.stat));


        List<submenuitem> heading2= new ArrayList<submenuitem>();
        heading2.add(new submenuitem("منبه مواعيد الصلاه",R.drawable.alarm));
        heading2.add(new submenuitem("اتجاه القبله",R.drawable.kebla));
        heading2.add(new submenuitem("مساجد حولي",R.drawable.mosques));
        heading2.add(new submenuitem("اذكار الصلاه",R.drawable.seb7abeda));
        heading2.add(new submenuitem("احصائيات",R.drawable.stat));


        List<submenuitem> heading3= new ArrayList<submenuitem>();
        heading3.add(new submenuitem("شهر رمضان",R.drawable.helal));
        heading3.add(new submenuitem("ايام السنه",R.drawable.calender));
        heading3.add(new submenuitem("احصائيات",R.drawable.stat));

        List<submenuitem> heading4= new ArrayList<submenuitem>();
        heading4.add(new submenuitem("المسبحه الالكترونيه",R.drawable.seb7abeda));
        heading4.add(new submenuitem("منبه التسبيح",R.drawable.alarm));
        heading4.add(new submenuitem("احصائيات",R.drawable.stat));

        List<submenuitem> heading5= new ArrayList<submenuitem>();
        heading5.add(new submenuitem("حاسبة الزكاه",R.drawable.calculator));
        heading5.add(new submenuitem("مواضيع الزكاه",R.drawable.zakahslider));
        heading5.add(new submenuitem("احصائيات",R.drawable.stat));

        List<submenuitem> heading6= new ArrayList<submenuitem>();
        heading6.add(new submenuitem("حصن المسلم",R.drawable.hesnmoslem));
        heading6.add(new submenuitem("تفسير الاحلام",R.drawable.tafseera7lam));
        heading6.add(new submenuitem("الرقيه الشرعيه",R.drawable.quranabyad));
        heading6.add(new submenuitem("التاريخ الهجري",R.drawable.calender));
        heading6.add(new submenuitem("اسلاميات اناشيد",R.drawable.play));
        heading6.add(new submenuitem("احائيث مختاره",R.drawable.search));
        heading6.add(new submenuitem("مناسك الحج و العمره",R.drawable.hegwe3omra));



        listDataChild.put(listDataHeader.get(0).toString(), heading1);// Header, Child data
        listDataChild.put(listDataHeader.get(1).toString(), heading2);
        listDataChild.put(listDataHeader.get(2).toString(), heading3);
        listDataChild.put(listDataHeader.get(3).toString(), heading4);
        listDataChild.put(listDataHeader.get(4).toString(), heading5);
        listDataChild.put(listDataHeader.get(5).toString(), heading6);

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }



}
