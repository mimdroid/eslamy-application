package com.islami.app.CarouselContent;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nineoldandroids.view.ViewHelper;

import java.io.IOException;
import java.io.InputStream;

import com.islami.app.HomeActivity;
import com.islami.app.R;

public class MyFragment extends Fragment {
	
	public static Fragment newInstance(HomeActivity context, int ResID,
									   float scale, boolean IsBlured,String name)
	{
		
		Bundle b = new Bundle();
		b.putInt("ResID",ResID);
		b.putString("name",name);
		b.putFloat("scale", scale);
		b.putBoolean("IsBlured", IsBlured);
		return Fragment.instantiate(context, MyFragment.class.getName(), b);
	}

    int ids[]={6,7,8,9,11,12,14,15,16,17,18,20,26,28,29};
    public boolean hasImage(int id){
      for (int i:ids){
       if (i==id)return true;

      }
        return false;
    }
    public Bitmap LoadImageFromAsset(int qid){
        try {
            InputStream s=getActivity().getAssets().open("Radio/"+qid +".jpg");
            byte[]im=new byte[s.available()];
            s.read(im);
            Bitmap b=  BitmapFactory.decodeByteArray(im, 0, im.length);
            return b;
        }catch (IOException e){
            return null;
        }

    }
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		if (container == null) {
			return null;
		}
		
		LinearLayout l = (LinearLayout)
				inflater.inflate(R.layout.mf, container, false);
		
		int ResID = this.getArguments().getInt("ResID");
		String name=this.getArguments().getString("name");
		TextView tv = (TextView) l.findViewById(R.id.viewID);
		tv.setText(name);
		ImageView im=(ImageView)l.findViewById(R.id.content);
       	if (hasImage(ResID))
        im.setImageBitmap(LoadImageFromAsset(ResID));
		MyLinearLayout root = (MyLinearLayout) l.findViewById(R.id.root);
		float scale = this.getArguments().getFloat("scale");
		root.setScaleBoth(scale);
		boolean isBlured=this.getArguments().getBoolean("IsBlured");

		if(isBlured)
		{
			ViewHelper.setAlpha(root,MyPagerAdapter.getMinAlpha());
			ViewHelper.setRotationY(root, MyPagerAdapter.getMinDegree());
		}
		return l;
	}
}
