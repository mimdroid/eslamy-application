package com.islami.app.ZakahContent;

import android.app.Activity;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.islami.app.R;


public class MoneyZakah extends Activity {
    EditText editText;
    TextView result_view;
    String Lang="arabic";
    TextView textArabic;
    TextView textEnglish;
    TextView goldArabic;
    TextView goldEnglish;
    TextView resultArabic;
    TextView resultEnglish;
    TextView calview;
    Typeface custom_font_arabic;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.money_zakah);
         custom_font_arabic= Typeface.createFromAsset(getAssets(), "fonts/GESSTwoLight.otf");
         textArabic=(TextView)findViewById(R.id.money_text_arabic);
         textEnglish=(TextView)findViewById(R.id.money_text_english);
         goldArabic=(TextView)findViewById(R.id.money_gold_text_arabic);
         goldEnglish=(TextView)findViewById(R.id.money_gold_text_english);
         resultArabic=(TextView)findViewById(R.id.money_result_arabic);
         resultEnglish=(TextView)findViewById(R.id.money_result_english);
         calview=(TextView)findViewById(R.id.money_calculate);
         textArabic.setTypeface(custom_font_arabic);
         goldArabic.setTypeface(custom_font_arabic);
         resultArabic.setTypeface(custom_font_arabic);
        //Lang="english";
        if(Lang.equals("arabic"))
        {
            calview.setTypeface(custom_font_arabic);
        }
        if(Lang.equals("english"))
        {
            changeLang();
        }

    }
    public void changeLang()
    {
        goldArabic.setVisibility(View.GONE);
        goldEnglish.setVisibility(View.VISIBLE);
        textArabic.setVisibility(View.GONE);
        textEnglish.setVisibility(View.VISIBLE);
        calview.setText("Calculate");

    }
    public void money_calculate(View view)
    {
        editText=(EditText)findViewById(R.id.edit_money);
        result_view=(TextView)findViewById(R.id.text_result_money);
        EditText gold=(EditText)findViewById(R.id.edit_money_gold);
        String s=editText.getText().toString();
        String sGold=gold.getText().toString();
        if(Lang.equals("arabic"))
        {
            result_view.setTypeface(custom_font_arabic);
        }
        if(sGold.isEmpty())
        {
            if(Lang.equals("arabic"))
            {
                Toast.makeText(this,"يجب ادخال سعر الذهب عيار 24 قيراط",Toast.LENGTH_LONG).show();

            }
            else
            {
                Toast.makeText(this,"please enter price of gold",Toast.LENGTH_LONG).show();
            }
        }
        else if(s.isEmpty()==false)
        {
            float x=Float.valueOf(s);
            //x=x/40;
            if(x>=Float.valueOf(sGold)*85)
            result_view.setText((x/40)+"");
            else
            {
                if(Lang.equals("arabic"))
                {
                    Toast.makeText(this,"مالك أقل من 85 جرام ذهب فلا يجوز اخراج زكاة", Toast.LENGTH_LONG).show();
                }
                else
                {
                    Toast.makeText(this,"your money less than 85 gram of gold there is no alms for you",Toast.LENGTH_LONG).show();
                }
            }
        }
        else
        {
            if(Lang.equals("arabic"))
            {
                Toast.makeText(this,"من فضلك ادخل قيمة المال", Toast.LENGTH_LONG).show();
            }
            else
            {
                Toast.makeText(this,"please enter your money",Toast.LENGTH_LONG).show();
            }
        }

    }


}
