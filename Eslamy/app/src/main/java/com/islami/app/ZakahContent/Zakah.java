package com.islami.app.ZakahContent;

import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import Manage.Reader;
import com.islami.app.R;


public class Zakah extends Fragment {
    TextView zakahSection;
    String Lang="arabic";
    Typeface custom_font_arabic;
    TextView The_concept_of_Zakat;
    TextView Meaning_linguistic_and_legislative;
    TextView Zakat_in_the_Holy_Quran;
    TextView Zakat_in_the_Sunnah;
    TextView Conditions_should_be_zakat;
    TextView Zakat_Fitr;
    TextView Zakah_banks;
    TextView Zakat_Calculator;

    Context context;

    public Zakah()
    {

    }
    @Override
    public void onCreate( Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.zakah, container, false);
        context=view.getContext();
        custom_font_arabic= Typeface.createFromAsset(context.getAssets(), "fonts/GESSTwoLight.otf");
        zakahSection=(TextView)view.findViewById(R.id.zakah_text);
        The_concept_of_Zakat=(TextView)view.findViewById(R.id.The_concept_of_Zakat);
        Meaning_linguistic_and_legislative=(TextView)view.findViewById(R.id.Meaning_linguistic_and_legislative);
        Zakat_in_the_Holy_Quran=(TextView)view.findViewById(R.id.Zakat_in_the_Holy_Quran);
        Zakat_in_the_Sunnah=(TextView)view.findViewById(R.id.Zakat_in_the_Sunnah);
        Conditions_should_be_zakat=(TextView)view.findViewById(R.id.Conditions_should_be_zakat);
        Zakat_Fitr=(TextView)view.findViewById(R.id.Zakat_Fitr);
        Zakah_banks=(TextView)view.findViewById(R.id.Zakah_banks);
        Zakat_Calculator=(TextView)view.findViewById(R.id.Zakat_Calculator);
       // Lang="english";
        if(Lang.equals("arabic"))
        {
            zakahSection.setTypeface(custom_font_arabic);
            The_concept_of_Zakat.setTypeface(custom_font_arabic);
            Meaning_linguistic_and_legislative.setTypeface(custom_font_arabic);
            Zakat_in_the_Holy_Quran.setTypeface(custom_font_arabic);
            Zakat_in_the_Sunnah.setTypeface(custom_font_arabic);
            Conditions_should_be_zakat.setTypeface(custom_font_arabic);
            Zakat_Fitr.setTypeface(custom_font_arabic);
            Zakah_banks.setTypeface(custom_font_arabic);
            Zakat_Calculator.setTypeface(custom_font_arabic);
            String file="Zakah/The_concept_of_Zakat_arabic.txt";
            Reader reader=new Reader(context);
            String result=reader.readFromFile(file);
            zakahSection.setText(result);
        }
        if(Lang.equals("english"))
        {
            changeLang();
        }
        Call();
        return view;
    }
    public void Call()
    {
         Meaning_linguistic_and_legislative.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View v) {

                 String file="Zakah/Meaning_linguistic_and_legislative_arabic.txt";
                 Reader reader=new Reader(context);
                 String result=reader.readFromFile(file);
                 zakahSection.setText(result);
             }
         });
        The_concept_of_Zakat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String file="Zakah/The_concept_of_Zakat_arabic.txt";
                Reader reader=new Reader(context);
                String result=reader.readFromFile(file);
                zakahSection.setText(result);
            }
        });
        Zakat_in_the_Sunnah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String file="Zakah/Zakat_in_the_Sunnah_arabic.txt";
                Reader reader=new Reader(context);
                String result=reader.readFromFile(file);
                zakahSection.setText(result);
            }
        });
        Zakat_in_the_Holy_Quran.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String file="Zakah/Zakat_in_the_Holy_Quran_arabic.txt";
                Reader reader=new Reader(context);
                String result=reader.readFromFile(file);
                zakahSection.setText(result);
            }
        });
        Conditions_should_be_zakat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String file="Zakah/Conditions_should_be_zakat_arabic.txt";
                Reader reader=new Reader(context);
                String result=reader.readFromFile(file);
                zakahSection.setText(result);
            }
        });
        Zakat_Fitr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String file="Zakah/Zakat_Fitr_arabic.txt";
                Reader reader=new Reader(context);
                String result=reader.readFromFile(file);
                zakahSection.setText(result);
            }
        });
        Zakah_banks.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String file="Zakah/Zakah_banks_arabic.txt";
                Reader reader=new Reader(context);
                String result=reader.readFromFile(file);
                zakahSection.setText(result);
            }
        });
        Zakat_Calculator.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String file="Zakah/Zakat_Calculator_arabic.txt";
                Reader reader=new Reader(context);
                String result=reader.readFromFile(file);
                zakahSection.setText(result);
            }
        });
    }
    public void changeLang()
    {
        The_concept_of_Zakat.setText("concept of Zakat");
        Meaning_linguistic_and_legislative.setText(" linguistic and legislative");
        Zakat_in_the_Holy_Quran.setText(" in the Holy Quran");
        Zakat_in_the_Sunnah.setText("Zakat in the Sunnah");
        Conditions_should_be_zakat.setText("Conditions of zakat");
        Zakat_Fitr.setText("Zakat Al-Fitr");
        Zakah_banks.setText("Zakat banks");
        Zakat_Calculator.setText("Zakat Calculator");
    }
    public void The_concept_of_Zakat(View view)
    {
        String file="Zakah/The_concept_of_Zakat_arabic.txt";
        Reader reader=new Reader(context);
        String result=reader.readFromFile(file);
        zakahSection.setText(result);
    }
    public void Meaning_linguistic_and_legislative(View view)
    {
        String file="Zakah/Meaning_linguistic_and_legislative_arabic.txt";
        Reader reader=new Reader(context);
        String result=reader.readFromFile(file);
        zakahSection.setText(result);
    }

    public void Zakat_in_the_Holy_Quran(View view)
    {
        String file="Zakah/Zakat_in_the_Holy_Quran_arabic.txt";
        Reader reader=new Reader(context);
        String result=reader.readFromFile(file);
        zakahSection.setText(result);
    }
    public void Zakat_in_the_Sunnah(View view)
    {
        String file="Zakah/Zakat_in_the_Sunnah_arabic.txt";
        Reader reader=new Reader(context);
        String result=reader.readFromFile(file);
        zakahSection.setText(result);
    }
    public void Conditions_should_be_zakat(View view)
    {
        String file="Zakah/Conditions_should_be_zakat_arabic.txt";
        Reader reader=new Reader(context);
        String result=reader.readFromFile(file);
        zakahSection.setText(result);
    }
    public void Zakat_Fitr(View view)
    {
        String file="Zakah/Zakat_Fitr_arabic.txt";
        Reader reader=new Reader(context);
        String result=reader.readFromFile(file);
        zakahSection.setText(result);
    }
    public void Zakah_banks(View view)
    {
        String file="Zakah/Zakah_banks_arabic.txt";
        Reader reader=new Reader(context);
        String result=reader.readFromFile(file);
        zakahSection.setText(result);
    }
    public void Zakat_Calculator(View view)
    {
        String file="Zakah/Zakat_Calculator_arabic.txt";
        Reader reader=new Reader(context);
        String result=reader.readFromFile(file);
        zakahSection.setText(result);
    }
}
