package com.islami.app.Rosary;


import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.Date;

import DataBase.DBInterface;
import Manage.Reader;
import com.islami.app.R;


public class RosaryActivity extends Fragment {
    Spinner spinner_environment;
    ImageButton refresh;
    TextView rosaryText,rosaryTotalText,rosaryCount,rosaryTotalCount,rosary,rosary_blessings;
    String Lang="arabic";
    Typeface custom_font_arabic;
    Date now = new Date();
    DBInterface dbInterface;
    int index=0;
    Context context;
    View view;
    public RosaryActivity()
    {

    }
    @Override
    public void onCreate( Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.activity_rosary, container, false);
        context = view.getContext();
        custom_font_arabic = Typeface.createFromAsset(context.getAssets(), "fonts/GESSTwoLight.otf");
        spinner_environment = (Spinner)view.findViewById(R.id.spinner1);
      //  custom_font_arabic= Typeface.createFromAsset(context.getAssets(), "fonts/GESSTwoLight.otf");
        setId();
        if(Lang.equals("arabic")) {
            ArrayAdapter adapter = ArrayAdapter.createFromResource(context, R.array.spinner_array_Rosary_arabic, R.layout.spinner_phone);
            adapter.setDropDownViewResource(R.layout.dropitem);
            spinner_environment.setAdapter(adapter);
            rosary_blessings.setTypeface(custom_font_arabic);
            rosaryText.setTypeface(custom_font_arabic);
            rosaryTotalText.setTypeface(custom_font_arabic);
            Reader reader=new Reader(context);
            String s=reader.readBlessingsArabic();
            rosary_blessings.setText(s);
        }
        else
        {
            ArrayAdapter adapter = ArrayAdapter.createFromResource(context, R.array.spinner_array_Rosary_english, R.layout.spinner_phone);
            adapter.setDropDownViewResource(R.layout.dropitem);
            spinner_environment.setAdapter(adapter);
            changeLang();
        }
        spinner_environment.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                long d=(now.getDate())+((now.getMonth()+1)*12)+(now.getYear()*365);
                Log.v("pause", dbInterface.InsertZaker(index, Integer.valueOf(rosaryCount.getText().toString()), d) + " " + rosaryCount.getText().toString());
                index=position;
                rosaryTotalCount.setText(dbInterface.Total_zaker(position)+"");
                rosaryCount.setText("0");
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        return view;
    }
    /*@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rosary);

    }*/
    public void changeLang()
    {
        Reader reader=new Reader(context);
        String s=reader.readBlessingEnglish();
        rosary_blessings.setText(s);
        rosaryText.setText("Citation");
        rosaryTotalText.setText("Total of Citation");
        ArrayAdapter adapter = ArrayAdapter.createFromResource(context, R.array.spinner_array_Rosary_english, R.layout.spinner_phone);
        adapter.setDropDownViewResource(R.layout.dropitem);
        spinner_environment.setAdapter(adapter);

    }
    public void setId()
    {
        rosary_blessings=(TextView)view.findViewById(R.id.rosary_blessings);
        refresh=(ImageButton)view.findViewById(R.id.rosary_refresh);
        rosaryText=(TextView)view.findViewById(R.id.rosary_text);
        rosaryCount=(TextView)view.findViewById(R.id.rosary_count);
        rosaryTotalCount=(TextView)view.findViewById(R.id.rosary_total_count);
        rosaryTotalText=(TextView)view.findViewById(R.id.rosary_total_text);
        dbInterface=new DBInterface(context);
        rosaryTotalCount.setText(dbInterface.Total_zaker(0)+"");
        refresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int count=Integer.valueOf(rosaryCount.getText().toString());
                count++;
                rosaryCount.setText(count+"");
                int total=Integer.valueOf(rosaryTotalCount.getText().toString());
                total++;
                rosaryTotalCount.setText(total+"");
       //         Log.v("test",dbInterface.InsertZaker(index, now.toString())+" ");

            }
        });
    }

    @Override
    public void onStop() {
        super.onStop();
        long d=(now.getDate())+((now.getMonth()+1)*12)+(now.getYear()*365);
        Log.v("pause",dbInterface.InsertZaker(index, Integer.valueOf(rosaryCount.getText().toString()), d)+" "+rosaryCount.getText().toString());

    }
    /*
    @Override
    protected void onPause() {
        super.onPause();
        long d=(now.getDate())+((now.getMonth()+1)*12)+(now.getYear()*365);
        Log.v("pause",dbInterface.InsertZaker(index, Integer.valueOf(rosaryCount.getText().toString()), d)+" "+rosaryCount.getText().toString());
    }

    @Override
    protected void onResume() {
        super.onResume();
    }*/
}
