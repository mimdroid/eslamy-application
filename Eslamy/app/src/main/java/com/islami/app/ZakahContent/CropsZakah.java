package com.islami.app.ZakahContent;

import android.app.Activity;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.islami.app.R;


public class CropsZakah extends Activity {
Boolean with=false;
    TextView withText,withoutText;
    EditText edit_crops;
    EditText edit_crops_quantity;
    TextView text_result_crops;
    String Lang="arabic";
    Typeface custom_font_arabic;
    TextView crops_text_arabic;
    TextView crops_text_english;
    TextView crops_quantity_text_arabic;
    TextView crops_quantity_text_english;
    TextView money_result_arabic;
    TextView money_result_english;

    TextView textView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
         super.onCreate(savedInstanceState);
         setContentView(R.layout.crops_zakah);
         custom_font_arabic= Typeface.createFromAsset(getAssets(), "fonts/GESSTwoLight.otf");
         withText=(TextView)findViewById(R.id.ray_with_cost);
         withoutText=(TextView)findViewById(R.id.ray_without_cost);
         crops_text_arabic=(TextView)findViewById(R.id.crops_text_arabic);
         crops_text_english=(TextView)findViewById(R.id.crops_text_english);
         crops_quantity_text_arabic=(TextView)findViewById(R.id.crops_quantity_text_arabic);
         crops_quantity_text_english=(TextView)findViewById(R.id.crops_quantity_text_english);
         money_result_arabic=(TextView)findViewById(R.id.money_result_arabic);
         money_result_english=(TextView)findViewById(R.id.money_result_english);
         textView=(TextView)findViewById(R.id.crops_calculate);
         crops_quantity_text_arabic.setTypeface(custom_font_arabic);
         crops_text_arabic.setTypeface(custom_font_arabic);
         money_result_arabic.setTypeface(custom_font_arabic);
        //Lang="english";
        if(Lang.equals("arabic"))
        {
            textView.setTypeface(custom_font_arabic);
            withoutText.setTypeface(custom_font_arabic);
            withText.setTypeface(custom_font_arabic);
        }
        if(Lang.equals("english"))
        {
            change_lang();
        }
    }
    public void change_lang()
    {
        crops_text_arabic.setVisibility(View.GONE);
        crops_text_english.setVisibility(View.VISIBLE);
        crops_quantity_text_arabic.setVisibility(View.GONE);
        crops_quantity_text_english.setVisibility(View.VISIBLE);
        money_result_arabic.setVisibility(View.GONE);
        money_result_english.setVisibility(View.VISIBLE);
        withText.setText("irrigation with cost");
        withoutText.setText("irrigation without cost");
        textView.setText("Calculate");
    }
    public void ray_without_cost(View view)
    {
    with=false;
    withText.setBackgroundColor(Color.parseColor("#46352B"));
    withoutText.setBackgroundColor(Color.parseColor("#EBC467"));

}
    public void ray_with_cost(View view)
    {
        with=true;
        withText.setBackgroundColor(Color.parseColor("#EBC467"));
        withoutText.setBackgroundColor(Color.parseColor("#46352B"));
    }
    public void crops_calculate(View view)
    {
        edit_crops=(EditText)findViewById(R.id.edit_crops);
        edit_crops_quantity=(EditText)findViewById(R.id.edit_crops_quantity);
        text_result_crops=(TextView)findViewById(R.id.text_result_crops);
        if(Lang.equals("arabic"))
        {
            text_result_crops.setTypeface(custom_font_arabic);
        }
        if(edit_crops_quantity.getText().toString().isEmpty())
        {
            if(Lang.equals("arabic"))
            {
                Toast.makeText(this,"من فضلك ادخل الكمية",Toast.LENGTH_LONG).show();
            }
            else
            {
                Toast.makeText(this,"please enter the quantity",Toast.LENGTH_LONG).show();
            }
        }
        float quantity=Float.valueOf(edit_crops_quantity.getText().toString());
        if(quantity<612)
        {
            if(Lang.equals("arabic"))
            {
                Toast.makeText(this,"لا يوجد زكاة لاقل من 612 كجم",Toast.LENGTH_LONG).show();
            }
            else
            {
                Toast.makeText(this,"there is no alms for less than 612 kg",Toast.LENGTH_LONG).show();
            }
            return;
        }
        else
        {
            float r=0;
            if(with==true)
            {
                r=quantity/20;
            }
            else
            {
                r=quantity/10;
            }
            if(Lang.equals("arabic"))
            text_result_crops.setText(r+" كجم");
            else
                text_result_crops.setText(r+" km");

        }

    }
}
