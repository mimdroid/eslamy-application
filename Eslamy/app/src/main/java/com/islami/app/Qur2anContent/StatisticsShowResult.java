package com.islami.app.Qur2anContent;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.Date;

import DataBase.DBInterface;
import com.islami.app.R;

/**
 * Created by Admin on 1/13/2016.
 */
public class StatisticsShowResult extends Fragment
{
    SharedPreferences sharedPreferences;
    String Lang="arabic";
    Context context;
    View view;
    Typeface custom_font_arabic;

    Date from,to;
    Spinner spinnerFrom;
    Spinner spinnerTo;
    EditText dayFrom,dayTo,yearFrom,yearTo;
    int monthFrom,monthTo;
    TextView mins,ayas,hefz;


    @Override
    public void onCreate( Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
      //  setRetainInstance(true);
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.q_stat_show, container, false);
        context = view.getContext();
        custom_font_arabic = Typeface.createFromAsset(context.getAssets(), "fonts/GESSTwoLight.otf");
        dayFrom=(EditText)view.findViewById(R.id.dayFrom);
        dayTo=(EditText)view.findViewById(R.id.dayTo);
        yearFrom=(EditText)view.findViewById(R.id.yearFrom);
        yearTo=(EditText)view.findViewById(R.id.yearTo);
        from=new Date();
        to=new Date();
        dayFrom.setText(from.getDate()+"");
        dayTo.setText(from.getDate()+"");
        yearTo.setText((from.getYear()+1900)+"");
        yearFrom.setText((from.getYear()+1900)+"");
        monthFrom=from.getMonth();
        monthTo=monthFrom;
        ArrayAdapter adapter = ArrayAdapter.createFromResource(context, R.array.month_english, R.layout.spinner_phone);
        adapter.setDropDownViewResource(R.layout.dropitem);
        spinnerFrom=(Spinner)view.findViewById(R.id.spinnerFrom);
        spinnerTo=(Spinner)view.findViewById(R.id.spinnerTo);
        spinnerFrom.setAdapter(adapter);
        spinnerTo.setAdapter(adapter);
        spinnerFrom.setSelection(monthFrom);
        spinnerTo.setSelection(monthTo);
/*******************************************************************/
        spinnerFrom.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                monthFrom=position;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
        spinnerTo.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                monthTo=position;
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        mins=(TextView)view.findViewById(R.id.q_stat_mins);
        ayas=(TextView)view.findViewById(R.id.q_stats_ayas);
        hefz=(TextView)view.findViewById(R.id.q_stats_hefz);

        sharedPreferences=context.getSharedPreferences("QuranStats", Context.MODE_PRIVATE);


        Lang="english";
        //    if(Lang.equals("english"))
        //changeLang();
        Call();
        return view;

    }



    public void Call()
    {
        Button show_res=(Button)view.findViewById(R.id.show_zaker);
        show_res.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Show();
            }
        });

    }
    public void Show()
    {
        Log.e("show Stat","enter");
        int day1=Integer.valueOf(dayFrom.getText().toString());
        int day2=Integer.valueOf(dayTo.getText().toString());
        int year1=Integer.valueOf(yearFrom.getText().toString());
        int year2=Integer.valueOf(yearTo.getText().toString());
        if(day1<0||day1>31)day1=1;
        if(day2<0||day2>31)day2=1;
        from.setDate(day1);
        to.setDate(day2);
        from.setMonth(monthFrom);
        to.setMonth(monthTo);
        from.setYear(year1-1900);
        to.setYear(year2-1900);
        DBInterface dbInterface=new DBInterface(context);
        long d1=(from.getDate())+((from.getMonth()+1)*12)+(from.getYear()*365);
        long d2=(to.getDate())+((to.getMonth()+1)*12)+(to.getYear()*365);
        if(sharedPreferences.getBoolean("Qur2an_stat_reading_enabled",false))
        {
            Log.e("show Stat in in in","enter");

            int totalread=dbInterface.getTotalReadAyas(d1,d2);
            ayas.setText(totalread+"");
        }
        if(sharedPreferences.getBoolean("Qur2an_stat_Listen_enabled",false))
        {Log.e("show Stat in in in","enter");

            int totallisten= dbInterface.getTotalListen(d1,d2);
            mins.setText(totallisten+"");

        }
        if (sharedPreferences.getBoolean("Qur2an_stat_Hefz_enabled",false)){
            Log.e("show Stat in in in","enter");
            int totalhefz= dbInterface.getTotalHefz(d1,d2);
            hefz.setText(totalhefz+"");
        }


    }

    /*public void changeLang()
    {
        TextView textView1=(TextView)view.findViewById(R.id.text1);
        TextView textView2=(TextView)view.findViewById(R.id.text2);
        TextView textView3=(TextView)view.findViewById(R.id.text3);
        TextView textView4=(TextView)view.findViewById(R.id.text4);
        TextView textView5=(TextView)view.findViewById(R.id.text5);
        TextView textView6=(TextView)view.findViewById(R.id.text6);
        TextView textView7=(TextView)view.findViewById(R.id.text7);
        TextView textView8=(TextView)view.findViewById(R.id.text8);
        TextView textView9=(TextView)view.findViewById(R.id.text9);
        TextView textView10=(TextView)view.findViewById(R.id.counter);
        String Lang="arabic";
        if(Lang.equals("arabic"))
        {
            textView1.setTypeface(custom_font_arabic);
            textView2.setTypeface(custom_font_arabic);
            textView3.setTypeface(custom_font_arabic);
            textView4.setTypeface(custom_font_arabic);
            textView4.setTypeface(custom_font_arabic);
            textView6.setTypeface(custom_font_arabic);
            textView7.setTypeface(custom_font_arabic);
            textView8.setTypeface(custom_font_arabic);
            textView9.setTypeface(custom_font_arabic);
            textView10.setTypeface(custom_font_arabic);
        }
        else
        {
            textView1.setText("report");
            textView2.setText("Allah is the greatest");
            textView3.setText("Thank god");
            textView4.setText("Glory of God");
            textView5.setText("No God unless Allah");
            textView6.setText("i ask for forgiveness from the mighty Allah");
            textView7.setText("There is no power unless by Allah");
            textView8.setText("Pray to God the Prophet Mohamed");
            textView9.setText("Total");
            textView10.setText("count");
        }*/
    }
