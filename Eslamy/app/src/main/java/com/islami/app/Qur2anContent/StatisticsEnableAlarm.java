package com.islami.app.Qur2anContent;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.SwitchCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;

import com.islami.app.R;


public class StatisticsEnableAlarm extends Fragment {
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    String Lang="arabic";
    Context context;
    View view;
    Typeface custom_font_arabic;
    public StatisticsEnableAlarm()
    {

    }
    @Override
    public void onCreate( Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setRetainInstance(true);
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.q_stat_enable, container, false);
        context = view.getContext();
        custom_font_arabic = Typeface.createFromAsset(context.getAssets(), "fonts/GESSTwoLight.otf");

        sharedPreferences=context.getSharedPreferences("QuranStats", Context.MODE_PRIVATE);
        editor=sharedPreferences.edit();
        SwitchCompat EnableRead=(SwitchCompat)view.findViewById(R.id.switch_compat_reading_Quran);
        SwitchCompat EnableListen=(SwitchCompat)view.findViewById(R.id.switch_compat_listeningQuran);
        SwitchCompat EnableHefz=(SwitchCompat)view.findViewById(R.id.switch_compat_ayathefz);

        if(sharedPreferences.getBoolean("Qur2an_stat_reading_enabled",false))
        {
           EnableRead.setChecked(true);
        }
        if(sharedPreferences.getBoolean("Qur2an_stat_Listen_enabled",false))
        {
            EnableListen.setChecked(true);
        }
        if(sharedPreferences.getBoolean("Qur2an_stat_Hefz_enabled",false))
        {
            EnableHefz.setChecked(true);
        }

        EnableRead.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                editor.putBoolean("Qur2an_stat_reading_enabled", isChecked);
                editor.commit();
                Log.e("switch","num 1 "+isChecked);
            }
        });

       EnableListen.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                editor.putBoolean("Qur2an_stat_Listen_enabled", isChecked);
                editor.commit();
                Log.e("switch","num 2 "+isChecked);
            }
        });

        EnableHefz.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                editor.putBoolean("Qur2an_stat_Hefz_enabled", isChecked);
                editor.commit();
                Log.e("switch","num 3 "+isChecked);
            }
        });


        Lang="english";
    //    if(Lang.equals("english"))
        //changeLang();
    return view;

    }
/*
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rosary_alarm);
        textTime=(TextView)findViewById(R.id.rosary_alarm_time);
        textAlarm=(TextView)findViewById(R.id.rosary_alarm_text);
        sharedPreferences=getSharedPreferences("Salah_Alarm", Context.MODE_PRIVATE);
        editor=sharedPreferences.edit();

        android.support.v7.widget.SwitchCompat switchCompat=(SwitchCompat)findViewById(R.id.switch_compat_rosary);
        if(sharedPreferences.getBoolean("zakerAlarm",false))
        {
            switchCompat.setChecked(true);
        }
        switchCompat.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                editor.putBoolean("zakerAlarm", isChecked);
                editor.commit();
                if(isChecked==true)
                {

                  //  Intent intent=new Intent(getApplicationContext(),RosaryAlarmReceiver.class);
                  //  sendBroadcast(intent);
                  //  Toast.makeText(getApplicationContext(),"start", Toast.LENGTH_LONG).show();*/
          //      }
               /* else
                {

                }
            }
        });
        Lang="english";
        if(Lang.equals("english"))
        changeLang();
    }*//*
public void changeLang()
{
    textTime.setText("every 5 m");
    textAlarm.setText("zaker Alarm");
}*/
}
