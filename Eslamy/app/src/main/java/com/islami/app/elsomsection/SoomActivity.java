package com.islami.app.elsomsection;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import java.util.ArrayList;

import com.islami.app.R;


public class SoomActivity extends Fragment{
   // TabHost tabHost;
    ArrayList<Fragment> fragments;
    ViewPager viewPager;
    MypagerAdapter pageAdapter;
    private TabLayout tabLayout;

    private int[] tabIcons = {
            R.drawable.analysisindicator ,
            R.drawable.elsonanindicator,
            R.drawable.ramadanindicator
    };

    @Override
    public void onCreate( Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
    int tabnum;

    public void setTabnum(int tabnum) {
        this.tabnum = tabnum;
    }

    public SoomActivity(){}


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_soom, container, false);

        tabLayout = (TabLayout) view.findViewById(R.id.tabs);
        viewPager=(ViewPager)view.findViewById(R.id.viewPagerContainer);
        setupViewPager(viewPager);
        setuptabLayout();
        tabLayout.getTabAt(tabnum).select();
        tabLayout.setSelectedTabIndicatorHeight(0);
        return view;

    }

    public void setuptabLayout(){
        try {
            tabLayout.setupWithViewPager(viewPager);
            for (int i = 0; i < tabLayout.getTabCount(); i++) {
                TabLayout.Tab tab = tabLayout.getTabAt(i);
                tab.setCustomView(pageAdapter.getTabView(i));
            }
            tabLayout.requestFocus();
        }
        catch(Exception ex){
            Log.e("errr2", ex.getMessage());
        }
    }

    public void setupViewPager(ViewPager viewPager) {
        pageAdapter = new MypagerAdapter(getContext(),getActivity().getSupportFragmentManager());
        pageAdapter.addFragment(new Analysis(),  tabIcons[0]);
        pageAdapter.addFragment(new elsonanActivity(),tabIcons[1]);
        pageAdapter.addFragment(new ramadanActivity(), tabIcons[2]);
        viewPager.setAdapter(pageAdapter);
    }




    public class MypagerAdapter extends FragmentStatePagerAdapter {
        Context mContext;
        ArrayList<Fragment>fragments=new ArrayList<>();
        ArrayList<Integer>Icons=new ArrayList<>();

        @Override
        public Fragment getItem(int position) {
            return fragments.get(position);
        }

        @Override
        public int getCount() {
            return fragments.size();
        }

        public MypagerAdapter(Context context, FragmentManager fm) {
            super(fm);
            this.mContext = context;
        }

        public void addFragment(Fragment fragment,  int drawable) {
            fragments.add(fragment);
            Icons.add(drawable);
        }
        @Override
        public int getItemPosition(Object object)
        {
            return POSITION_UNCHANGED;
        }



        @Override
        public CharSequence getPageTitle(int position) {
            return null;
        }

        public View getTabView(int position) {
            View tab = LayoutInflater.from(mContext).inflate(R.layout.tabindicatorsoom, null);
            ImageView tabImage = (ImageView) tab.findViewById(R.id.tab_Image);
            tabImage.setBackgroundResource(Icons.get(position));
            return tab;
        }

    }



}
