package com.islami.app.Qur2anContent;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.GridLayout;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.viewpagerindicator.CirclePageIndicator;

import java.util.ArrayList;
import java.util.List;

import com.islami.app.Prefs;
import com.islami.app.R;

/**
 * Created by Admin on 1/3/2016.
 */
public class Read extends Fragment {

    GridLayout parent;
    LinearLayout.LayoutParams params;
    LinearLayout child;
    ViewPager viewPager;
    int w, h;
    List<GridLayout> fragments = new ArrayList<>();
    CirclePageIndicator mIndicator;
    LayoutInflater inflater;

    public Read() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.q_read, container, false);
        this.inflater = inflater;

        ImageButton gotoBookMark=(ImageButton)view.findViewById(R.id.gotoBookMark);

        gotoBookMark.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences prefs = getActivity().getSharedPreferences(Prefs.BOOK_MARK_PREF, Context.MODE_PRIVATE);
                String SuraID = prefs.getString("Sura_ID", null);
                int pos=prefs.getInt("Aya_Pos",-1);
                if (SuraID != null) {
                    Intent i=new Intent(getContext(),ReadSura.class);
                 //   Log.e("aa",id.getText().toString());
                    i.putExtra("suraid",SuraID);
                    i.putExtra("isbookmark","true");
                    startActivity(i);
                }
                else {
                    Toast.makeText(getContext(),"you Dont have Book Mark...",Toast.LENGTH_SHORT).show();

                }
            }
        });

        viewPager = (ViewPager) view.findViewById(R.id.pager);
        mIndicator = (CirclePageIndicator) view.findViewById(R.id.indicator);
        viewPager.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {


            @Override
            public void onGlobalLayout() {
                // Ensure you call it only once :
                viewPager.getViewTreeObserver().removeGlobalOnLayoutListener(this);

                // Here you can get the size :)
                w = viewPager.getWidth();
                h = viewPager.getHeight();
                PrepareSuras();
            }
        });

        return view;
    }

    surasPagerAdapter adapter=new surasPagerAdapter();
    @Override
    public void onResume() {
        super.onResume();
   // PrepareSuras();
        viewPager.setAdapter(adapter);
        mIndicator.setViewPager(viewPager);

    }

    public void PrepareSuras() {
        if (fragments.size()==0){
        params = new LinearLayout.LayoutParams(w / 3, h / 3);
        int x = 0;
        for (int i = 0; i < (114 / 9) + 1; i++) {
            parent = (GridLayout) inflater.inflate(R.layout.surasitems, null);
            for (int j = 0; j < 9; j++, x++) {
                LinearLayout child = (LinearLayout) inflater.inflate(R.layout.suraitem, null);
                child.setLayoutParams(params);
                TextView suraname = (TextView) child.findViewById(R.id.suraname);
                suraname.setText(DataNeeded.EnSuras[x]);
                suraname = (TextView) child.findViewById(R.id.suraid);
                suraname.setText((x + 1) + " r");

                parent.addView(child);
                if (x == 113) break;
            }
            fragments.add(parent);
            if (x == 113) break;

        }
        }



        viewPager.setAdapter(adapter);
        mIndicator.setViewPager(viewPager);


    }



    private class surasPagerAdapter extends PagerAdapter {


        @Override
        public int getCount() {
            return fragments.size();
        }


        @Override
        public Object instantiateItem(ViewGroup collection, int position) {



            collection.addView(fragments.get(position),0);

            return fragments.get(position);
        }

        @Override
        public void destroyItem(ViewGroup collection, int position, Object view) {
            collection.removeView((GridLayout) view);
        }

  @Override
        public boolean isViewFromObject(View view, Object object) {
            return (view==object);
        }

        @Override
        public void finishUpdate(ViewGroup arg0) {}


        @Override
        public void restoreState(Parcelable arg0, ClassLoader arg1) {}

        @Override
        public Parcelable saveState() {
            return null;
        }

        @Override
        public void startUpdate(ViewGroup arg0) {}
    }
}