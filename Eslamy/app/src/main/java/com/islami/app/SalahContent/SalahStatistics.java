package com.islami.app.SalahContent;

import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.Date;

import DataBase.DBInterface;
import com.islami.app.R;


public class SalahStatistics extends Fragment {
    Spinner spinnerFrom;
    Spinner spinnerTo;
    EditText dayFrom,dayTo,yearFrom,yearTo;
    int monthFrom,monthTo;
    Date from,to;
    String Lang="arabic";
    Typeface custom_font_arabic;
    char[] arabicNumbers = {'٠','١','٢','٣','٤','٥','٦','٧','٨','٩'};
    TextView input1,input2,input3,input4,input5,input6,input7,input8,input9,input10;
    Context context;
    View view;
    public SalahStatistics()
    {
    }
    @Override
    public void onCreate( Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.activity_salah_statistics, container, false);
        context = view.getContext();
        custom_font_arabic= Typeface.createFromAsset(context.getAssets(), "fonts/GESSTwoLight.otf");
        input1=(TextView)view.findViewById(R.id.input1);
        input2=(TextView)view.findViewById(R.id.input2);
        input3=(TextView)view.findViewById(R.id.input3);
        input4=(TextView)view.findViewById(R.id.input4);
        input5=(TextView)view.findViewById(R.id.input5);
        input6=(TextView)view.findViewById(R.id.input6);
        input7=(TextView)view.findViewById(R.id.input7);
        input8=(TextView)view.findViewById(R.id.input8);
        input9=(TextView)view.findViewById(R.id.input9);
        input10=(TextView)view.findViewById(R.id.input10);
        dayFrom=(EditText)view.findViewById(R.id.dayFrom);
        dayTo=(EditText)view.findViewById(R.id.dayTo);
        yearFrom=(EditText)view.findViewById(R.id.yearFrom);
        yearTo=(EditText)view.findViewById(R.id.yearTo);
        from=new Date();
        to=new Date();
        dayFrom.setText(from.getDate()+"");
        dayTo.setText(from.getDate()+"");
        yearTo.setText((from.getYear()+1900)+"");
        yearFrom.setText((from.getYear()+1900)+"");
        monthFrom=from.getMonth();
        monthTo=monthFrom;
        ArrayAdapter adapter = ArrayAdapter.createFromResource(context, R.array.month_arabic, R.layout.spinner_phone);
        adapter.setDropDownViewResource(R.layout.dropitem);
        spinnerFrom=(Spinner)view.findViewById(R.id.spinnerFrom);
        spinnerTo=(Spinner)view.findViewById(R.id.spinnerTo);
        spinnerFrom.setAdapter(adapter);
        spinnerTo.setAdapter(adapter);
        spinnerFrom.setSelection(monthFrom);
        spinnerTo.setSelection(monthTo);
        spinnerFrom.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                monthFrom=position;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        spinnerTo.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                monthTo=position;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        Button show_zaker=(Button)view.findViewById(R.id.show_zaker);
        show_zaker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Show();
            }
        });
        changeLang();
        return view;
    }

    public void changeLang()
    {
        TextView textView1=(TextView)view.findViewById(R.id.text1);
        TextView textView2=(TextView)view.findViewById(R.id.text2);
        TextView textView3=(TextView)view.findViewById(R.id.text3);
        TextView textView4=(TextView)view.findViewById(R.id.text4);
        TextView textView5=(TextView)view.findViewById(R.id.text5);
        TextView textView6=(TextView)view.findViewById(R.id.text6);
        TextView textView7=(TextView)view.findViewById(R.id.text7);
        TextView textView8=(TextView)view.findViewById(R.id.text8);

        if(Lang.equals("arabic"))
        {
            textView1.setTypeface(custom_font_arabic);
            textView2.setTypeface(custom_font_arabic);
            textView3.setTypeface(custom_font_arabic);
            textView4.setTypeface(custom_font_arabic);
            textView5.setTypeface(custom_font_arabic);
            textView6.setTypeface(custom_font_arabic);
            textView7.setTypeface(custom_font_arabic);
            textView8.setTypeface(custom_font_arabic);
            input1.setTypeface(custom_font_arabic);
            input2.setTypeface(custom_font_arabic);
            input3.setTypeface(custom_font_arabic);
            input4.setTypeface(custom_font_arabic);
            input5.setTypeface(custom_font_arabic);
            input6.setTypeface(custom_font_arabic);
            input7.setTypeface(custom_font_arabic);
            input8.setTypeface(custom_font_arabic);
            input9.setTypeface(custom_font_arabic);
            input10.setTypeface(custom_font_arabic);

            dayTo.setText(dayFrom.getText().toString());
            dayFrom.setText(dayFrom.getText().toString());
            yearFrom.setText(yearFrom.getText().toString());
            yearTo.setText(yearTo.getText().toString());
            dayTo.setTypeface(custom_font_arabic);
            dayFrom.setTypeface(custom_font_arabic);
            yearTo.setTypeface(custom_font_arabic);
            yearFrom.setTypeface(custom_font_arabic);
        }
    }

    public String changeToArabic(String number)
    {
        String s=number;
        int count=0;
        for(char i='0';i<='9';i++)
        {
            s=s.replace(i,arabicNumbers[count]);
            count++;
        }
        return s;
    }
    public long convertToDays(Date d)
    {
        return d.getDate()+((d.getMonth()+1)*30)+((d.getYear()+1900)*365);

    }
    public void Show()
    {
        DBInterface dbInterface=new DBInterface(context);
        from.setDate(Integer.valueOf(dayFrom.getText().toString()));
        from.setYear(Integer.valueOf(yearFrom.getText().toString())-1900);
        to.setYear(Integer.valueOf(yearTo.getText().toString())-1900);
        to.setDate(Integer.valueOf(dayTo.getText().toString()));
        long x=dbInterface.getNumberOfFardInMosque(convertToDays(from),convertToDays(to));
        long y=dbInterface.getNumberOfFardOutMosque(convertToDays(from), convertToDays(to));
        Log.v("testdate", x + " "+y);
        input1.setText(x+"");
        input2.setText(y+"");
        input3.setText(x+y+"");
        int num_day=Integer.valueOf(dayTo.getText().toString())-Integer.valueOf(dayFrom.getText().toString())+1;
        int num_month=monthTo+monthFrom;
        int num_year=Integer.valueOf(yearTo.getText().toString())-Integer.valueOf(yearFrom.getText().toString());
        int num_fard=(num_day)+(num_month*30)+(num_year*365);
        if(num_fard<0)num_fard*=-1;
        Log.v("test",num_day+" "+num_month+" "+num_year+" "+num_fard);
        int p=(int)(((x+y)*100.0)/(num_fard*5.0));
        input4.setText("%"+p);
        long s=dbInterface.getTotalOfSann(convertToDays(from),convertToDays(to));
        int p_s=(int)((s*100.0)/(num_fard*14.0));
        input5.setText(s+"");
        input6.setText(s+"");
        input7.setText("%"+ p_s+"");
        long n=dbInterface.getTotalNawafl(convertToDays(from),convertToDays(to));
        int p_n=(int)((n*100.0)/(num_fard*5.0));
        input8.setText(n+"");
        input9.setText(n+"");
        input10.setText("%"+p_n);
    }
}

    /*@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_salah_statistics);
        custom_font_arabic= Typeface.createFromAsset(getAssets(), "fonts/GESSTwoLight.otf");
        input1=(TextView)findViewById(R.id.input1);
        input2=(TextView)findViewById(R.id.input2);
        input3=(TextView)findViewById(R.id.input3);
        input4=(TextView)findViewById(R.id.input4);
        input5=(TextView)findViewById(R.id.input5);
        input6=(TextView)findViewById(R.id.input6);
        input7=(TextView)findViewById(R.id.input7);
        input8=(TextView)findViewById(R.id.input8);
        input9=(TextView)findViewById(R.id.input9);
        input10=(TextView)findViewById(R.id.input10);
        dayFrom=(EditText)findViewById(R.id.dayFrom);
        dayTo=(EditText)findViewById(R.id.dayTo);
        yearFrom=(EditText)findViewById(R.id.yearFrom);
        yearTo=(EditText)findViewById(R.id.yearTo);
        from=new Date();
        to=new Date();
        dayFrom.setText(from.getDate()+"");
        dayTo.setText(from.getDate()+"");
        yearTo.setText((from.getYear()+1900)+"");
        yearFrom.setText((from.getYear()+1900)+"");
        monthFrom=from.getMonth();
        monthTo=monthFrom;
        ArrayAdapter adapter = ArrayAdapter.createFromResource(this, R.array.month_arabic, R.layout.spinner_phone);
        adapter.setDropDownViewResource(R.layout.dropitem);
        spinnerFrom=(Spinner)findViewById(R.id.spinnerFrom);
        spinnerTo=(Spinner)findViewById(R.id.spinnerTo);
        spinnerFrom.setAdapter(adapter);
        spinnerTo.setAdapter(adapter);
        spinnerFrom.setSelection(monthFrom);
        spinnerTo.setSelection(monthTo);
        spinnerFrom.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                monthFrom=position;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        spinnerTo.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                monthTo=position;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        changeLang();
    }*/