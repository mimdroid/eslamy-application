package com.islami.app.elsomsection;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.TextView;

import java.util.ArrayList;

import com.islami.app.R;


public class ArabicCalendar extends BaseAdapter {
    ArrayList<String> arrayList=new ArrayList<>();
    Context context;
    int start=0;
    Typeface custom_font_arabic;
    int index=1;
    int month=0,year=0,selectedday;
    ArrayList<TextView>views=new ArrayList<>();
    String fontPath = "fonts/GESSTwoLight.otf";
    Typeface tf ;
    //s
    public ArabicCalendar(Context c,int s,int l,int day,int mon,int ye, int selectedday)
    {
        context=c;
         tf = Typeface.createFromAsset(context.getResources().getAssets(), fontPath);

        this.start=s;
        this.index=day;
        this.month=mon;
        this.year=ye;
        this.selectedday=selectedday;
        for(int i=0;i<start;i++)arrayList.add("");
        for(int i=1;i<=l;i++)arrayList.add(" "+i+"");
        if(arrayList.size()%7!=0)
        {
            int dif=7-(arrayList.size()%7);
            for(int i=0;i<dif;i++)arrayList.add("");
        }
        for(int i=0;i<arrayList.size();i++)
        {
            int k=i+6;
            for(int j=i;j<i+3;j++,k--)
            {
                String temp=arrayList.get(j);
                arrayList.set(j,arrayList.get(k));
                arrayList.set(k,temp);
            }
            i+=6;
        }
        for(int i=0;i<arrayList.size();i++)
        {
            final TextView textView = new TextView(context);
            textView.setText(""+arrayList.get(i));
            textView.setLayoutParams(new GridView.LayoutParams(35, ViewGroup.LayoutParams.WRAP_CONTENT));
            textView.setTextSize(10);
            textView.setGravity(View.TEXT_ALIGNMENT_CENTER);
            textView.setClickable(true);
            textView.setTypeface(custom_font_arabic);
            textView.setPadding(5,5,5,5);
            textView.setTextColor(Color.parseColor("#FFFFFF"));
            textView.setTypeface(tf);
            views.add(textView);
             String numberOnly= arrayList.get(i).replaceAll("[^0-9]", "");
            Log.v("tag11",numberOnly );
            if(numberOnly.equals(selectedday+"")) {
                textView.setBackgroundResource(R.drawable.roundedabout);
            }
        }
    }
    public int getIndex()
    {
        return index;
    }
    @Override
    public int getCount() {
        return arrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return arrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        TextView textView=views.get(position);

      return textView;
    }}
