package com.islami.app.elsomsection;

/**
 * Created by FCI on 1/8/2016.
 */
public class DateHijri {
   private int day;
  private   int month;
   private int year;

    DateHijri(){
        day=0;
        month=0;
        year=0;

    }

    DateHijri(int y , int m , int d){
        day=d;
        month=m;
        year=y;
    }
    public String toString(){
        return year+" "+month+" "+day;
    }

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }
}

