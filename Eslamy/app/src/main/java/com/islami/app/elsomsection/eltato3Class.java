package com.islami.app.elsomsection;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import org.joda.time.Chronology;
import org.joda.time.DateTime;
import org.joda.time.chrono.ISOChronology;
import org.joda.time.chrono.IslamicChronology;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Calendar;

import com.islami.app.R;

/**
 * Created by FCI on 1/5/2016.
 */


public class eltato3Class extends Fragment {

    final String[] MonthNamesArabic = {"يناير", "فبراير ", "مارس", "ابريل", "مايو", "يونيو", "يوليو", "اغسطس", "سبتمبر", "أكتوبر", "نوفمبر", "ديسمبر"};
    final String[] MonthNamesEnglish = {"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"};
    final String[] HijriMonthArabic = {"محرم", "ذى.الحجه", "شوال", "محرم", "شعبان"};
    final String[] HijriMonthEnglish = {"Moharam", "Ze El-haga", "Shwal", "Moharam", "Shaaban"};
    final int[] HijriMonthNum = {1, 12, 10, 1, 8};
    final int[] HijriDayNum = {1, 10, 9};
    ListView lv;
    Context context;
    public  String[] titlelist = {" يوم عاشوراء", " يوم عرفه", " شهر شوال ", " شهر محرم", " شهر شعبان","التطوع المقيد الشهرى"," ","التطوع المقيد الاسبوعى", " ","المقيد بحال الشخص"," " +
            ""};
    public  String yearlist ;
    int GeoYear = 0, GeoDay = 0, GeoMonth = 0, HijriYear = 0;
    ArrayList<InputStream> texts;
    ArrayList <String>hijritexts,yeartexts;
    public  String[] text , hijritext , yeartext;
    public eltato3Class(){}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.eloayd1 , container, false);

        context = getContext();
        texts=new ArrayList<>();
        hijritexts=new ArrayList<>();
        yeartexts=new ArrayList<>();
        InputStream Is;
        try {
            Is = getResources().getAssets().open("ArfaArabic");
            texts.add(Is);
            Is = getResources().getAssets().open("Ahsora.txt");
            texts.add(Is);
            Is = getResources().getAssets().open("Moharam");
            texts.add(Is);
            Is = getResources().getAssets().open("Shwal");
            texts.add(Is);
            Is = getResources().getAssets().open("Sha3ban");
            texts.add(Is);
            Is = getResources().getAssets().open("Monthly");
            texts.add(Is);
            Is = getResources().getAssets().open("Weekly");
            texts.add(Is);
            Is = getResources().getAssets().open("Personal");
            texts.add(Is);
        } catch (IOException e) {
            e.printStackTrace();
        }
        text= new String[]{GetData(texts.get(1)),GetData(texts.get(0)),GetData(texts.get(2)),GetData(texts.get(3)),GetData(texts.get(4))," ",GetData(texts.get(5))," ",GetData(texts.get(6))," ",GetData(texts.get(7))};
        CurrentDate(0,1);
        CurrentDate(1,2);
        CurrentDate(2,0);
        CurrentDate(0,0);
        CurrentDate(4,0);
        hijritext=new String[]{hijritexts.get(0),hijritexts.get(1),hijritexts.get(2),hijritexts.get(3),hijritexts.get(4)};
        yeartext=new String[]{yeartexts.get(0),yeartexts.get(1),yeartexts.get(2),yeartexts.get(3),yeartexts.get(4)};
        ///////////////////////////////////////////////////////////////

        lv = (ListView) view.findViewById(R.id.listView);
        lv.setAdapter(new CustomAdapter(context, titlelist , yearlist,text,hijritext,yeartext));

        return view;

    }

    public String GetData(InputStream inputStream){
        InputStream iS = null;
        String d="Not Found";
        try {
            iS=inputStream;
            BufferedReader reader = new BufferedReader(new InputStreamReader(iS));
            StringBuilder sb = new StringBuilder();
            String line = reader.readLine();
            sb.append(line);
            d=sb.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return d;
    }
    public  int GetGeoMonth(int HijriYear,int HijriMonth , int HijriDay){
        DateTime t= DateTime.now();
        DateTime dtIslamic = t.withChronology(IslamicChronology.getInstance());
        Chronology iso = ISOChronology.getInstanceUTC();
        Chronology hijri = IslamicChronology.getInstanceUTC();
        DateTime dtHijri = new DateTime(HijriYear,HijriMonth,HijriDay,Calendar.HOUR_OF_DAY
                ,Calendar.MINUTE,hijri);
        DateTime dtIso = new DateTime(dtHijri, iso);
        return  dtIso.getMonthOfYear();
    }

    public  int GetGeoDay(int HijriYear,int HijriMonth , int HijriDay){
        DateTime t= DateTime.now();
        Calendar calendar = Calendar.getInstance();
        DateTime dtIslamic = t.withChronology(IslamicChronology.getInstance());
        Chronology iso = ISOChronology.getInstanceUTC();
        Chronology hijri = IslamicChronology.getInstanceUTC();
        DateTime dtHijri = new DateTime(HijriYear,HijriMonth,HijriDay,Calendar.HOUR_OF_DAY
                ,Calendar.MINUTE,hijri);
        DateTime dtIso = new DateTime(dtHijri, iso);
        return  dtIso.getDayOfMonth();
    }
    public  int GetGeoYear(int HijriYear,int HijriMonth , int HijriDay){
        DateTime t= DateTime.now();
        DateTime dtIslamic = t.withChronology(IslamicChronology.getInstance());
        Chronology iso = ISOChronology.getInstanceUTC();
        Chronology hijri = IslamicChronology.getInstanceUTC();
        DateTime dtHijri = new DateTime(HijriYear,HijriMonth,HijriDay,Calendar.HOUR_OF_DAY
                ,Calendar.MINUTE,hijri);
        DateTime dtIso = new DateTime(dtHijri, iso);
        return  dtIso.getYear();
    }
    public int GetHijriYear(int GeoYear){
        DateTime dtISO = new DateTime(GeoYear, 1, 1, 12, 0, 0, 0);
        DateTime dtIslamic = dtISO.withChronology(IslamicChronology.getInstance());
        return  dtIslamic.getYear();
    }

    public void CurrentDate(int index , int index2){
        Calendar calendar = Calendar.getInstance();
        GeoYear = calendar.get(Calendar.YEAR);
        GeoMonth=calendar.get(Calendar.DAY_OF_MONTH);
        GeoDay=calendar.get(Calendar.MONTH)+1;
        yearlist=GeoYear+"";
        HijriYear = GetHijriYear(GeoYear);
        hijritexts.add( HijriDayNum[index2]+" "+ HijriMonthArabic[index]+" "+ HijriYear);
        GeoDay=GetGeoDay(HijriYear, HijriMonthNum[index], HijriDayNum[index2]);
        GeoMonth=GetGeoMonth(HijriYear,HijriMonthNum[index],HijriDayNum[index2]);
        yeartexts.add(GeoDay +" "+MonthNamesArabic[GeoMonth-1] + " "+GeoYear);
    }
    public String GetHijriDayfromText(TextView Text){
        return Text.getText().toString().split(" ")[0];
    }
    public String GetHijriMonthfromText(TextView Text){
        return Text.getText().toString().split(" ")[1];
    }

    public String GetHijriYearfromText(TextView Text){
        return Text.getText().toString().split(" ")[2];
    }

}
