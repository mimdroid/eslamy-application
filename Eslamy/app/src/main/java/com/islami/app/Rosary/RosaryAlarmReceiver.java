package com.islami.app.Rosary;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by Ibrahim on 12/31/2015.
 */

public class RosaryAlarmReceiver extends BroadcastReceiver {
    Context c;
    Timer timer;
    TimerTask timerTask;
    @Override
    public void onReceive(Context context, Intent intent) {
        c=context;
        timer=new Timer();
        startTask();
        timer.schedule(timerTask, 5000, 60*1000);
    }

    public void startTask()
    {
        timerTask = new TimerTask() {
            public void run() {
                Intent i = new Intent(c, RosaryService.class);
                Log.d("test", "start_task");
                c.startService(i);
                c.stopService(i);
            }
        };
    }
}
