package com.islami.app.SalahContent;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.ListView;

import java.util.ArrayList;

import Manage.Reader;
import com.islami.app.R;


public class Azkaar_section_salah extends Activity {
    Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_azkaar_section_salah);
        Intent intent=getIntent();
        int index=intent.getIntExtra("position",-1);
        Reader reader=new Reader(this);
        //   Log.v("test", reader.readFromFile());
        ArrayList<String> title_arabic=new ArrayList<>();
        ArrayList<String>title_english=new ArrayList<>();
        ArrayList<String>azkar_arabic=new ArrayList<>();
        ArrayList<String>azkar_english=new ArrayList<>();
        ArrayList<Integer>num_secions=new ArrayList<>();
        reader.readAzakrSalah(reader.readFromFile("AzarSalah.txt"),title_arabic,title_english,azkar_arabic,azkar_english,num_secions);
        ArrayList<String>sublist=new ArrayList<>();
        ArrayList<String>sublist1=new ArrayList<>();
        if(index!=-1)
        {
            int counter=0;
            for(int i=0;i<index;i++)
            {
                counter+=num_secions.get(i);
            }
            Log.v("index", index + " ");
            for(int i=counter;i<counter+num_secions.get(index);i++)
            {
                sublist.add(azkar_arabic.get(i));
                sublist1.add(azkar_english.get(i));
            }
        }
        context=this;

        ListView lv=(ListView) findViewById(R.id.azkaar_section_salah_listview);
        lv.setAdapter(new Azkaar_section_salah_Adapter(this, sublist,sublist1));
    }



}
