package com.islami.app.Qur2anContent;

import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.GridLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.viewpagerindicator.CirclePageIndicator;

import java.util.ArrayList;
import java.util.List;

import com.islami.app.R;

/**
 * Created by Admin on 1/3/2016.
 */

public class Listen extends Fragment {

    LayoutInflater inflater;
    public Listen(){

    }



    @Override
    public void onCreate( Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    GridLayout parent;
    LinearLayout.LayoutParams params;
    LinearLayout child;
    ViewPager viewPager;
    int w,h;
    List<GridLayout> fragments=new ArrayList<>();
    CirclePageIndicator mIndicator;
    @Override
    public View onCreateView(LayoutInflater inflater,  ViewGroup container,  Bundle savedInstanceState) {
        View view= inflater.inflate(R.layout.q_listen, container, false);
        this.inflater=inflater;

         viewPager = (ViewPager) view.findViewById(R.id.pager);
         mIndicator = (CirclePageIndicator)view.findViewById(R.id.indicator);
         viewPager.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {


            @Override
            public void onGlobalLayout() {
                // Ensure you call it only once :
                viewPager.getViewTreeObserver().removeGlobalOnLayoutListener(this);

                // Here you can get the size :)
                 w=viewPager.getWidth();
                 h=viewPager.getHeight();
        //        Log.e("www",w+" "+h);
                PrepareSuras();
            }
        });



        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
    // PrepareSuras();
     //    Log.e("w",w+" "+h);
        viewPager.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {


            @Override
            public void onGlobalLayout() {
                // Ensure you call it only once :
               viewPager.getViewTreeObserver().removeGlobalOnLayoutListener(this);

                // Here you can get the size :)
        //        w=viewPager.getWidth();
          //      h=viewPager.getHeight();
          //      Log.e("www",w+" "+h);
                PrepareSuras();
            }
        });
    }

    surasPagerAdapter adapter=new surasPagerAdapter();

    public void PrepareSuras() {
//        Toast.makeText(getContext(),ArSuras.length+" "+EnSuras.length,Toast.LENGTH_SHORT).show();
if (fragments.size()==0) {
    params = new LinearLayout.LayoutParams(w / 3, h / 3);
    int x = 0;
    for (int i = 0; i < (114 / 9) + 1; i++) {
        parent = (GridLayout) inflater.inflate(R.layout.surasitems, null);
        for (int j = 0; j < 9; j++, x++) {
            LinearLayout child = (LinearLayout) inflater.inflate(R.layout.suraitem, null);
            child.setLayoutParams(params);
            TextView suraname = (TextView) child.findViewById(R.id.suraname);
            suraname.setText(DataNeeded.EnSuras[x]);
            suraname = (TextView) child.findViewById(R.id.suraid);
            suraname.setText((x + 1) + " l");

            parent.addView(child);
            if (x == 113) break;
        }
        fragments.add(parent);
        if (x == 113) break;

    }

}
        viewPager.setAdapter(adapter);
        mIndicator.setViewPager(viewPager);

    }

    private class surasPagerAdapter extends PagerAdapter {


        @Override
        public int getCount() {
            return fragments.size();
        }

        /**
         * Create the page for the given position.  The adapter is responsible
         * for adding the view to the container given here, although it only
         * must ensure this is done by the time it returns from
         * {@link #finishUpdate(android.view.ViewGroup)}.
         *
         * @param collection The containing View in which the page will be shown.
         * @param position The page position to be instantiated.
         * @return Returns an Object representing the new page.  This does not
         * need to be a View, but can be some other container of the page.
         */
        @Override
        public Object instantiateItem(ViewGroup collection, int position) {



            collection.addView(fragments.get(position),0);

            return fragments.get(position);
        }

        /**
         * Remove a page for the given position.  The adapter is responsible
         * for removing the view from its container, although it only must ensure
         * this is done by the time it returns from {@link #finishUpdate(android.view.ViewGroup)}.
         *
         * @param collection The containing View from which the page will be removed.
         * @param position The page position to be removed.
         * @param view The same object that was returned by
         * {@link #instantiateItem(android.view.View, int)}.
         */
        @Override
        public void destroyItem(ViewGroup collection, int position, Object view) {
            collection.removeView((GridLayout) view);
        }


        /**
         * Determines whether a page View is associated with a specific key object
         * as returned by instantiateItem(ViewGroup, int). This method is required
         * for a PagerAdapter to function properly.
         * @param view Page View to check for association with object
         * @param object Object to check for association with view
         * @return
         */
        @Override
        public boolean isViewFromObject(View view, Object object) {
            return (view==object);
        }


        /**
         * Called when the a change in the shown pages has been completed.  At this
         * point you must ensure that all of the pages have actually been added or
         * removed from the container as appropriate.
         * @param arg0 The containing View which is displaying this adapter's
         * page views.
         */
        @Override
        public void finishUpdate(ViewGroup arg0) {}


        @Override
        public void restoreState(Parcelable arg0, ClassLoader arg1) {}

        @Override
        public Parcelable saveState() {
            return null;
        }

        @Override
        public void startUpdate(ViewGroup arg0) {}
    }
}
