package com.islami.app.Rosary;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.SwitchCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.islami.app.R;


public class RosaryAlarm extends Fragment {
    TextView textTime,textAlarm;
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    String Lang="arabic";
    Context context;
    View view;
    Typeface custom_font_arabic;
    public RosaryAlarm()
    {

    }
    @Override
    public void onCreate( Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.activity_rosary_alarm, container, false);
        context = view.getContext();
        custom_font_arabic = Typeface.createFromAsset(context.getAssets(), "fonts/GESSTwoLight.otf");
        textTime=(TextView)view.findViewById(R.id.rosary_alarm_time);
        textAlarm=(TextView)view.findViewById(R.id.rosary_alarm_text);
        sharedPreferences=context.getSharedPreferences("Salah_Alarm", Context.MODE_PRIVATE);
        editor=sharedPreferences.edit();
        android.support.v7.widget.SwitchCompat switchCompat=(SwitchCompat)view.findViewById(R.id.switch_compat_rosary);
        if(sharedPreferences.getBoolean("zakerAlarm",false))
        {
            switchCompat.setChecked(true);
        }
        switchCompat.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                editor.putBoolean("zakerAlarm", isChecked);
                editor.commit();
            }
        });
        //Lang="english";
        if(Lang.equals("english"))
        changeLang();
    return view;
    }
/*
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rosary_alarm);
        textTime=(TextView)findViewById(R.id.rosary_alarm_time);
        textAlarm=(TextView)findViewById(R.id.rosary_alarm_text);
        sharedPreferences=getSharedPreferences("Salah_Alarm", Context.MODE_PRIVATE);
        editor=sharedPreferences.edit();

        android.support.v7.widget.SwitchCompat switchCompat=(SwitchCompat)findViewById(R.id.switch_compat_rosary);
        if(sharedPreferences.getBoolean("zakerAlarm",false))
        {
            switchCompat.setChecked(true);
        }
        switchCompat.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                editor.putBoolean("zakerAlarm", isChecked);
                editor.commit();
                if(isChecked==true)
                {

                  //  Intent intent=new Intent(getApplicationContext(),RosaryAlarmReceiver.class);
                  //  sendBroadcast(intent);
                  //  Toast.makeText(getApplicationContext(),"start", Toast.LENGTH_LONG).show();*/
          //      }
               /* else
                {

                }
            }
        });
        Lang="english";
        if(Lang.equals("english"))
        changeLang();
    }*/
public void changeLang()
{
    textTime.setText("every 1 m");
    textAlarm.setText("zaker Alarm");
}
}
