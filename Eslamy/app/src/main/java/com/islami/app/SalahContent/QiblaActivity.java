package com.islami.app.SalahContent;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.hardware.GeomagneticField;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import ConnectionHandler.Connector;
import Manage.GPS;
import com.islami.app.R;


public class QiblaActivity extends Fragment implements SensorEventListener {
    private ImageView image;
    private ImageView CompassBackground;
    private float currentDegree = 0f;
    private SensorManager mSensorManager;
    TextView tvHeading;
    Matrix matrix;
    Sensor accelerometer,magnetometer;
    Bitmap bitmap,rot_bitmap;
    GeomagneticField geoField;
    float[] mGravity;
    float[] mGeomagnetic;
    float azimut=0.0f;
    float rotation;
    float ff;
    LocationManager locationManager;
    GPS gps;
    Location l,loc;
    Context context;
    View view;
    public QiblaActivity()
    {

    }
    @Override
    public void onCreate( Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.activity_qibla, container, false);
        context = view.getContext();
        image = (ImageView) view.findViewById(R.id.arrow);
        CompassBackground=(ImageView)view.findViewById(R.id.CompassBackground);
        locationManager= (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        Connector connector=new Connector(context);
     //   if(connector.isOnline()) {
            gps = new GPS(locationManager);
            l = gps.getLocation();
            loc = gps.getLocation();
            loc.setLatitude(21.4216181);
            loc.setLongitude(39.8247903);
            ff = l.bearingTo(loc);
            mSensorManager = (SensorManager) context.getSystemService(context.SENSOR_SERVICE);
            accelerometer = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
            magnetometer = mSensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
            mSensorManager.registerListener(this, accelerometer, 1);
            mSensorManager.registerListener(this, magnetometer, 1);
        return view;
    }
    @Override
    public void onSensorChanged(SensorEvent event) {

        if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER)
            mGravity = event.values;
        if (event.sensor.getType() == Sensor.TYPE_MAGNETIC_FIELD)
            mGeomagnetic = event.values;
        if (mGravity != null && mGeomagnetic != null) {
            float R[] = new float[9];
            float I[] = new float[9];
            boolean success = SensorManager.getRotationMatrix(R, I, mGravity, mGeomagnetic);
            if (success) {
                float orientation[] = new float[3];
                SensorManager.getOrientation(R, orientation);
                azimut = orientation[0]; // orientation contains: azimut, pitch and roll
            }
        }
        rotation = ff-(azimut * 360 / (2 * 3.14159f));
        Log.v("angle",ff+" "+ rotation + " "+(azimut * 360 / (2 * 3.14159f))+" ");
        CompassBackground.setRotation((azimut * 360 / (2 * 3.14159f)));
        image.setRotation(rotation);
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

}
