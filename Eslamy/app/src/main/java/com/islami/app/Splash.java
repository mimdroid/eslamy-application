package com.islami.app;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.view.Menu;
import android.view.MenuItem;
import com.islami.app.R;

import com.islami.app.Rosary.RosaryAlarmReceiver;
import com.islami.app.SalahContent.AlarmService;


public class Splash extends Activity {
    Handler handler=new Handler();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash);
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                SharedPreferences sharedPreferences=getSharedPreferences("Boot", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor=sharedPreferences.edit();
                Boolean salahAlarm=sharedPreferences.getBoolean("Alarm",false);
                    Intent zaker=new Intent(getApplicationContext(), RosaryAlarmReceiver.class);
                    sendBroadcast(zaker);
                    Intent service=new Intent(getApplicationContext(), AlarmService.class);
                    startService(service);
                    editor.putBoolean("Alarm",true);
                    editor.commit();
                Intent intent=new Intent(getApplicationContext(),HomeActivity.class);
                startActivity(intent);
            }
        },5000);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_splash, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
