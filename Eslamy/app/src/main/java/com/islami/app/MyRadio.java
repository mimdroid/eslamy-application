package com.islami.app;

import android.content.Context;
import android.media.MediaPlayer;
import android.widget.Toast;

/**
 * Created by Admin on 12/26/2015.
 */
public class MyRadio  {
    Context context;
    Thread RadioThread;
    String RadioURL;
    String Qare2Name;
    MediaPlayer mediaPlayer;
   private boolean youshouldStop=false;
   public boolean playing(){
       if (mediaPlayer==null)return false;
       return mediaPlayer.isPlaying();
   }

    MyRadio(Context c,String RadioURL,String Qare2Name){
        this.RadioURL=RadioURL;
        RadioThread=null;
        this.Qare2Name=Qare2Name;
        this.context=c;
    }
    public void Pause(){
        if (mediaPlayer!=null&&mediaPlayer.isPlaying()) {
            mediaPlayer.pause();
        }else if (mediaPlayer==null&&RadioThread!=null&&RadioThread.isAlive())youshouldStop=true;

    }

    public void stop(){
        if (mediaPlayer!=null&&mediaPlayer.isPlaying())mediaPlayer.stop();
      //  if (RadioThread!=null)RadioThread.stop();
        RadioThread=null;
        mediaPlayer=null;
        youshouldStop=true;
    }
  public void play(){
      if (mediaPlayer==null)StartStreaming();

      else if(mediaPlayer.isPlaying()==false)
      {
          mediaPlayer.start();
          Toast.makeText(context,Qare2Name+" Radio is playing",Toast.LENGTH_SHORT).show();
      }
  }

    public void StartStreaming(){
if (RadioThread==null) {
    youshouldStop=false;
    RadioThread = new myThread();
    RadioThread.start();
    Toast.makeText(context,Qare2Name+" Radio is playing",Toast.LENGTH_SHORT).show();

}


    }

    public class myThread extends Thread{
        @Override
        public void run() {
             mediaPlayer=new MediaPlayer();
                try {
                    mediaPlayer.setDataSource(RadioURL);
                    mediaPlayer.prepare();
                    if (youshouldStop){mediaPlayer.stop();return;}
                    if (!mediaPlayer.isPlaying())
                        mediaPlayer.start();

                }catch (Exception e){
                    e.printStackTrace();
                    //Toast.makeText(context,"Unable to play Radio",Toast.LENGTH_SHORT).show();

                }
            }

        }
    }

