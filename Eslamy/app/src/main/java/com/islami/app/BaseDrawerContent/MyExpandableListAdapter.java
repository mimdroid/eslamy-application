package com.islami.app.BaseDrawerContent;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.TextView;

import com.islami.app.R;

import java.util.HashMap;
import java.util.List;


/**
 * Created by Admin on 12/21/2015.
 */
public class MyExpandableListAdapter extends BaseExpandableListAdapter {

    private Context mContext;
    private List<navitemdata> mListDataHeader; // header titles

    // child data in format of header title, child title
    private HashMap<String, List<submenuitem>> mListDataChild;
    ExpandableListView  expandList;

    public MyExpandableListAdapter(Context context, List<navitemdata> listDataHeader,HashMap<String, List<submenuitem>> listChildData
            ,ExpandableListView mView)
    {
        this.mContext = context;
        this.mListDataHeader = listDataHeader;
        this.mListDataChild = listChildData;
        this.expandList=mView;
    }


    @Override
    public int getGroupCount() {
        mListDataHeader.size();
        return this.mListDataHeader.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        int childCount=0;
        if(groupPosition<=5)
            childCount=this.mListDataChild.get(this.mListDataHeader.get(groupPosition).toString()).size();

        return childCount;
    }

    @Override
    public Object getGroup(int groupPosition) {

        return this.mListDataHeader.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return this.mListDataChild.get(this.mListDataHeader.get(groupPosition).toString())
                .get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View   convertView, ViewGroup parent) {
        navitemdata headerTitle = ((navitemdata) getGroup(groupPosition));
        if (groupPosition<=7) {
            if (convertView == null){
                LayoutInflater infalInflater = (LayoutInflater) this.mContext
                        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.nav_list_header, null);
        }
        TextView lblListHeader = (TextView) convertView.findViewById(R.id.listheader);
        ImageView headerIcon=(ImageView)convertView.findViewById(R.id.headericon);
        lblListHeader.setTypeface(null, Typeface.BOLD);
        lblListHeader.setText(headerTitle.title);
        headerIcon.setImageResource(headerTitle.id);
        }


        return convertView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition,  boolean isLastChild, View convertView, ViewGroup parent) {
        final submenuitem child = (submenuitem) getChild(groupPosition, childPosition);

        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this.mContext
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.nav_list_submenu, null);
        }

        TextView txtListChild = (TextView) convertView
                .findViewById(R.id.submenutext);

        ImageView imagelistchiled = (ImageView) convertView
                .findViewById(R.id.submenuicon);

        txtListChild.setText(child.subname);
        imagelistchiled.setImageResource(child.iconid);

        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

}



