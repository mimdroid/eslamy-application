package com.islami.app.elsomsection;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import com.islami.app.R;

/**
 * Created by FCI on 1/8/2016.
 */
public class elomtlk extends Fragment {

    LayoutInflater inflater;
    String fontPath = "fonts/GESSTwoLight.otf";
    Typeface tf ;

    public elomtlk() {

    }
TextView text1 , text2;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.elmotlk, container, false);
        this.inflater = inflater;
        InputStream Is=null;
        text1=(TextView)view.findViewById(R.id.elmotlk1);
        text2=(TextView)view.findViewById(R.id.elmotlk2);
        tf = Typeface.createFromAsset(getContext().getResources().getAssets(), fontPath);

        try {
            Is = getResources().getAssets().open("elmotlk1");
            text1.setText(GetData(Is));
            text1.setTypeface(tf);

            Is = getResources().getAssets().open("elomtlk2");
            text2.setText(GetData(Is));
            text2.setTypeface(tf);
            Is = getResources().getAssets().open("elmotlk3");
            text2.setText(text2.getText()+ " \n " + " \n "+ " \n "+GetData(Is));
        } catch (IOException e) {
            e.printStackTrace();
        }
            return view;
    }

    public String GetData(InputStream inputStream){
        InputStream iS = null;
        String d="Not Found";
        try {
            iS=inputStream;
            BufferedReader reader = new BufferedReader(new InputStreamReader(iS));
            StringBuilder sb = new StringBuilder();
            String line = reader.readLine();
            sb.append(line);
            d=sb.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return d;
    }
}