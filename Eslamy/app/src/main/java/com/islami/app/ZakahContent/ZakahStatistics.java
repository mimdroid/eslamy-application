package com.islami.app.ZakahContent;

import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Date;

import DataBase.DBInterface;
import com.islami.app.R;


public class ZakahStatistics extends Fragment {
    TextView yearText;
    Typeface custom_font_arabic;
    Date now;
    String Lang="arabic";
    TextView text1,text2,text3,text4,text5,text6,text7,text8,text9;
    TextView count1,count2,count3,count4,count5,count6,count7;
    Context context;
    View view;
    public ZakahStatistics()
    {
    }
    @Override
    public void onCreate( Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
         view = inflater.inflate(R.layout.zakah_statistics, container, false);
        context = view.getContext();

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        custom_font_arabic = Typeface.createFromAsset(context.getAssets(), "fonts/GESSTwoLight.otf");
        yearText=(TextView)view.findViewById(R.id.year);
        changeLang();
        DBInterface dbInterface=new DBInterface(context);
        ArrayList <Double>arrayList=dbInterface.SelectZakah(now.getYear()+1900);
        count1.setText(arrayList.get(0)+"");
        count2.setText(arrayList.get(1)+"");
        count3.setText(arrayList.get(2)+"");
        count4.setText(arrayList.get(3)+"");
        count5.setText(arrayList.get(4)+"");
        count6.setText(arrayList.get(5)+"");
        double d=0;
        for(int i=0;i<6;i++)d+=arrayList.get(i);
        count7.setText(d+"");
        Call();
    }

    public void Call()
    {
        Button AllYears=(Button)view.findViewById(R.id.AllYears);
        ImageButton NextYear=(ImageButton)view.findViewById(R.id.NextYear);
        ImageButton PrevYear=(ImageButton)view.findViewById(R.id.PrevYear);
        AllYears.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AllYears();
            }
        });
        NextYear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NextYear();
            }
        });
        PrevYear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PrevYear();
            }
        });

    }

    public void changeLang()
    {
        now=new Date();
        yearText.setText(now.getYear()+1900+"");
        text1=(TextView)view.findViewById(R.id.text1);
        text2=(TextView)view.findViewById(R.id.text2);
        text3=(TextView)view.findViewById(R.id.text3);
        text4=(TextView)view.findViewById(R.id.text4);
        text5=(TextView)view.findViewById(R.id.text5);
        text6=(TextView)view.findViewById(R.id.text6);
        text7=(TextView)view.findViewById(R.id.text7);
        text8=(TextView)view.findViewById(R.id.text8);
        text9=(TextView)view.findViewById(R.id.text9);
        count1=(TextView)view.findViewById(R.id.count1);
        count2=(TextView)view.findViewById(R.id.count2);
        count3=(TextView)view.findViewById(R.id.count3);
        count4=(TextView)view.findViewById(R.id.count4);
        count5=(TextView)view.findViewById(R.id.count5);
        count6=(TextView)view.findViewById(R.id.count6);
        count7=(TextView)view.findViewById(R.id.count7);
        if(Lang.equals("arabic"))
        {
            yearText.setTypeface(custom_font_arabic);
            text1.setTypeface(custom_font_arabic);
            text2.setTypeface(custom_font_arabic);
            text3.setTypeface(custom_font_arabic);
            text4.setTypeface(custom_font_arabic);
            text5.setTypeface(custom_font_arabic);
            text6.setTypeface(custom_font_arabic);
            text7.setTypeface(custom_font_arabic);
            text8.setTypeface(custom_font_arabic);
            text9.setTypeface(custom_font_arabic);
            count1.setTypeface(custom_font_arabic);
            count2.setTypeface(custom_font_arabic);
            count3.setTypeface(custom_font_arabic);
            count4.setTypeface(custom_font_arabic);
            count5.setTypeface(custom_font_arabic);
            count6.setTypeface(custom_font_arabic);
            count7.setTypeface(custom_font_arabic);

        }
        else
        {
            text1.setText("Amount");
            text2.setText("report");
            text3.setText("Annual Zakat");
            text4.setText("Zakat al-Fitr");
            text5.setText("Ongoing charity");
            text6.setText("Charity relatives");
            text7.setText("Charity Kids");
            text8.setText("Other");
            text9.setText("Total");
        }
    }
    public void NextYear()
    {
        now.setYear(now.getYear()+1);
        yearText.setText(now.getYear()+1900+"");
        DBInterface dbInterface=new DBInterface(context);
        ArrayList <Double>arrayList=dbInterface.SelectZakah(now.getYear()+1900);
        count1.setText(arrayList.get(0)+"");
        count2.setText(arrayList.get(1)+"");
        count3.setText(arrayList.get(2)+"");
        count4.setText(arrayList.get(3)+"");
        count5.setText(arrayList.get(4)+"");
        count6.setText(arrayList.get(5)+"");
        double d=0;
        for(int i=0;i<6;i++)d+=arrayList.get(i);
        count7.setText(d+"");

    }
    public void PrevYear()
    {
        now.setYear(now.getYear()-1);
        yearText.setText(now.getYear()+1900+"");
        DBInterface dbInterface=new DBInterface(context);
        ArrayList <Double>arrayList=dbInterface.SelectZakah(now.getYear()+1900);
        count1.setText(arrayList.get(0)+"");
        count2.setText(arrayList.get(1)+"");
        count3.setText(arrayList.get(2)+"");
        count4.setText(arrayList.get(3)+"");
        count5.setText(arrayList.get(4)+"");
        count6.setText(arrayList.get(5)+"");
        double d=0;
        for(int i=0;i<6;i++)d+=arrayList.get(i);
        count7.setText(d+"");

    }
    public void AllYears()
    {
        DBInterface dbInterface=new DBInterface(context);
        ArrayList <Double>arrayList=dbInterface.SelectAllZakah();
        count1.setText(arrayList.get(0)+"");
        count2.setText(arrayList.get(1)+"");
        count3.setText(arrayList.get(2)+"");
        count4.setText(arrayList.get(3)+"");
        count5.setText(arrayList.get(4)+"");
        count6.setText(arrayList.get(5)+"");
        double d=0;
        for(int i=0;i<6;i++)d+=arrayList.get(i);
        count7.setText(d+"");

    }
}
