package com.islami.app.Qur2anContent;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import DataBase.DBInterface;
import Manage.Reader;
import com.islami.app.Prefs;
import com.islami.app.R;

public class ReadSura extends AppCompatActivity {

    int suraid;
    String ssuraid;
    suraAdapter adapter;
    ListView listView;
    HashSet<Integer>all;
    Typeface custom_font_arabic;
    DBInterface dbInterface;
    int mSelecteditem=-1;
    Set<Integer>ReadedAyas=new HashSet<>();
    Date d=new Date();
    int TotalAyas=0;
    int starth;
    int startM;
    long currDate;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_read_sura);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        custom_font_arabic = Typeface.createFromAsset(getApplicationContext
                ().getAssets(), "fonts/GESSTwoLight.otf");
        ssuraid=getIntent().getStringExtra("suraid");
        suraid=Integer.valueOf(ssuraid);
        listView=(ListView)findViewById(R.id.AyatList);
        TextView title=(TextView)findViewById(R.id.suraname);
        title.setText(DataNeeded.EnSuras[suraid-1]);
        Reader r=new Reader(this);
        String[]Ayat=r.readSura(ssuraid+".txt");
        starth=d.getHours();
        startM=d.getMinutes();
        currDate=(long) ( d.getDate()+((d.getMonth()+1)*12)+((d.getYear())*365) );
         dbInterface=new DBInterface(this);
        all=dbInterface.getAyasofSura(suraid);
        adapter=new suraAdapter(this, Ayat);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                ReadedAyas.add(position);
                adapter.notifyDataSetChanged();
                mSelecteditem=position;
                TotalAyas+=1;
            }
        });
        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {

                if (all.contains(position)){all.remove(position);
                    Toast.makeText(getApplicationContext(),"removed",Toast.LENGTH_SHORT);
                    adapter.notifyDataSetChanged();
                    dbInterface.unhefz(suraid,position);
                }
               else {
                    Toast.makeText(getApplicationContext(),"added",Toast.LENGTH_SHORT);
                    all.add(position);
                adapter.notifyDataSetChanged();
                dbInterface.insertQuranHefz(suraid,position,currDate);
     }
                return false;
            }
        });

        try {

            String s=getIntent().getStringExtra("isbookmark");
            if (s!=null){
            SharedPreferences prefs = getSharedPreferences(Prefs.BOOK_MARK_PREF, Context.MODE_PRIVATE);
            int AyaPos = prefs.getInt("Aya_Pos", -1);
            if (AyaPos != -1){
                listView.setSelection(AyaPos);
                }
            }

}catch (ClassCastException e){

}
        //Button hefz,bookmark,listen;
        //hefz=(Button)findViewById(R.id.hefz);
        //listen=(Button)findViewById(R.id.listentoaya);
        //bookmark=(Button)findViewById(R.id.ayabookmark);
        AdView mAdView = (AdView) findViewById(R.id.adViewmain);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);

    }

    @Override
    protected void onStop() {
        super.onStop();
       if (TotalAyas==0)return;
        Date d=new Date();
        int ch= d.getHours();
        int cm=d.getMinutes();
        int total;
        if (ch==starth)total=(60-startM)+cm;
        else total=(((ch-starth-1)*60)-(60-startM)+cm);

        dbInterface.insertQuranRead(TotalAyas,total,currDate);

    }

    public void hefz(View v){

    }
    public void listen(View v){

    }
    public void bookmark(View v){

            int x = listView.getFirstVisiblePosition(); //this is the first visible row
        SharedPreferences.Editor editor = getSharedPreferences(Prefs.BOOK_MARK_PREF, MODE_PRIVATE).edit();
        editor.putString("Sura_ID",ssuraid);
        editor.putInt("Aya_Pos",x);
        editor.commit();

            Log.e("scroll",x+"");
    }


    private class suraAdapter extends BaseAdapter{
        String[] Ayat;
        String Lang="arabic";
        //Typeface custom_font_arabic;
        int index;
        Context context;
        private  LayoutInflater inflater=null;
        public suraAdapter(Context c,String[]ayat)
        {

            context=c;
            this.Ayat=ayat;
            //custom_font_arabic= Typeface.createFromAsset(context.getAssets(), "fonts/GESSTwoLight.otf");
//Lang=;
            inflater = ( LayoutInflater )context.
                    getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }
        @Override
        public int getCount() {
            return Ayat.length;
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            Holder holder=new Holder();
            View rowView;
            rowView = inflater.inflate(R.layout.aya_item, null);

            holder.tv=(TextView) rowView.findViewById(R.id.ayatextview);
                holder.tv.setText(Ayat[position]+"("+((int)position+1)+")");
               holder.tv.setTypeface(custom_font_arabic);
         if (all.contains(position)==true){
             holder.tv.setBackgroundColor(Color.argb(75,255,255,255));

         }
            if(position==mSelecteditem){
                holder.tv.setBackgroundColor(Color.argb(110,235,196,103));
            }

            return rowView;
        }
        public class Holder
        {
            TextView tv;
        }


    }

}
