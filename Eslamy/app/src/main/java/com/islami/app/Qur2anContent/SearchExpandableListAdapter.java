package com.islami.app.Qur2anContent;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.TextView;

import java.util.ArrayList;

import com.islami.app.R;

/**
 * Created by Admin on 12/21/2015.
 */
public class SearchExpandableListAdapter extends BaseExpandableListAdapter {

    private Context mContext;

    // child data in format of header title, child title
    ExpandableListView  expandList;

    public SearchExpandableListAdapter(Context context, ExpandableListView mView)
    {
        this.mContext = context;
        this.expandList=mView;
    }


    @Override
    public int getGroupCount() {

        return DataNeeded.suras.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {

        return DataNeeded.suras.get(groupPosition).getFounded().size();
    }

    @Override
    public Object getGroup(int groupPosition) {

        return DataNeeded.suras.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return DataNeeded.suras.get(groupPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View   convertView, ViewGroup parent) {
        suraStructure headerTitle = ((suraStructure) getGroup(groupPosition));

            if (convertView == null){
                LayoutInflater infalInflater = (LayoutInflater) this.mContext
                        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.aya_item, null);
        }
        TextView lblListHeader = (TextView) convertView.findViewById(R.id.ayatextview);
        lblListHeader.setTypeface(null, Typeface.BOLD);
        lblListHeader.setText(headerTitle.getsuraName("ar"));



        return convertView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition,  boolean isLastChild, View convertView, ViewGroup parent) {
        final suraStructure child = (suraStructure) getChild(groupPosition, childPosition);

        if (convertView == null){
            LayoutInflater infalInflater = (LayoutInflater) this.mContext
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.search_result_list_submenu, null);
        }

        TextView sura = (TextView) convertView.findViewById(R.id.sura_details);
     //   TextView sura = (TextView) convertView.findViewById(R.id.search_aya_details);


       // if(aya!=null)
       ArrayList<Integer>ids=child.getFounded();
         sura.setText(child.getAya(ids.get(childPosition))+"\n");
        //    Log.e("aa",child.getAya(i));

        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

}



