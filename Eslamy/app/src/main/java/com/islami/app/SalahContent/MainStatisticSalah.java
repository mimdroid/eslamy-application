package com.islami.app.SalahContent;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import java.util.ArrayList;

import com.islami.app.R;

/**
 * Created by Ibrahim on 1/10/2016.
 */
public class MainStatisticSalah extends Fragment {
    ArrayList<Fragment> fragments;
    ViewPager viewPager;
    MypagerAdapter pageAdapter;
    private TabLayout tabLayout;
    View view;
    private int[] tabIcons = {
            R.drawable.recordindicator,
            R.drawable.resultindicator,
    };
    @Override
    public void onCreate( Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
    int tabnum;
    public void setTabnum(int tabnum) {
        this.tabnum = tabnum;
    }
    public MainStatisticSalah(){}
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.main_statistic_zakah, container, false);
        tabLayout = (TabLayout) view.findViewById(R.id.tabs1);
        viewPager=(ViewPager)view.findViewById(R.id.viewPagerContainer1);
        setupViewPager(viewPager);
        setuptabLayout();
        Log.e("bef", "called");
        tabLayout.getTabAt(tabnum).select();

        return view;
    }


    public void setupViewPager(ViewPager viewPager) {
        pageAdapter = new MypagerAdapter(viewPager.getContext(),getChildFragmentManager());
        pageAdapter.addFragment(new RecordingStatisticsPrayer(),tabIcons[0]);
        pageAdapter.addFragment(new SalahStatistics(),tabIcons[1]);
        viewPager.setAdapter(pageAdapter);
    }
    public void setuptabLayout(){
        try {
            tabLayout.setupWithViewPager(viewPager);
            for (int i = 0; i < tabLayout.getTabCount(); i++) {
                TabLayout.Tab tab = tabLayout.getTabAt(i);
                tab.setCustomView(pageAdapter.getTabView(i));
            }
            tabLayout.requestFocus();
        }
        catch(Exception ex){
            Log.e("errr2", ex.getMessage());
        }
    }
    public class MypagerAdapter extends FragmentStatePagerAdapter {
        Context mContext;
        ArrayList<Fragment>fragments=new ArrayList<>();
        ArrayList<Integer>Icons=new ArrayList<>();

        @Override
        public Fragment getItem(int position) {
            return fragments.get(position);
        }

        @Override
        public int getCount() {
            return fragments.size();
        }

        public MypagerAdapter(Context context, FragmentManager fm) {
            super(fm);
            this.mContext = context;
        }

        public void addFragment(Fragment fragment,  int drawable) {
            fragments.add(fragment);
            Icons.add(drawable);
        }




        @Override
        public int getItemPosition(Object object)
        {
            return POSITION_UNCHANGED;
        }
        @Override
        public CharSequence getPageTitle(int position) {
            return null;
        }
        public View getTabView(int position) {
            View tab = LayoutInflater.from(mContext).inflate(R.layout.tabindicator, null);
            ImageView tabImage = (ImageView) tab.findViewById(R.id.icon);
            tabImage.setBackgroundResource(Icons.get(position));
            return tab;
        }

    }
}
