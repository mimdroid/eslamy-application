package com.islami.app.elsomsection;

import android.content.Context;
import android.graphics.Typeface;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.GridView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import Manage.GPS;
import Manage.PrayTime;
import Manage.Reader;
import com.islami.app.R;

/**
 * Created by FCI on 1/2/2016.
 */
public class ramadanActivity extends Fragment {

    int Geoyear;
    int GeoDay;
    int GeoMonth;
    int Hijriyear;
    int ramadanDay=1 , ramadanmonth=9;
    String HijriRamdanArabic="01 رمضان";
    String HijriRamdanEnglish="  01 Ramadan";
    TextView HijriText;
    TextView GeoText;
    TextView YearText;
    Button al1,ar1,al2,ar2;
    private ConverterDate Converter;
    final String [] MonthNamesArabic={"يناير"  , "فبراير ","مارس", "ابريل","مايو", "يونيو","يوليو","اغسطس","سبتمبر","أكتوبر","نوفمبر","ديسمبر"};
    final String [] MonthNamesEnglish={"January","February","March","April","May","June","July","August","September","October","November","December"};
    final String []DaysOfWeekArabic={"السبت", "الجمعه","الخميس","الاربعاء", "الثلاثاء", "الاتنين", "الاحد"};
    final String []DaysOfWeekEnglish={"Su", "Mon","Tu","WE","Th ","Fri ", "Sa " };
   // final int []IDs={R.id.day1,R.id.day2,R.id.day3,R.id.day4,R.id.day5,R.id.day6,R.id.day7};
    String []tempMonth ;
    String HijriRamdan;
    String lang="A";
    public CalendarAdapter adapter;// adapter instance
    public ArabicCalendar adap;
    public addapterdays day;
    Calendar month ;
    View view ;
    String fontPath = "fonts/GESSTwoLight.otf";
    TextView movingtext;
    TextView elfagr , elmahrb;
    LocationManager locationManager;
    GPS gps;

    @Override
public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
     view = inflater.inflate(R.layout.ramadan_activity, container, false);
        Typeface tf = Typeface.createFromAsset(getResources().getAssets(), fontPath);

        Converter =new ConverterDate();
        if(lang.equals("A")){
            HijriRamdan =HijriRamdanArabic;
            tempMonth=MonthNamesArabic;
//            loaddaysofweek(DaysOfWeekArabic);

        }
        else {
            HijriRamdan =HijriRamdanEnglish;
            tempMonth=MonthNamesEnglish;
  //          loaddaysofweek(DaysOfWeekEnglish);
        }
        locationManager= (LocationManager) getContext().getSystemService(Context.LOCATION_SERVICE);
        gps=new GPS(locationManager);

        YearText = (TextView) view.findViewById(R.id.Geoyear1);
        HijriText = (TextView) view.findViewById(R.id.Higriyear);
        GeoText = (TextView) view.findViewById(R.id.GeoDate);
        movingtext=(TextView)view.findViewById(R.id.movingtext);
        Reader reader=new Reader(getContext());
        movingtext.setText(reader.readBlessingsArabic());
        elfagr=(TextView)view.findViewById(R.id.elfagr);
        elmahrb=(TextView)view.findViewById(R.id.elmahrb);
        Calendar calendar = Calendar.getInstance();
        Geoyear = calendar.get(Calendar.YEAR);
        YearText.setText(String.valueOf(Geoyear));
        Hijriyear = Converter.GetHijriYear(Geoyear);
        al1=(Button)view.findViewById(R.id.arrowleft1);
        ar1=(Button)view.findViewById(R.id.arrowright1);
        al2=(Button)view.findViewById(R.id.arrowleft2);
        ar2=(Button)view.findViewById(R.id.arrowright2);
        GeoDay = Converter.GetGeoDay(Hijriyear, ramadanmonth, ramadanDay);
        GeoMonth = Converter.GetGeoMonth(Hijriyear, ramadanmonth, ramadanDay);
        Geoyear = Converter.GetGeoYear(Hijriyear, ramadanmonth ,ramadanDay);
        HijriText.setText(" " + HijriRamdan + Hijriyear);
        GeoText.setText(GeoDay + " " + tempMonth[GeoMonth - 1] + " " + Geoyear);
        YearText.setTypeface(tf);
        HijriText.setTypeface(tf);
        GeoText.setTypeface(tf);

        month = new GregorianCalendar(Geoyear,GeoMonth-1,GeoDay);
        adap = new ArabicCalendar(getActivity(),month.get(Calendar.DAY_OF_WEEK),month.getActualMaximum(Calendar.DAY_OF_MONTH ),GeoDay,GeoMonth,Geoyear,GeoDay);
        adapter = new CalendarAdapter(getActivity(), (GregorianCalendar) month , GeoDay);

        GridView gridview = (GridView) view.findViewById(R.id.gridview);

        if(lang.equals("A")){
            gridview.setAdapter(adap);
            day=new addapterdays(getActivity(),DaysOfWeekArabic,tf);
        }
        else gridview.setAdapter(adapter);
        al1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                prevyear();
            }
        });
        al2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                prevMonth();
            }
        });
        ar1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                nextyear();
            }
        });
        ar2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                nextMonth();
            }
        });
        Date d = new Date(Geoyear,GeoMonth-1,GeoDay);
        setTimePrayer(d);
        return view;
}
/*
  public void loaddaysofweek(String [] day){
        for(int i=0; i <7;i++){
            TextView t =(TextView)view.findViewById(IDs[i]);
            t.setText(day[i]);
        }
    }
  */
public void nextyear()
    {
        Geoyear+=1;
        YearText.setText(String.valueOf(Geoyear));
        Hijriyear=Converter.GetHijriYear(Geoyear);

        HijriText.setText(" "+HijriRamdan+Hijriyear);
        GeoDay=Converter.GetGeoDay(Hijriyear, ramadanmonth, ramadanDay);
        GeoMonth=Converter.GetGeoMonth(Hijriyear, ramadanmonth, ramadanDay);
        Geoyear=Converter.GetGeoYear(Hijriyear, ramadanmonth, ramadanDay);
        GeoText.setText(GeoDay + " " + tempMonth[GeoMonth - 1] + " " + Geoyear);
        month = new GregorianCalendar(Geoyear,GeoMonth -1,GeoDay);
        adapter = new CalendarAdapter(getActivity(), (GregorianCalendar) month , GeoDay);
        adap = new ArabicCalendar(getActivity(),month.get(Calendar.DAY_OF_WEEK),month.getActualMaximum(Calendar.DAY_OF_MONTH ),1,GeoMonth,Geoyear,GeoDay);
        GridView gridview = (GridView) view.findViewById(R.id.gridview);
        if(lang.equals("A"))
            gridview.setAdapter(adap);
        else gridview.setAdapter(adapter);
        Date d = new Date(Geoyear,GeoMonth-1,GeoDay);
        setTimePrayer(d);

    }

    public void prevyear()
    {    Geoyear-=1;
        YearText.setText(String.valueOf(Geoyear));
        Hijriyear=Converter.GetHijriYear(Geoyear);
        HijriText.setText(" "+HijriRamdan+Hijriyear);
        GeoDay=Converter.GetGeoDay(Hijriyear, ramadanmonth, ramadanDay);
        GeoMonth=Converter.GetGeoMonth(Hijriyear, ramadanmonth, ramadanDay);
        Geoyear=Converter.GetGeoYear(Hijriyear, ramadanmonth ,ramadanDay);
        GeoText.setText(GeoDay + " " + tempMonth[GeoMonth - 1] + " " + Geoyear);
        month = new GregorianCalendar(Geoyear,GeoMonth -1,GeoDay);
        adap = new ArabicCalendar(getActivity(),month.get(Calendar.DAY_OF_WEEK),month.getActualMaximum(Calendar.DAY_OF_MONTH ),1,GeoMonth,Geoyear,GeoDay);
        adapter = new CalendarAdapter(getActivity(), (GregorianCalendar) month , GeoDay);
        GridView gridview = (GridView) view.findViewById(R.id.gridview);
        if(lang.equals("A"))
            gridview.setAdapter(adap);
        else gridview.setAdapter(adapter);
        Date d = new Date(Geoyear,GeoMonth-1,GeoDay);
        setTimePrayer(d);
    }

    public void nextMonth()
    {
        Hijriyear+=1;
        HijriText.setText(" "+HijriRamdan+Hijriyear);
        GeoDay=Converter.GetGeoDay(Hijriyear, ramadanmonth, ramadanDay);
        GeoMonth=Converter.GetGeoMonth(Hijriyear, ramadanmonth, ramadanDay);
        Geoyear=Converter.GetGeoYear(Hijriyear, ramadanmonth, ramadanDay);
        YearText.setText(String.valueOf(Geoyear));
        GeoText.setText(String.valueOf(Geoyear));
        GeoText.setText(GeoDay+" "+tempMonth[GeoMonth-1]+" "+Geoyear);
        month = new GregorianCalendar(Geoyear,GeoMonth-1 ,GeoDay);
        adap = new ArabicCalendar(getActivity(),month.get(Calendar.DAY_OF_WEEK),month.getActualMaximum(Calendar.DAY_OF_MONTH ),1,GeoMonth,Geoyear,GeoDay);
        adapter = new CalendarAdapter(getActivity(), (GregorianCalendar) month , GeoDay);
        GridView gridview = (GridView) view.findViewById(R.id.gridview);
        if(lang.equals("A"))
            gridview.setAdapter(adap);
        else gridview.setAdapter(adapter);
        Date d = new Date(Geoyear,GeoMonth-1,GeoDay);
        setTimePrayer(d);
    }
    public void prevMonth()
    {
        Hijriyear-=1;
        HijriText.setText(" "+HijriRamdan+Hijriyear);
        GeoDay=Converter.GetGeoDay(Hijriyear, ramadanmonth, ramadanDay);
        GeoMonth=Converter.GetGeoMonth(Hijriyear, ramadanmonth, ramadanDay);
        Geoyear=Converter.GetGeoYear(Hijriyear, ramadanmonth, ramadanDay);
        YearText.setText(String.valueOf(Geoyear));
        GeoText.setText(String.valueOf(Geoyear));
        GeoText.setText(GeoDay+" "+tempMonth[GeoMonth-1]+" "+Geoyear);
        month = new GregorianCalendar(Geoyear,GeoMonth-1,GeoDay);
        adap = new ArabicCalendar(getActivity(),month.get(Calendar.DAY_OF_WEEK),month.getActualMaximum(Calendar.DAY_OF_MONTH ),1,GeoMonth,Geoyear,GeoDay);
        adapter = new CalendarAdapter(getActivity(), (GregorianCalendar) month , GeoDay);
        GridView gridview = (GridView) view.findViewById(R.id.gridview);
        if(lang.equals("A"))
            gridview.setAdapter(adap);
        else gridview.setAdapter(adapter);
        Date d = new Date(Geoyear,GeoMonth-1,GeoDay);
        setTimePrayer(d);
    }


public void setTimePrayer(Date date){
    double timezone = (Calendar.getInstance().getTimeZone()
            .getOffset(Calendar.getInstance().getTimeInMillis()))
            / (1000 * 60 * 60);
    PrayTime prayers = new PrayTime();

    prayers.setTimeFormat(prayers.Time12);
    prayers.setCalcMethod(prayers.Makkah);
    prayers.setAsrJuristic(prayers.Shafii);
    prayers.setAdjustHighLats(prayers.AngleBased);
    int[] offsets = { 0, 0, 0, 0, 0, 0, 0 }; // {Fajr,Sunrise,Dhuhr,Asr,Sunset,Maghrib,Isha}
    prayers.tune(offsets);

    Date now = date;
//        now.setDate(32);

    Log.v("Date1", now.toString());
    Calendar cal = Calendar.getInstance();
    cal.setTime(now);
    ArrayList<String> arrayList = prayers.getPrayerTimes(cal, gps.get_Latitude(),
            gps.get_Longitude(), timezone);
    ArrayList prayerNames = prayers.getTimeNames();
    elfagr.setText(arrayList.get(0));
    elmahrb.setText(arrayList.get(5));
}




}
