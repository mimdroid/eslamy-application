package com.islami.app.ZakahContent;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.TextView;

import org.joda.time.Chronology;
import org.joda.time.LocalDate;
import org.joda.time.chrono.ISOChronology;
import org.joda.time.chrono.IslamicChronology;

import java.util.ArrayList;

import com.islami.app.R;

/**
 * Created by Ibrahim on 1/8/2016.
 */
public class ArabicCalendar extends BaseAdapter {
    ArrayList<String> arrayList=new ArrayList<>();
    Context context;
    TextView english;
    TextView arabic;
    int start=0;
    Typeface custom_font_arabic;
    int index=1;
    int month=0,year=0;
    String months[]=new String[]{"يناير","فبراير","مارس","ابرايل","مايو","يونيه","يوليو","أغسطس","ستمبر","أكتوبر","نوفمبر","ديسمبر"};
    ArrayList<TextView>views=new ArrayList<>();
    String[] arabic_H_Months={"محرم","صفر","ربيع الأول","ربيع الثاني","جمادي الأول","جمادي الثاني","رجب","شعبان","رمضان","شوال","ذو القعدة","ذو الحجة"};
    Chronology hijri;
    Chronology iso;

    public ArabicCalendar(Context c,int s,int l,TextView e,TextView a,int day,int mon,int ye)
    {
        context=c;
        this.start=s;
        english=e;
        arabic=a;
        this.index=day;
        this.month=mon;
        this.year=ye;
        custom_font_arabic= Typeface.createFromAsset(context.getAssets(), "fonts/GESSTwoLight.otf");
        for(int i=0;i<start;i++)arrayList.add("");
        for(int i=1;i<=l;i++)arrayList.add(" "+i+"");
        if(arrayList.size()%7!=0)
        {
            int dif=7-(arrayList.size()%7);
            for(int i=0;i<dif;i++)arrayList.add("");
        }
        for(int i=0;i<arrayList.size();i++)
        {
            int k=i+6;
            for(int j=i;j<i+3;j++,k--)
            {
                String temp=arrayList.get(j);
                arrayList.set(j,arrayList.get(k));
                arrayList.set(k,temp);
            }
            i+=6;
        }
        for(int i=0;i<arrayList.size();i++)
        {

            final TextView textView = new TextView(context);
            textView.setText(""+arrayList.get(i));
            textView.setLayoutParams(new GridView.LayoutParams(35, ViewGroup.LayoutParams.WRAP_CONTENT));
            textView.setTextSize(10);
            textView.setGravity(View.TEXT_ALIGNMENT_CENTER);
            textView.setClickable(true);
            textView.setTypeface(custom_font_arabic);
            textView.setPadding(5,5,5,5);
            textView.setTextColor(Color.parseColor("#FFFFFF"));
            views.add(textView);
        }
        for(int i=0;i<views.size();i++)
        {
            final int ii=i;

            views.get(i).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(ii>start)
                    {
                        String s=views.get(ii).getText().toString();
                        s=s.replace(" ","");
                        index=Integer.valueOf(s);
                        english.setText(index+"    "+months[month]+"    "+(year)+"");
                        views.get(ii).setBackgroundResource(R.drawable.roundedabout);
                        iso = ISOChronology.getInstanceUTC();
                        hijri = IslamicChronology.getInstanceUTC();
                        LocalDate todayIso = new LocalDate(year, month+1,index, iso);
                        LocalDate todayHijri = new LocalDate(todayIso.toDateTimeAtStartOfDay(),
                                hijri);
                        int arabicDay=todayHijri.getDayOfMonth();
                        int arabicMonth=todayHijri.getMonthOfYear();
                        int arabicYear=todayHijri.getYear();
                        arabic.setText(arabicDay+"  "+arabic_H_Months[arabicMonth]+"   "+arabicYear);
                        for(int j=0;j<views.size();j++)
                        {
                            if(j!=ii)
                            views.get(j).setBackgroundColor(Color.parseColor("#745D4F"));


                        }
                    }

                }
            });
         //   for(int j=0;j<views.size();j++)

        }
     //   Log.v("test",arrayList.size()+" ");

   //     this.index=index;
    }
    public int getIndex()
    {
        return index;
    }
    @Override
    public int getCount() {
        return arrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return arrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        TextView textView=views.get(position);

       /* textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(position>start)
                {
                    index=position-start;
                    english.setText(position-start+"");
                    textView.setBackgroundResource(R.drawable.roundedabout);

                }

              //  textView.setBackgroundColor(Color.parseColor("#000000"));
            }
        });*/
     //   Log.v("testview",position+"");
        return textView;
    }}
