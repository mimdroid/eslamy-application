package com.islami.app.Rosary;

import android.annotation.TargetApi;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.PixelFormat;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;

import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

import com.islami.app.R;

/**
 * Created by Ibrahim on 12/31/2015.
 */
public class RosaryService extends Service {
    Timer timer;
    TimerTask timerTask;
    SharedPreferences sharedPreferences;
    String Lang="arabic";
    String arr[]=new String[7];
    Context context;
    @Override
    public IBinder onBind(Intent intent) {
        startTimer();

        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        context=this;
        sharedPreferences=getSharedPreferences("Salah_Alarm", Context.MODE_PRIVATE);


        startTimer();

    }
    public void startTimer()
    {
      //  timer=new Timer();
        startTask();
      //  timer.schedule(timerTask, 5000,1*30*1000);
    }
    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    public  void startTask()
    {
        Log.d("test", "start_task_service");
        Boolean b=sharedPreferences.getBoolean("zakerAlarm",false);
                if(b==false)return;
                Random random=new Random();
                double r=random.nextDouble()*100;
                int x=((int)r)%7;
        if(Lang.equals("arabic"))
        {
            arr[0]="الله أكبر";
            arr[1]="الحمد الله";
            arr[2]="سبحان الله";
            arr[3]=" لا اله الا الله";
            arr[4]="أستغر الله العظيم";
            arr[5]="لا حول ولا قوة الا بالله";
            arr[6]="اللهم صلى على سيدنا محمد";
        }
        else
        {
            arr[0]="Allah is the greatest";
            arr[1]="Thank god";
            arr[2]="Glory of God";
            arr[3]="No God unless Allah";
            arr[4]="i ask for forgiveness from the mighty Allah";
            arr[5]="There is no power unless by Allah";
            arr[6]="Pray to God the Prophet Mohamed";
        }

        String s=arr[x];

                 final TextView textView = new TextView(this);
                 final WindowManager windowManager = (WindowManager) getSystemService(WINDOW_SERVICE);
                textView.setText(s);
                textView.setTextSize(15);
                textView.setTextColor(Color.parseColor("#FFFFFF"));
                textView.setBackground(getResources().getDrawable(R.drawable.roundedabout));
                textView.setPadding(20,20,20,20);
                final WindowManager.LayoutParams params = new WindowManager.LayoutParams(
                        WindowManager.LayoutParams.WRAP_CONTENT,
                        WindowManager.LayoutParams.WRAP_CONTENT,
                        WindowManager.LayoutParams.TYPE_PHONE,
                        WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
                        PixelFormat.TRANSLUCENT);
                params.windowAnimations = android.R.style.Animation_Translucent;
                params.gravity = Gravity.TOP | Gravity.CENTER;
                params.x = 0;
                params.y = 100;
                windowManager.addView(textView, params);
                //chatHead.startAnimation(animation);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            windowManager.removeView(textView);

                        } catch (Exception e) {

                        }
                    }
                }, 5000);
                textView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        windowManager.removeView(textView);
                    }
                });

    }
    public void stoptimertask() {
        //stop the timer, if it's not already null
        if (timer != null) {
            timer.cancel();
            timer = null;
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        stoptimertask();
    }
}
