package com.islami.app.Rosary;

import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Date;

import DataBase.DBInterface;
import com.islami.app.R;


public class RosaryStatistics extends Fragment {
    Date from,to;
    Spinner spinnerFrom;
    Spinner spinnerTo;
    TextView count1;
    TextView count2;
    TextView count3;
    TextView count4;
    TextView count5;
    TextView count6;
    TextView count7;
    TextView count8;
    EditText dayFrom,dayTo,yearFrom,yearTo;
    int monthFrom,monthTo;
    Typeface custom_font_arabic;
    Context context;
    View view;
    public RosaryStatistics()
    {

    }
    @Override
    public void onCreate( Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.activity_rosary_statistics, container, false);
        context = view.getContext();
        custom_font_arabic = Typeface.createFromAsset(context.getAssets(), "fonts/GESSTwoLight.otf");
        dayFrom=(EditText)view.findViewById(R.id.dayFrom);
        dayTo=(EditText)view.findViewById(R.id.dayTo);
        yearFrom=(EditText)view.findViewById(R.id.yearFrom);
        yearTo=(EditText)view.findViewById(R.id.yearTo);
        count1=(TextView)view.findViewById(R.id.count1);
        count2=(TextView)view.findViewById(R.id.count2);
        count3=(TextView)view.findViewById(R.id.count3);
        count4=(TextView)view.findViewById(R.id.count4);
        count5=(TextView)view.findViewById(R.id.count5);
        count6=(TextView)view.findViewById(R.id.count6);
        count7=(TextView)view.findViewById(R.id.count7);
        count8=(TextView)view.findViewById(R.id.count8);
        changeLang();
        from=new Date();
        to=new Date();
        dayFrom.setText(from.getDate()+"");
        dayTo.setText(from.getDate()+"");
        yearTo.setText((from.getYear()+1900)+"");
        yearFrom.setText((from.getYear()+1900)+"");
        monthFrom=from.getMonth();
        monthTo=monthFrom;
        ArrayAdapter adapter = ArrayAdapter.createFromResource(context, R.array.month_arabic, R.layout.spinner_phone);
        adapter.setDropDownViewResource(R.layout.dropitem);
        spinnerFrom=(Spinner)view.findViewById(R.id.spinnerFrom);
        spinnerTo=(Spinner)view.findViewById(R.id.spinnerTo);
        spinnerFrom.setAdapter(adapter);
        spinnerTo.setAdapter(adapter);
        spinnerFrom.setSelection(monthFrom);
        spinnerTo.setSelection(monthTo);
        spinnerFrom.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                monthFrom=position;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
        spinnerTo.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                monthTo=position;
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
        Call();
        return view;
    }
    public void Call()
    {
        Button show_zaker=(Button)view.findViewById(R.id.show_zaker);
        show_zaker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Show();
            }
        });

    }
    public void Show()
    {
        int day1=Integer.valueOf(dayFrom.getText().toString());
        int day2=Integer.valueOf(dayTo.getText().toString());
        int year1=Integer.valueOf(yearFrom.getText().toString());
        int year2=Integer.valueOf(yearTo.getText().toString());
        if(day1<0||day1>31)day1=1;
        if(day2<0||day2>31)day2=1;
        from.setDate(day1);
        to.setDate(day2);
        from.setMonth(monthFrom);
        to.setMonth(monthTo);
        from.setYear(year1-1900);
        to.setYear(year2-1900);
        DBInterface dbInterface=new DBInterface(context);
        long d1=(from.getDate())+((from.getMonth()+1)*12)+(from.getYear()*365);
        long d2=(to.getDate())+((to.getMonth()+1)*12)+(to.getYear()*365);
        ArrayList<Long> arrayList=dbInterface.All_Azkar(d1,d2);
        long total=0l;

        for(int i=0;i<arrayList.size();i++)
        {
            total+=arrayList.get(i);
           // Log.v("test", arrayList.get(i) + "");
        }
        count1.setText(arrayList.get(0)+"");
        count2.setText(arrayList.get(1)+"");
        count3.setText(arrayList.get(2)+"");
        count4.setText(arrayList.get(3)+"");
        count5.setText(arrayList.get(4)+"");
        count6.setText(arrayList.get(5)+"");
        count7.setText(arrayList.get(6)+"");
        count8.setText(total+"");


    }

    public void changeLang()
    {
        TextView textView1=(TextView)view.findViewById(R.id.text1);
        TextView textView2=(TextView)view.findViewById(R.id.text2);
        TextView textView3=(TextView)view.findViewById(R.id.text3);
        TextView textView4=(TextView)view.findViewById(R.id.text4);
        TextView textView5=(TextView)view.findViewById(R.id.text5);
        TextView textView6=(TextView)view.findViewById(R.id.text6);
        TextView textView7=(TextView)view.findViewById(R.id.text7);
        TextView textView8=(TextView)view.findViewById(R.id.text8);
        TextView textView9=(TextView)view.findViewById(R.id.text9);
        TextView textView10=(TextView)view.findViewById(R.id.counter);
        String Lang="arabic";
        if(Lang.equals("arabic"))
        {
            textView1.setTypeface(custom_font_arabic);
            textView2.setTypeface(custom_font_arabic);
            textView3.setTypeface(custom_font_arabic);
            textView4.setTypeface(custom_font_arabic);
            textView4.setTypeface(custom_font_arabic);
            textView6.setTypeface(custom_font_arabic);
            textView7.setTypeface(custom_font_arabic);
            textView8.setTypeface(custom_font_arabic);
            textView9.setTypeface(custom_font_arabic);
            textView10.setTypeface(custom_font_arabic);
        }
        else
        {
            textView1.setText("report");
            textView2.setText("Allah is the greatest");
            textView3.setText("Thank god");
            textView4.setText("Glory of God");
            textView5.setText("No God unless Allah");
            textView6.setText("i ask for forgiveness from the mighty Allah");
            textView7.setText("There is no power unless by Allah");
            textView8.setText("Pray to God the Prophet Mohamed");
            textView9.setText("Total");
            textView10.setText("count");
        }
    }

    /*
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rosary_statistics);
        custom_font_arabic= Typeface.createFromAsset(getAssets(), "fonts/GESSTwoLight.otf");
        changeLang();
        dayFrom=(EditText)findViewById(R.id.dayFrom);
        dayTo=(EditText)findViewById(R.id.dayTo);
        yearFrom=(EditText)findViewById(R.id.yearFrom);
        yearTo=(EditText)findViewById(R.id.yearTo);
        from=new Date();
        to=new Date();
        dayFrom.setText(from.getDate()+"");
        dayTo.setText(from.getDate()+"");
        yearTo.setText((from.getYear()+1900)+"");
        yearFrom.setText((from.getYear()+1900)+"");
        monthFrom=from.getMonth();
        monthTo=monthFrom;
        ArrayAdapter adapter = ArrayAdapter.createFromResource(this, R.array.month_arabic, R.layout.spinner_phone);
        adapter.setDropDownViewResource(R.layout.dropitem);
        spinnerFrom=(Spinner)findViewById(R.id.spinnerFrom);
        spinnerTo=(Spinner)findViewById(R.id.spinnerTo);
        spinnerFrom.setAdapter(adapter);
        spinnerTo.setAdapter(adapter);
        spinnerFrom.setSelection(monthFrom);
        spinnerTo.setSelection(monthTo);
        spinnerFrom.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                monthFrom=position;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        spinnerTo.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                monthTo=position;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }*/
}
