package com.islami.app.elsomsection;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import org.joda.time.Chronology;
import org.joda.time.DateTime;
import org.joda.time.chrono.ISOChronology;
import org.joda.time.chrono.IslamicChronology;

import java.util.Calendar;

import com.islami.app.R;

/**
 * Created by FCI on 1/7/2016.
 */
public class RegisterAnalysis extends Fragment {

    TextView MoharamText;
    TextView ShaabanText;
    TextView ShwalText;
    TextView otherText;
    TextView ramadanText;
    SQLiteDatabase db;
    TextView HijriYear;
    Button ashora,arfa;
    int GeoYear;
    int GeoMonth;
    int GeoDay;
    String Year;
    Database mydb;
    int isashora=0;
    int isarfa=0;
    int Hijri;
    int count;
    boolean isold=false;
 Button analysisyearprev;
 Button analysisyearnext;
 Button plusmMoharam;
 Button minusMoharam;
 Button plusshaaban;
 Button minusshaaban;
 Button plusramadan;
 Button minusramadan;
 Button plusshwal;
 Button minusshwal;
 Button plusother;
 Button minusother;
    
String lang="A";
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.registeranalysis, container, false);
        String fontPath = "fonts/GESSTwoLight.otf";
        Typeface tf = Typeface.createFromAsset(getResources().getAssets(), fontPath);
        HijriYear=(TextView)view.findViewById(R.id.analysisyeartext);
        MoharamText=(TextView)view.findViewById(R.id.Moharamanalysistext);
        ShaabanText=(TextView)view.findViewById(R.id.shaabananalysistext);
        ShwalText=(TextView)view.findViewById(R.id.shwalanalysistext);
        otherText=(TextView)view.findViewById(R.id.otheranalysistext);
        ramadanText=(TextView)view.findViewById(R.id.ramadananalysistext);
        HijriYear.setTypeface(tf);
        MoharamText.setTypeface(tf);
        ShaabanText.setTypeface(tf);
        ShwalText.setTypeface(tf);
        otherText.setTypeface(tf);
        ramadanText.setTypeface(tf);



        ashora=(Button)view.findViewById(R.id.isashora);
        arfa=(Button)view.findViewById(R.id.isarfa);
        analysisyearnext=(Button)view.findViewById(R.id.analysisyearnext);
        analysisyearprev=(Button)view.findViewById(R.id.analysisyearprev);
        plusmMoharam=(Button)view.findViewById(R.id.plusmMoharam);
        minusMoharam=(Button)view.findViewById(R.id.minusMoharam);
        plusshaaban=(Button)view.findViewById(R.id.plusshaaban);
        minusshaaban=(Button)view.findViewById(R.id.minusshaaban);
        plusramadan=(Button)view.findViewById(R.id.plusramadan);
        minusramadan=(Button)view.findViewById(R.id.minusramadan);
        plusshwal=(Button)view.findViewById(R.id.plusshwal);
        minusshwal=(Button)view.findViewById(R.id.minusshwal);
        plusother=(Button)view.findViewById(R.id.plusother);
        minusother=(Button)view.findViewById(R.id.minusother);

        mydb=new Database(getContext());
        Calendar calendar = Calendar.getInstance();
        GeoYear = calendar.get(Calendar.YEAR);
        GeoMonth=calendar.get(Calendar.DAY_OF_MONTH);
        GeoDay=calendar.get(Calendar.MONTH)+1;
        Year=GeoYear+"/"+GeoMonth+"/"+GeoDay;
        Hijri= GetHijriYear(GeoYear);
        HijriYear.setText(Hijri+"");
        isashora=0;
        isarfa=0;
        mydb.getDatabyhijri(HijriYear.getText().toString());
        GetData(HijriYear.getText().toString());

        ashora.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isashora();
            }
        });
        arfa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isarfa();
            }
        });
        analysisyearnext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                analysisyearnext();
            }
        });
        analysisyearprev.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                analysisyearprev();
            }
        });
        plusramadan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                plusramadan();
            }
        });
        minusramadan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                minusramadan();
            }
        });
        plusother.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                plusother();
            }
        });
        minusother.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                minusother();
            }
        });
        plusmMoharam.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                plusmMoharam();
            }
        });
        minusMoharam.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                minusMoharam();
            }
        });
        plusshwal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                plusshwal();
            }
        });
        minusshwal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                minusshwal();
            }
        });
        plusshaaban.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                plusshaaban();
            }
        });
        minusshaaban.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                minusshaaban();
            }
        });
        return view;

    }

    public void analysisyearprev(){
    Hijri=Integer.parseInt(HijriYear.getText().toString());
    Hijri-=1;
    HijriYear.setText(Hijri+"");
    GetData(HijriYear.getText().toString());
}
public void analysisyearnext(){
    Hijri=Integer.parseInt(HijriYear.getText().toString());
    Hijri+=1;
    HijriYear.setText(Hijri+"");
    GetData(HijriYear.getText().toString());
}

  public void plusmMoharam(){
   count=Integer.parseInt(MoharamText.getText().toString());
   count+=1;
  MoharamText.setText(count+"");
      addorupdate();
 }

   public  void minusMoharam(){
       count=Integer.parseInt(MoharamText.getText().toString());
       count-=1;
       MoharamText.setText(count+"");
       addorupdate();

   }


  public  void isashora(){
      isashora=1;
      ashora.setBackground(getResources().getDrawable(R.drawable.circlebutton2));
      ashora.setClickable(false);
      addorupdate();

  }


    public void plusshaaban(){
        count=Integer.parseInt(ShaabanText.getText().toString());
        count+=1;
        ShaabanText.setText(count+"");
        addorupdate();

    }

    public  void minusshaaban(){
        count=Integer.parseInt(ShaabanText.getText().toString());
        count-=1;
        ShaabanText.setText(count+"");
        addorupdate();

    }

    public void plusshwal(){
        count=Integer.parseInt(ShwalText.getText().toString());
        count+=1;
        ShwalText.setText(count+"");
        addorupdate();

    }

    public  void minusshwal(){
        count=Integer.parseInt(ShwalText.getText().toString());
        count-=1;
        ShwalText.setText(count+"");
        addorupdate();
    }
    public void plusother(){
        count=Integer.parseInt(otherText.getText().toString());
        count+=1;
        otherText.setText(count+"");
        addorupdate();
    }

    public  void minusother(){
        count=Integer.parseInt(otherText.getText().toString());
        count-=1;
        otherText.setText(count+"");
        addorupdate();
    }

    public void plusramadan(){
        count=Integer.parseInt(ramadanText.getText().toString());
        count+=1;
        ramadanText.setText(count+"");
        addorupdate();
    }

    public  void minusramadan(){
        count=Integer.parseInt(ramadanText.getText().toString());
        count-=1;
        ramadanText.setText(count+"");
        addorupdate();

    }

    public void isarfa(){
        isarfa=1;
        arfa.setBackground(getResources().getDrawable(R.drawable.circlebutton2));
        arfa.setClickable(false);
        addorupdate();

    }
    public boolean isCursorEmpty(Cursor cursor){
        if(!cursor.moveToFirst() || cursor.getCount() == 0) return true;
        return false;
    }


   public void addorupdate(){

       if(isold){
//update
           int moh=Integer.parseInt(MoharamText.getText().toString());
           int sha=Integer.parseInt(ShaabanText.getText().toString());
           int ramadan=Integer.parseInt(ramadanText.getText().toString());
           int sw=Integer.parseInt(ShwalText.getText().toString());
           int oth=Integer.parseInt(otherText.getText().toString());
    mydb.updateContact(HijriYear.getText().toString()," ",moh,isashora,sha,ramadan,sw,isarfa,oth);


       }
       else{
//insert
           int moh=Integer.parseInt(MoharamText.getText().toString());
           int sha=Integer.parseInt(ShaabanText.getText().toString());
           int ramadan=Integer.parseInt(ramadanText.getText().toString());
           int sw=Integer.parseInt(ShwalText.getText().toString());
           int oth=Integer.parseInt(otherText.getText().toString());
           mydb.insertContact(HijriYear.getText().toString()," ",moh,isashora,sha,ramadan,sw,isarfa,oth);

       }

   }

public void GetData(String Hijri){
  Cursor c= mydb.getDatabyhijri(Hijri);
    c.moveToFirst();
   if(!isCursorEmpty(c)){
       isold=true;
     MoharamText.setText(c.getString(c.getColumnIndex("moharam")));
     ShaabanText.setText(c.getString(c.getColumnIndex("shaaban")));
     ramadanText.setText(c.getString(c.getColumnIndex("ramadan")));
     ShwalText.setText(c.getString(c.getColumnIndex("shwal")));
     otherText.setText(c.getString(c.getColumnIndex("other")));
     isarfa=c.getInt(c.getColumnIndex("arfa"));
     isashora=c.getInt(c.getColumnIndex("ashora"));
    if(isarfa>0) {
    arfa.setBackground(getResources().getDrawable(R.drawable.circlebutton2));
    arfa.setClickable(false);
    }
    else {
        arfa.setBackground(getResources().getDrawable(R.drawable.circlebutton));
        arfa.setClickable(true);
    }

    if(isashora>0){
        ashora.setBackground(getResources().getDrawable(R.drawable.circlebutton2));
        ashora.setClickable(false);
    }
    else{
        ashora.setBackground(getResources().getDrawable(R.drawable.circlebutton));
        ashora.setClickable(true);
    }
   }
   else {
       isold=false;
       MoharamText.setText("0");
       ShaabanText.setText("0");
       ramadanText.setText("0");
       ShwalText.setText("0");
       otherText.setText("0");
       ashora.setBackground(getResources().getDrawable(R.drawable.circlebutton));
       ashora.setClickable(true);
       isashora=0;
       arfa.setBackground(getResources().getDrawable(R.drawable.circlebutton));
       arfa.setClickable(true);
        isarfa=0;
   }
}
    public  int GetGeoMonth(int HijriYear,int HijriMonth , int HijriDay){
        DateTime t= DateTime.now();
        DateTime dtIslamic = t.withChronology(IslamicChronology.getInstance());
        Chronology iso = ISOChronology.getInstanceUTC();
        Chronology hijri = IslamicChronology.getInstanceUTC();
        DateTime dtHijri = new DateTime(HijriYear,HijriMonth,HijriDay,22,22,hijri);
        DateTime dtIso = new DateTime(dtHijri, iso);
        return  dtIso.getMonthOfYear();
    }

    public  int GetGeoDay(int HijriYear,int HijriMonth , int HijriDay){
        DateTime t= DateTime.now();
        Calendar calendar = Calendar.getInstance();
        DateTime dtIslamic = t.withChronology(IslamicChronology.getInstance());
        Chronology iso = ISOChronology.getInstanceUTC();
        Chronology hijri = IslamicChronology.getInstanceUTC();
        DateTime dtHijri = new DateTime(HijriYear,HijriMonth,HijriDay,22,22,hijri);
        DateTime dtIso = new DateTime(dtHijri, iso);
        return  dtIso.getDayOfMonth();
    }
    public  int GetGeoYear(int HijriYear,int HijriMonth , int HijriDay){
        DateTime t= DateTime.now();
        DateTime dtIslamic = t.withChronology(IslamicChronology.getInstance());
        Chronology iso = ISOChronology.getInstanceUTC();
        Chronology hijri = IslamicChronology.getInstanceUTC();
        DateTime dtHijri = new DateTime(HijriYear,HijriMonth,HijriDay,22,22,hijri);
        DateTime dtIso = new DateTime(dtHijri, iso);
        return  dtIso.getYear();
    }

    public int GetHijriYear(int GeoYear){
        DateTime dtISO = new DateTime(GeoYear, 1, 1, 12, 0, 0, 0);
        DateTime dtIslamic = dtISO.withChronology(IslamicChronology.getInstance());
        return  dtIslamic.getYear();
    }





}
