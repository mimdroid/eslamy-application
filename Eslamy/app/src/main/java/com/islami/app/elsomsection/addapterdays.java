package com.islami.app.elsomsection;

import android.content.Context;
import android.graphics.Typeface;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.TextView;

/**
 * Created by FCI on 1/12/2016.
 */
public class addapterdays extends BaseAdapter {

    private Context context;
    private String[] texts ;
    Typeface t;
    public addapterdays(Context context , String [] strings,Typeface tf) {
        this.context = context;
        texts=strings;
        t=tf;
    }

    public int getCount() {
        return 9;
    }

    public Object getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return 0;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        TextView tv;
        if (convertView == null) {
            tv = new TextView(context);
            tv.setLayoutParams(new GridView.LayoutParams(85, 85));
            tv.setTypeface(t);

        }
        else {
            tv = (TextView) convertView;
        }

        tv.setText(texts[position]);
        return tv;
    }
}
