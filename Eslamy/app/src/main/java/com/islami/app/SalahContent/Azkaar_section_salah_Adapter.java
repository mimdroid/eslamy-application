package com.islami.app.SalahContent;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import com.islami.app.R;

/**
 * Created by Ibrahim on 12/27/2015.
 */
public class Azkaar_section_salah_Adapter extends BaseAdapter {
    ArrayList<String> azkar_arabic=new ArrayList<>();
    ArrayList<String>azkar_english=new ArrayList<>();
    ArrayList<Integer>num_secions=new ArrayList<>();
    String Lang="arabic";
    Typeface custom_font_arabic;
    int index;
    Context context;
    private static LayoutInflater inflater=null;
    public Azkaar_section_salah_Adapter(Context c,ArrayList<String>a,ArrayList<String>a1)
    {
       context=c;
        azkar_arabic=a;
        azkar_english=a1;
        custom_font_arabic= Typeface.createFromAsset(context.getAssets(), "fonts/GESSTwoLight.otf");

//Lang=;
      inflater = ( LayoutInflater )context.
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }
    @Override
    public int getCount() {
        return azkar_arabic.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Holder holder=new Holder();
        View rowView;
        rowView = inflater.inflate(R.layout.azkaar_section_salah_item, null);

        holder.tv=(TextView) rowView.findViewById(R.id.zkaar_section_salah_item_textview);
        if(Lang.equals("arabic")) {
            holder.tv.setText(azkar_arabic.get(position));
            holder.tv.setTypeface(custom_font_arabic);
        }
        else
            holder.tv.setText(azkar_english.get(position));

        return rowView;
    }
    public class Holder
    {
        TextView tv;
    }
}
