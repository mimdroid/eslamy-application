package com.islami.app.ZakahContent;

import android.app.Activity;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.islami.app.R;


public class GoldZakah extends Activity {
    EditText editTextWeight;
    EditText editTextPrice;
    TextView result_view;
    String Lang="arabic";
    TextView textArabicWeight;
    TextView textEnglishWeight;
    TextView textArabicPrice;
    TextView textEnglishPrice;
    TextView resultArabic;
    TextView resultEnglish;
    TextView calview;
    Typeface custom_font_arabic;
    Typeface custom_font_english;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.gold_zakah);
        custom_font_arabic= Typeface.createFromAsset(getAssets(), "fonts/GESSTwoLight.otf");
        custom_font_english=Typeface.createFromAsset(getAssets(),  "fonts/font.TTF");

        textArabicWeight=(TextView)findViewById(R.id.weight_gold_text_arabic);
        textEnglishWeight=(TextView)findViewById(R.id.weight_gold_text_english);
        textArabicPrice=(TextView)findViewById(R.id.gold_price_arabic);
        textEnglishPrice=(TextView)findViewById(R.id.gold_price_english);
        resultArabic=(TextView)findViewById(R.id.gold_result_arabic);
        resultEnglish=(TextView)findViewById(R.id.gold_result_english);
        calview=(TextView)findViewById(R.id.gold_calculate);
        textArabicWeight.setTypeface(custom_font_arabic);
        textArabicPrice.setTypeface(custom_font_arabic);
        resultArabic.setTypeface(custom_font_arabic);
     //   Lang="english";
        if(Lang.equals("arabic"))
        {
            calview.setTypeface(custom_font_arabic);
        }
        if(Lang.equals("english"))
        {
            changeLang();
        }
    }

    public void changeLang()
    {
        textArabicWeight.setVisibility(View.GONE);
        textEnglishWeight.setVisibility(View.VISIBLE);
        textArabicPrice.setVisibility(View.GONE);
        textEnglishPrice.setVisibility(View.VISIBLE);
        resultArabic.setVisibility(View.GONE);
        resultEnglish.setVisibility(View.VISIBLE);
        calview.setText("Calculate");

    }

    public void gold_calculate(View view)
    {
        float price=0,weight=0.0f;
        editTextPrice=(EditText)findViewById(R.id.edit_gold_price);
        editTextWeight=(EditText)findViewById(R.id.edit_weight_gold);
        result_view=(TextView)findViewById(R.id.text_result_gold);
        String sPrice=editTextPrice.getText().toString();
        String sWeight=editTextWeight.getText().toString();
        if(Lang.equals("arabic"))
        {
            result_view.setTypeface(custom_font_arabic);
        }
        if(!sPrice.isEmpty()&&!sWeight.isEmpty())
        {
            price=Float.valueOf(sPrice);
            weight=Float.valueOf(sWeight);
        }
        if(weight<85.0)
        {
            if(Lang.equals("arabic"))
            {
                Toast.makeText(this,"لا يوجد زكاة لأقل من 85 غرام ذهب",Toast.LENGTH_LONG).show();
            }
            else
            {
                Toast.makeText(this,"there is no alms for less than 85 gram",Toast.LENGTH_LONG).show();
            }
        }
        else
        {
            float r = (price * weight) / 40;
            result_view.setText(r + "");
        }
    }
}
