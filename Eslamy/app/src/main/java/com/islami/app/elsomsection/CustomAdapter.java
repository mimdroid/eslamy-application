package com.islami.app.elsomsection;

import android.content.Context;
import android.graphics.Typeface;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

import org.joda.time.Chronology;
import org.joda.time.DateTime;
import org.joda.time.chrono.ISOChronology;
import org.joda.time.chrono.IslamicChronology;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Calendar;

import com.islami.app.R;

public class CustomAdapter extends BaseAdapter {
    String [] title;
    String  year;
    String [] GeoMonth;
    String [] HijriMonth;
    String [] content;
    String [] text;
    Context context;
    final String[] MonthNamesArabic = {"يناير", "فبراير ", "مارس", "ابريل", "مايو", "يونيو", "يوليو", "اغسطس", "سبتمبر", "أكتوبر", "نوفمبر", "ديسمبر"};
    final String[] MonthNamesEnglish = {"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"};
    final String[] HijriMonthArabic = {"محرم", "ذى.الحجه", "شوال", "محرم", "شعبان"};
    final String[] HijriMonthEnglish = {"Moharam", "Ze El-haga", "Shwal", "Moharam", "Shaaban"};
    final int[] HijriMonthNum = {1, 12, 10, 1, 8};
    final int[] HijriDayNum = {1, 10, 9};
    int GeoYear = 0, GeoDay = 0, GeoMonth1 = 0, HijriYear = 0;
    ArrayList<Pair<Integer,Integer>> month;
    String fontPath = "fonts/GESSTwoLight.otf";
    Typeface tf ;
    private static LayoutInflater inflater=null;

    public CustomAdapter( Context mainActivity, String[] prgmNameList, String pr, String[] texts, String[] hyear, String
            [] gyear) {
        title=prgmNameList;
        context=mainActivity;
        tf = Typeface.createFromAsset(context.getResources().getAssets(), fontPath);
        year=pr;
        GeoMonth=gyear;
        HijriMonth=hyear;
        this.text=texts;
        inflater = ( LayoutInflater )context.
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }
    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return title.length;
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    public class Holder
    {
        TextView title;
        TextView year;
        TextView GeoMonth;
        TextView HijriGeo;
        TextView Content;
        TextView text;
        Button ly;
        Button ry;
        Button lm;
        Button rm;

    }
    public void CurrentDate( TextView yeartextview , TextView hijritextview, TextView geotextview , int index , int index2){
        Calendar calendar = Calendar.getInstance();
        GeoYear = Integer.parseInt(yeartextview.getText().toString());
        GeoMonth1=calendar.get(Calendar.DAY_OF_MONTH);
        GeoDay=calendar.get(Calendar.MONTH)+1;
        yeartextview.setText(GeoYear+"");
        HijriYear = GetHijriYear(GeoYear);
        hijritextview.setText(HijriDayNum[index2] + " " + HijriMonthArabic[index] + " " + HijriYear);
        GeoDay=GetGeoDay(HijriYear, HijriMonthNum[index], HijriDayNum[index2]);
        GeoMonth1=GetGeoMonth(HijriYear,HijriMonthNum[index],HijriDayNum[index2]);
        geotextview.setText(GeoDay + " " + MonthNamesArabic[GeoMonth1 - 1] + " " + GeoYear);
    }

    public String GetData(InputStream inputStream){
        InputStream iS = null;
        String d="Not Found";
        try {
            iS=inputStream;
            BufferedReader reader = new BufferedReader(new InputStreamReader(iS));
            StringBuilder sb = new StringBuilder();
            String line = reader.readLine();
            sb.append(line);
            d=sb.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return d;
    }
    public  int GetGeoMonth(int HijriYear,int HijriMonth , int HijriDay){
        DateTime t= DateTime.now();
        DateTime dtIslamic = t.withChronology(IslamicChronology.getInstance());
        Chronology iso = ISOChronology.getInstanceUTC();
        Chronology hijri = IslamicChronology.getInstanceUTC();
        DateTime dtHijri = new DateTime(HijriYear,HijriMonth,HijriDay,Calendar.HOUR_OF_DAY
                ,Calendar.MINUTE,hijri);
        DateTime dtIso = new DateTime(dtHijri, iso);
        return  dtIso.getMonthOfYear();
    }

    public  int GetGeoDay(int HijriYear,int HijriMonth , int HijriDay){
        DateTime t= DateTime.now();
        Calendar calendar = Calendar.getInstance();
        DateTime dtIslamic = t.withChronology(IslamicChronology.getInstance());
        Chronology iso = ISOChronology.getInstanceUTC();
        Chronology hijri = IslamicChronology.getInstanceUTC();
        DateTime dtHijri = new DateTime(HijriYear,HijriMonth,HijriDay,Calendar.HOUR_OF_DAY
                ,Calendar.MINUTE,hijri);
        DateTime dtIso = new DateTime(dtHijri, iso);
        return  dtIso.getDayOfMonth();
    }
    public  int GetGeoYear(int HijriYear,int HijriMonth , int HijriDay){
        DateTime t= DateTime.now();
        DateTime dtIslamic = t.withChronology(IslamicChronology.getInstance());
        Chronology iso = ISOChronology.getInstanceUTC();
        Chronology hijri = IslamicChronology.getInstanceUTC();
        DateTime dtHijri = new DateTime(HijriYear,HijriMonth,HijriDay,Calendar.HOUR_OF_DAY
                ,Calendar.MINUTE,hijri);
        DateTime dtIso = new DateTime(dtHijri, iso);
        return  dtIso.getYear();
    }
    public int GetHijriYear(int GeoYear){
        DateTime dtISO = new DateTime(GeoYear, 1, 1, 12, 0, 0, 0);
        DateTime dtIslamic = dtISO.withChronology(IslamicChronology.getInstance());
        return  dtIslamic.getYear();
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        final Holder holder=new Holder();
        month=new ArrayList<>();
        month.add(new Pair<Integer, Integer>(1,10));
        month.add(new Pair<Integer, Integer>(12,9));
        month.add(new Pair<Integer, Integer>(10,1));
        month.add(new Pair<Integer, Integer>(1,1));
        month.add(new Pair<Integer, Integer>(8,1));

        View rowView;
        rowView = inflater.inflate(R.layout.listviewitem, null);
        holder.Content=(TextView)rowView.findViewById(R.id.Content);
        holder.text=(TextView)rowView.findViewById(R.id.t);
       if(position==2)holder.text.setText("الفترات المعينه");
       if(position==0) holder.text.setText("الايام  المعينه");
        holder.ly=(Button) rowView.findViewById(R.id.leftyear);
        holder.ry=(Button)rowView.findViewById(R.id.rightyear);
        holder.lm=(Button)rowView.findViewById(R.id.MonthLeft);
        holder.rm=(Button)rowView.findViewById(R.id.MonthRight);

        holder.title=(TextView) rowView.findViewById(R.id.title);
        holder.year=(TextView) rowView.findViewById(R.id.year);
        holder.GeoMonth=(TextView)rowView.findViewById(R.id.MonthTextGeo);
        holder.HijriGeo=(TextView)rowView.findViewById(R.id.MonthTextHijri);
        holder.Content.setText(text[position]);

        holder.title.setTypeface(tf);
        holder.year.setTypeface(tf);
        holder.GeoMonth.setTypeface(tf);
        holder.HijriGeo.setTypeface(tf);
        holder.Content.setTypeface(tf);

        if(position>4){
            holder.ly.setVisibility(View.GONE);
            holder.ry.setVisibility(View.GONE);
            holder.lm.setVisibility(View.GONE);
            holder.rm.setVisibility(View.GONE);
            holder.year.setVisibility(View.GONE);
            holder.GeoMonth.setVisibility(View.GONE);
            holder.HijriGeo.setVisibility(View.GONE);
            holder.title.setVisibility(View.GONE);
            holder.text.setText(title[position]);
        }
        else {
            holder.title.setText((position+1)+" "+"-"+" "+ title[position]);
            holder.year.setText(year);
            holder.GeoMonth.setText(GeoMonth[position]);
            holder.HijriGeo.setText(HijriMonth[position]);
        }


        holder.ly.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GeoYear=Integer.parseInt(holder.year.getText().toString());
                GeoYear-=1;
                holder.year.setText(GeoYear+"");
                HijriYear=GetHijriYear(GeoYear);
                String statictext= GetHijriDayfromText(holder.HijriGeo) + " " + GetHijriMonthfromText(holder.HijriGeo)+" "+HijriYear;
                holder.HijriGeo.setText(statictext);
                GeoDay=GetGeoDay(HijriYear, month.get(position).first, month.get(position).second);
                GeoMonth1=GetGeoMonth(HijriYear, month.get(position).first, month.get(position).second);
                holder.GeoMonth.setText(GeoDay +" "+MonthNamesArabic[GeoMonth1-1] + " "+GeoYear);


            }
        });

        holder.ry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GeoYear=Integer.parseInt(holder.year.getText().toString());
                GeoYear+=1;
                holder.year.setText(GeoYear+"");
                HijriYear=GetHijriYear(GeoYear);
                String statictext= GetHijriDayfromText(holder.HijriGeo) + " " + GetHijriMonthfromText(holder.HijriGeo)+" "+HijriYear;
                holder.HijriGeo.setText(statictext);
                GeoDay=GetGeoDay(HijriYear, month.get(position).first, month.get(position).second);
                GeoMonth1=GetGeoMonth(HijriYear, month.get(position).first, month.get(position).second);
                holder.GeoMonth.setText(GeoDay +" "+MonthNamesArabic[GeoMonth1-1] + " "+GeoYear);

            }
        });


        holder.lm.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {

                HijriYear=Integer.parseInt(GetHijriYearfromText(holder.HijriGeo))-1;
                holder.HijriGeo.setText(GetHijriDayfromText(holder.HijriGeo) + " " + GetHijriMonthfromText(holder.HijriGeo)+" "+HijriYear);
                GeoDay=GetGeoDay(HijriYear, month.get(position).first, month.get(position).second);
                GeoMonth1=GetGeoMonth(HijriYear,month.get(position).first, month.get(position).second);
                GeoYear=GetGeoYear(HijriYear,month.get(position).first, month.get(position).second);
                holder.GeoMonth.setText(GeoDay +" "+MonthNamesArabic[GeoMonth1-1] + " "+GeoYear);
                holder.year.setText(GeoYear + "");
            }
        });

        holder.rm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HijriYear=Integer.parseInt(GetHijriYearfromText(holder.HijriGeo))+1;
                holder.HijriGeo.setText(GetHijriDayfromText(holder.HijriGeo) + " " + GetHijriMonthfromText(holder.HijriGeo)+" "+HijriYear);
                GeoDay=GetGeoDay(HijriYear, month.get(position).first, month.get(position).second);
                GeoMonth1=GetGeoMonth(HijriYear, month.get(position).first, month.get(position).second);
                GeoYear=GetGeoYear(HijriYear, month.get(position).first, month.get(position).second);
                holder.GeoMonth.setText(GeoDay +" "+MonthNamesArabic[GeoMonth1-1] + " "+GeoYear);
                holder.year.setText(GeoYear+"");
            }
        });

        return rowView;
    }
    public String GetHijriDayfromText(TextView Text){
        return Text.getText().toString().split(" ")[0];

    }
    public String GetHijriMonthfromText(TextView Text){
        return Text.getText().toString().split(" ")[1];
    }

    public String GetHijriYearfromText(TextView Text){
        return Text.getText().toString().split(" ")[2];
    }

}
