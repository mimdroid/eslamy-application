package com.islami.app.SalahContent;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;

import Manage.Reader;
import com.islami.app.R;


public class SalahAzkarActivity extends Fragment {
    ListView lv;
    Context context;
    View view;
    public SalahAzkarActivity()
    {

    }
    @Override
    public void onCreate( Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.activity_salah_azkar, container, false);
        context = view.getContext();
        Reader reader=new Reader(context);
        //   Log.v("test", reader.readFromFile());
        ArrayList<String> title_arabic=new ArrayList<>();
        ArrayList<String>title_english=new ArrayList<>();
        ArrayList<String>azkar_arabic=new ArrayList<>();
        ArrayList<String>azkar_english=new ArrayList<>();
        ArrayList<Integer>num_secions=new ArrayList<>();
        reader.readAzakrSalah(reader.readFromFile("AzarSalah.txt"),title_arabic,title_english,azkar_arabic,azkar_english,num_secions);
        lv=(ListView)view.findViewById(R.id.activity_salah_azkar_listview);
        lv.setAdapter(new AzkarSalahAdapter(context, title_arabic,title_english));
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent=new Intent(context,Azkaar_section_salah.class);
                intent.putExtra("position",position);
                startActivity(intent);
            }
        });
        return view;
    }
    /*
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_salah_azkar);
        Reader reader=new Reader(this);
        //   Log.v("test", reader.readFromFile());
        ArrayList<String> title_arabic=new ArrayList<>();
        ArrayList<String>title_english=new ArrayList<>();
        ArrayList<String>azkar_arabic=new ArrayList<>();
        ArrayList<String>azkar_english=new ArrayList<>();
        ArrayList<Integer>num_secions=new ArrayList<>();
        reader.readAzakrSalah(reader.readFromFile("AzarSalah.txt"),title_arabic,title_english,azkar_arabic,azkar_english,num_secions);
        context=this;
        lv=(ListView) findViewById(R.id.activity_salah_azkar_listview);
        lv.setAdapter(new AzkarSalahAdapter(this, title_arabic,title_english));
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent=new Intent(context,Azkaar_section_salah.class);
                intent.putExtra("position",position);
                startActivity(intent);
            }
        });
    }
*/
}
