package com.islami.app.SalahContent;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationManager;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

import ConnectionHandler.Connector;
import Manage.GPS;
import Manage.PrayTime;

/**
 * Created by Ibrahim on 12/25/2015.
 */
public class AlarmService extends Service {
    private AlarmManager alarmMgr[]=new AlarmManager[10];
    private ArrayList<Integer> arrayList;
    ArrayList<String> prayerTimes;
    Intent intents[]=new Intent[10];
    Timer timer;
    TimerTask timerTask;
    final Handler handler=new Handler();
    double lat=0,lon=0;
    GPS gps;
    LocationManager locationManager;
    Location l;
    @Override
    public IBinder onBind(Intent intent) {
        Toast.makeText(this, "Service Startedhh", Toast.LENGTH_LONG).show();
        return null;
    }
    @Override
    public int onStartCommand(Intent i, int flags, int startId) {
        Log.v("seconds","in service");
        locationManager= (LocationManager)getSystemService(Context.LOCATION_SERVICE);
        gps = new GPS(locationManager);
        Connector connector=new Connector(this);
        if(connector.isOnline())
        {
            lat=gps.get_Latitude();
            lon=gps.get_Longitude();
        }
        startTimer();
        return START_STICKY;
    }
    public void startTimer()
    {
        timer=new Timer();
        initializeTimerTask();
        timer.schedule(timerTask, 5000, 20*60*60*1000);
    }
    public void initializeTimerTask() {
        timerTask = new TimerTask() {
            public void run() {

                handler.post(new Runnable() {

                    public void run() {
                        Cal_Times();
                        int b_fagr=arrayList.get(0)-10;
                        int b_dahr=arrayList.get(1)-10;
                        int b_asr=arrayList.get(2)-10;
                        int b_maghrb=arrayList.get(3)-10;
                        int b_isha=arrayList.get(4)-10;
                        arrayList.add(b_fagr);
                        arrayList.add(b_dahr);
                        arrayList.add(b_asr);
                        arrayList.add(b_maghrb);
                        arrayList.add(b_isha);
                        for(int i=0;i<10;i++)
                        {

                            intents[i] = new Intent(getApplicationContext(), AlarmReceiver.class);
                            //    intent.putExtra("list",arrayList);
                            alarmMgr[i] = (AlarmManager)getSystemService(Context.ALARM_SERVICE);
                            Log.v("iii",arrayList.get(i)+"");
                            intents[i].putExtra("index",arrayList.get(i));
                            PendingIntent alarmIntent = PendingIntent.getBroadcast(getApplicationContext(), i, intents[i], 0);
                            Calendar cal_alarm = Calendar.getInstance();
                            cal_alarm.set(Calendar.HOUR_OF_DAY,arrayList.get(i)/60);
                            cal_alarm.set(Calendar.MINUTE,arrayList.get(i)%60);
                            Log.v("testtt", cal_alarm.getTimeInMillis() + " "+i);
                            alarmMgr[i].set(AlarmManager.RTC_WAKEUP,
                                    cal_alarm.getTimeInMillis(), alarmIntent);
                        }
                    }
                });
            }
        };
    }
    public void stoptimertask(View v) {
        //stop the timer, if it's not already null
        if (timer != null) {
            timer.cancel();
            timer = null;
        }
    }
    public void ConvertToSeconds()
    {
        for(int i=0;i<prayerTimes.size();i++)
        {
            int Mseconds=0;
            String sss=prayerTimes.get(i).substring(0,2);
            if(prayerTimes.get(i).contains("pm")==true&&sss.equals("12")==false)
            {

                Mseconds+=(12*60);
            }
            int hour=0,minute=0;
            hour=((prayerTimes.get(i).charAt(0)-'0')*10)+(prayerTimes.get(i).charAt(1)-'0');
            minute=((prayerTimes.get(i).charAt(3)-'0')*10)+(prayerTimes.get(i).charAt(4)-'0');
            Mseconds+=((hour*60)+(minute));
            arrayList.add(Mseconds);
        }

    }
    public void Cal_Times()
    {


        double latitude = lat;
        double longitude = lon;
        double timezone = (Calendar.getInstance().getTimeZone()
                .getOffset(Calendar.getInstance().getTimeInMillis()))
                / (1000 * 60 * 60);
        PrayTime prayers = new PrayTime();

        prayers.setTimeFormat(prayers.Time12);
        prayers.setCalcMethod(prayers.Makkah);
        prayers.setAsrJuristic(prayers.Shafii);
        prayers.setAdjustHighLats(prayers.AngleBased);
        int[] offsets = { 0, 0, 0, 0, 0, 0, 0 }; // {Fajr,Sunrise,Dhuhr,Asr,Sunset,Maghrib,Isha}
        prayers.tune(offsets);

        Date now = new Date();
//        now.setDate(32);

        Log.v("Date1", now.toString());
        Calendar cal = Calendar.getInstance();
        cal.setTime(now);
        prayerTimes = prayers.getPrayerTimes(cal, latitude,
                longitude, timezone);
        ArrayList prayerNames = prayers.getTimeNames();
        arrayList=new ArrayList<Integer>();
        prayerTimes.remove(4);
        prayerTimes.remove(1);
        ConvertToSeconds();

        Log.v("seconds", cal.getTimeInMillis() + "");
        for(int i=0;i<5;i++)
        {
            Log.v("seconds", arrayList.get(i) + " " + prayerTimes.get(i));
        }
        for(int i=0;i<5;i++) {
            alarmMgr[i] = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        }

    }

}
