package com.islami.app.ZakahContent;

import android.app.Activity;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.islami.app.R;


public class CamelsZakah extends Activity {
    TextView camels_text_arabic;
    TextView camels_text_english;
    TextView caws_text_arabic;
    TextView caws_text_english;
    TextView cattle_text_arabic;
    TextView cattle_text_english;
    TextView cattle_calculate;
    TextView camels_result_arabic;
    TextView camels_result_english;
    String Lang="arabic";
    Typeface custom_font_arabic;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camels_zakah);
        custom_font_arabic= Typeface.createFromAsset(getAssets(), "fonts/GESSTwoLight.otf");
        camels_text_arabic=(TextView)findViewById(R.id.camels_text_arabic);
        camels_text_english=(TextView)findViewById(R.id.camels_text_english);
        caws_text_arabic=(TextView)findViewById(R.id.cows_text_arabic);
        caws_text_english=(TextView)findViewById(R.id.cows_text_english);
        cattle_text_arabic=(TextView)findViewById(R.id.cattle_text_arabic);
        cattle_text_english=(TextView)findViewById(R.id.cattle_text_english);
        cattle_calculate=(TextView)findViewById(R.id.cattle_calculate);
        camels_result_arabic=(TextView)findViewById(R.id.camels_result_arabic);
        camels_result_english=(TextView)findViewById(R.id.camels_result_english);
        camels_text_arabic.setTypeface(custom_font_arabic);
        caws_text_arabic.setTypeface(custom_font_arabic);
        cattle_text_arabic.setTypeface(custom_font_arabic);
        camels_result_arabic.setTypeface(custom_font_arabic);
        //change_Lang();
    }
    public void change_Lang()
    {
        camels_text_english.setVisibility(View.VISIBLE);
        camels_text_arabic.setVisibility(View.GONE);
        caws_text_arabic.setVisibility(View.GONE);
        caws_text_english.setVisibility(View.VISIBLE);
        caws_text_arabic.setVisibility(View.GONE);
        cattle_text_english.setVisibility(View.VISIBLE);
        cattle_calculate.setText("Calculate");
        camels_result_english.setVisibility(View.VISIBLE);
        camels_result_arabic.setVisibility(View.GONE);
        TextView r1,r2;
        r1=(TextView)findViewById(R.id.result_1);
        r2=(TextView)findViewById(R.id.result_2);
        r1.setVisibility(View.VISIBLE);
        r2.setVisibility(View.VISIBLE);

    }

public void cattle_calculate(View view)
{
    cal_camels();
    cal_caws();
    cal_cattle();
}
    public void cal_cattle()
    {
        EditText cattle=(EditText)findViewById(R.id.edit_cattle);
        TextView text_result_cattle=(TextView)findViewById(R.id.text_result_cattle);
        if(Lang.equals("arabic"))
        {
            text_result_cattle.setTypeface(custom_font_arabic);
        }
        float x=0;
        if(cattle.getText().toString().isEmpty()==false)
                x=Float.valueOf(cattle.getText().toString());
        String r="";
        if(Lang.equals("arabic")) {
            if (x >= 600) {
                r = (x / 100) + " شاه";
            } else if (x >= 500) {
                r = "خمس شياه";
            } else if (x >= 201) {
                r += "ثلاث شياه";
            } else if (x >= 121) {
                r += "شاتان";
            } else if (x >= 40) {
                r += "شاه واحدة (أنثى الغنم لا تقل عن سنة)";
            } else {
                r += "لا شئ";
            }
        }
        else {
            if (x >= 600) {
                r = (x / 100) + "Shah";
            } else if (x >= 500) {
                r = "Five shah";
            } else if (x >= 201) {
                r += "Three Shah";
            } else if (x >= 121) {
                r += "Two Shah";
            } else if (x >= 40) {
                r += "Shah (female sheep not less than one year)";
            } else {
                r += "no thing";
            }

        }
        text_result_cattle.setText(r);
    }
    public void cal_caws()
    {
        EditText cow=(EditText)findViewById(R.id.edit_cows);
        TextView text_result_cows=(TextView)findViewById(R.id.text_result_cows);
        if(Lang.equals("arabic"))
        {
            text_result_cows.setTypeface(custom_font_arabic);
        }
        int x=0;
        if(cow.getText().toString().isEmpty()==false)
            x=Integer.valueOf(cow.getText().toString());
        String r="";
        if(Lang.equals("arabic"))
        {
            if(x>=130)
            {
                r+=(x/30)+" تبيع  "+(x/40)+" مسنة";
            }
            else if(x>=120)
            {
                r="ثلاث مسنات أو أربعة أتبعة";

            }
            else if(x>=110)
            {
                r="مسنتان + تبيعان أو تبيعتان";
            }
            else if(x>=100)
            {
                r="مُسنة + تبيعان أو تبيعتان";

            }
            else if(x>=90)
            {
                r="ثلاث أتبعة";
            }
            else if(x>=80)
            {
                r="مسنتان";
            }
            else if(x>=70)
            {
                r="ُسنة + تبيع أو تبيعة";
            }
            else if(x>=60)
            {
                r="تبيعان أو تبيعتان";
            }
            else if(x>=40)
            {
                r="مسنة (أنثى البقر التي أتمت سنتين ودخلت في الثالثة)";
            }
            else if(x>=30)
            {
                r="تبيع (ما أتم من البقر سنة ودخل في الثانية، ذكراً كان أو أنثى)";
            }
            else
            {
                r="لاشيء فيها";
            }
        }
        else
        {
            if(x>=130)
            {
                r+=(x/30)+" Tbie"+(x/40)+" An elderly";
            }
            else if(x>=120)
            {
                r="Three an elderly or four Atbieh";

            }
            else if(x>=110)
            {
                r="Two An elderly+Tbien or Tbiatan";
            }
            else if(x>=100)
            {
                r="An elderly+Tbien or Tbiatan";

            }
            else if(x>=90)
            {
                r="three A tbieh";
            }
            else if(x>=80)
            {
                r="Msantan";
            }
            else if(x>=70)
            {
                r="An elderly+Tabiah or Tbia";
            }
            else if(x>=60)
            {
                r="Tbien or Tbiatan";
            }
            else if(x>=40)
            {
                r="An elderly (female cows that have completed two years and entered in the third)";
            }
            else if(x>=30)
            {
                r="Sale (what has been the year of the cow and income in the second, whether male or female)";
            }
            else
            {
                r="no thing ";
            }
        }
        text_result_cows.setText(r);

    }
    public void cal_camels()
    {
        EditText camel=(EditText)findViewById(R.id.edit_camels);
        TextView text_result_camels=(TextView)findViewById(R.id.text_result_camels);
        if(Lang.equals("arabic"))
        text_result_camels.setTypeface(custom_font_arabic);
        int x=0;
        if(camel.getText().toString().isEmpty()==false)
            x=Integer.valueOf(camel.getText().toString());
        String r="";
        if(Lang.equals("arabic"))
        {
            if(x>=210)
            {
                r=(x/40)+" لبون"+(x/50)+" حقة";
            }
            else if(x>=200)
            {
                r="أربع حقاق أو خمس بنات لبون";
            }
            else if(x>=190)
            {
                r="ثلاث حقاق + بنت لبون";
            }
            else if(x>=180)
            {
                r="بنتا لبون + حقتان";
            }
            else if(x>=170)
            {
                r="ثلاث بنات لبون + حقة";
            }
            else if(x>=160)
            {
                r="أربع بنات لبون";
            }
            else if(x>=150)
            {
                r="ثلاث حقاق";
            }
            else if(x>=140)
            {
                r="حقتان + بنتا لبون";
            }
            else if(x>=130)
            {
                r="حقة + بنتا لبون";
            }
            else if(x>=121)
            {
                r="ثلاث بنات لبون";
            }
            else if(x>=91)
            {
                r="حقتان";
            }
            else if(x>=76)
            {
                r="بنتا لبون";
            }
            else if(x>=61)
            {
                r="جذعة (أنثى الإبل التي أتمت أربع سنين ودخلت في الخامسة)";
            }
            else if(x>=46)
            {
                r="حِقه (أنثى الإبل التي أتمت ثلاث سنين ودخلت الرابعة، سميت حِقة لأنها استحقت أن يطرقها الفحل)";
            }
            else if(x>=36)
            {
                r="بنت لبون (أنثى الإبل التي أتمت سنتين ودخلت في الثالثة، سميت بذلك لأن أمها تكون وضعت غيرها في الغالب وصارت ذات لبن)";
            }
            else if(x>=25)
            {
                r="بنت مخاض (هي أنثى الإبل أتمت سنة واحدة ودخلت في الثانية، سميت بذلك لأن أمها لحقت بالمخاض وهي الحوامل الثانية)";
            }
            else if(x>=20)
            {
                r="4 (شياه)";
            }
            else if(x>=15)
            {
                r="3 (شياه)";
            }
            else if(x>=10)
            {
                r="شاتان";
            }
            else if(x>=5)
            {
                r="شاه";
            }
            else
            {
                r="لا شئ فيها";
            }
        }
        else
        {
            if(x>=210)
            {
                r=(x/40)+" Bonn"+(x/50)+" Haka";
            }
            else if(x>=200)
            {
                r="4 Hakaq or five  Bonn";
            }
            else if(x>=190)
            {
                r="Three Hakaq + one Bonn";
            }
            else if(x>=180)
            {
                r="Two Bonn + Haktan";
            }
            else if(x>=170)
            {
                r="Three blocks to Bonn + Haka";
            }
            else if(x>=160)
            {
                r="Four blocks to Bonn";
            }
            else if(x>=150)
            {
                r="three Haka";
            }
            else if(x>=140)
            {
                r="Haktan + Two Bonn";
            }
            else if(x>=130)
            {
                r="Haka + girl to Bonn";
            }
            else if(x>=121)
            {
                r="Three blocks to Bonn";
            }
            else if(x>=91)
            {
                r="Haktan";
            }
            else if(x>=76)
            {
                r="Penta to Bonn";
            }
            else if(x>=61)
            {
                r="Torso (female camel that has completed four years and entered in the fifth).";
            }
            else if(x>=46)
            {
                r="haka (female camel that has completed three years and entered the fourth, named because they truly deserved that Atrgaha stallion)";
            }
            else if(x>=36)
            {
                r="Girl of Bonn (female camel which completed two years and entered in the third, so named because her mother to be placed in the other and often become with milk)";
            }
            else if(x>=25)
            {
                r="Pangs girl (female camels are completed one year and came in second, so named because her mother suffered a second pregnant went into labor)";
            }
            else if(x>=20)
            {
                r="4 shah";
            }
            else if(x>=15)
            {
                r="three shah";
            }
            else if(x>=10)
            {
                r="2 shah";
            }
            else if(x>=5)
            {
                r="shah";
            }
            else
            {
                r=" there is no alms";
            }
        }
        text_result_camels.setText(r);

    }
}
