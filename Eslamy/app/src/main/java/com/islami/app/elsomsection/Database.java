package com.islami.app.elsomsection;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by FCI on 1/7/2016.
 */
public class Database extends SQLiteOpenHelper  {
    public static final String DATABASE_NAME = "Elsomdb.db";
    public static final String CONTACTS_TABLE_NAME = "Elsom";
    private HashMap hp;
    public Database(Context context)
    {
        super(context, DATABASE_NAME , null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(
                "create table Elsom " +
   "( hijridate text primary key , geodate text , moharam INTEGER  DEFAULT 0, ashora INTEGER  DEFAULT 0 , " +
   "shaaban INTEGER DEFAULT 0 ,ramadan INTEGER  DEFAULT 0 ,shwal INTEGER  DEFAULT 0,arfa INTEGER  DEFAULT 0 ,other INTEGER  DEFAULT 0 )"
        );
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // TODO Auto-generated method stub
        db.execSQL("DROP TABLE IF EXISTS Elsom");
        onCreate(db);
    }

    public boolean insertContact  (String hijriday, String geoday,int moharam,int ashora , int shaaban , int ramadan , int shwal, int arfa,int other)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("hijridate",hijriday);
        contentValues.put("geodate",geoday);
        contentValues.put("moharam",moharam);
        contentValues.put("ashora",ashora);
        contentValues.put("shaaban",shaaban);
        contentValues.put("ramadan",ramadan);
        contentValues.put("shwal",shwal);
        contentValues.put("arfa",arfa);
        contentValues.put("other",other);
        db.insert("Elsom", null, contentValues);
        return true;
    }

    public Cursor getDatabyhijri(String hijridate){

        SQLiteDatabase db = this.getReadableDatabase();
       return db.rawQuery( "select * from Elsom where hijridate="+hijridate+"", null );

    }
    public Cursor getDatabyGeo(String Geodate){
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res =  db.rawQuery( "select * from Elsom where geodate="+Geodate+"", null );
        return res;
    }

    public int numberOfRows(){
        SQLiteDatabase db = this.getReadableDatabase();
        int numRows = (int) DatabaseUtils.queryNumEntries(db, CONTACTS_TABLE_NAME);
        return numRows;
    }

    public boolean updateContact (String hijriday, String geoday,int moharam,int ashora , int shaaban , int ramadan , int shwal, int arfa,int other)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("geodate",geoday);
        contentValues.put("moharam",moharam);
        contentValues.put("ashora",ashora);
        contentValues.put("shaaban",shaaban);
        contentValues.put("ramadan",ramadan);
        contentValues.put("shwal",shwal);
        contentValues.put("arfa",arfa);
        contentValues.put("other",other);
        db.update("Elsom", contentValues, "hijridate = ? ", new String[] { hijriday } );
        return true;
    }

    public Integer deleteAnalysis (Integer id)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete("contacts",
                "id = ? ",
                new String[] { Integer.toString(id) });
    }

    public ArrayList<String> getAllAnalysis()
    {
        ArrayList<String> array_list = new ArrayList<String>();

        //hp = new HashMap();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res =  db.rawQuery( "select * from Elsom", null );
        res.moveToFirst();

        while(res.isAfterLast() == false){
            array_list.add(res.getString(res.getColumnIndex("hijridate")));
            res.moveToNext();
        }
        return array_list;
    }
}
