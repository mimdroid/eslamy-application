package com.islami.app.SalahContent;

import android.content.Context;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;

import ConnectionHandler.Connector;
import Manage.GPS;
import Manage.Mosque;
import com.islami.app.R;


public class MosqueAround extends Fragment {
    private GoogleMap mMap; // Might be null if Google Play services APK is not available.
    LocationManager locationManager;
    GPS gps;
    Location l;
    ArrayList<Mosque> arrayList = new ArrayList<>();
    Connector connector;
    Context context;
    View view;

    public MosqueAround() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.activity_mosque_around, container, false);
        context = getActivity();
        Log.e("inflated","tmam");
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        FragmentManager fm = getChildFragmentManager();
        Log.e("created","tmam");
        connector=new Connector(context);
        if (mMap == null) {
            // Try to obtain the map from the SupportMapFragment.
            mMap = ((SupportMapFragment) fm.findFragmentById(R.id.map1))
                    .getMap();
            // Check if we were successful in obtaining the map.
            if (mMap != null) {
                Log.e("map","!=null");
                if(connector.isOnline())
                {
                    locationManager= (LocationManager)context.getSystemService(Context.LOCATION_SERVICE);
                    gps=new GPS(locationManager);
                    l=gps.getLocation();
                    mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(l.getLatitude(), l.getLongitude()), 15.0f));
                    MosqueLoader loader=new MosqueLoader();
                    loader.start();

                }
            }


        }
    }

    private class MosqueLoader extends Thread{
        @Override
        public void run (){
            if(connector.isOnline()==true) {
                Log.v("MyLocation", l.getLatitude() + " " + l.getLongitude());
                arrayList = connector.getAllPlaces(l.getLatitude(),l.getLongitude());

                getActivity().runOnUiThread(new Runnable() {
                 @Override
                 public void run() {
                     setUpMap();
                 }
             });
            //    setUpMap();
                Log.v("MyLocation", "out");
            }

        }
    }
    private void setUpMap() {
        Log.v("location",arrayList.size()+"");
        Log.v("location",l.getLatitude()+" "+l.getLongitude());
        for(int i=0;i<arrayList.size();i++)
        {
            MarkerOptions markerOptions=new MarkerOptions();
            markerOptions.position(new LatLng(arrayList.get(i).lat, arrayList.get(i).lng));
            markerOptions.title(arrayList.get(i).name);
            mMap.addMarker(markerOptions);
        }
      //  mMap.addMarker(new MarkerOptions().position(new LatLng(0, 0)).title("Marker"));
    }
}
/*@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mosque_around);

        locationManager= (LocationManager) getApplicationContext().getSystemService(Context.LOCATION_SERVICE);
        gps=new GPS(locationManager);
        l=gps.getLocation();
        setUpMapIfNeeded();
        connector=new Connector(this);
        if(connector.isOnline()==true) {
          //  Log.v("MyLocation", l.getLatitude() + " " + l.getLongitude());
            arrayList = connector.getAllPlaces(30.044281,31.340002);
            //setUpMap();
            Log.v("MyLocation", "out");
        }
         //   setUpMapIfNeeded();
    }
    @Override
    protected void onResume() {
        super.onResume();
    }
*/
