package com.islami.app.Qur2anContent;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import Manage.Reader;
import com.islami.app.R;

/**
 * Created by Admin on 1/3/2016.
 */
public class Search extends Fragment {

    public Search(){

    }

    @Override
    public void onCreate( Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }



    Mysearcher m;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view= inflater.inflate(R.layout.q_search, container, false);

        final EditText editText=(EditText) view.findViewById(R.id.surasearchfor);
        Button B=(Button) view.findViewById(R.id.suraseach);

        B.setOnClickListener(new View.OnClickListener() {
                                 @Override
                                 public void onClick(View v) {
                                   String tosearch=editText.getText().toString();
                                  if(tosearch!=null&&tosearch.length()!=0){
                                      m=new Mysearcher(tosearch);
                                      m.start();
                                  }

                                 }
                             }
        );



        return view;
    }
    private  class Mysearcher extends Thread{
        Reader r=new Reader(getContext());
        String tosearch;
        Mysearcher(String S){
            tosearch=S;
        }
        ArrayList<suraStructure>suras=new ArrayList<>();
        public void run(){

           getActivity().runOnUiThread(new Runnable() {
               @Override
               public void run() {
                   Toast.makeText(getContext(),"please wait ...",Toast.LENGTH_LONG).show();
               }
           });
            for (int i=1;i<=114;i++){
                String []arr=r.readSura(i+".txt");
                suraStructure s=new suraStructure(arr,removepunct(arr),i-1);
                if (s.finder(tosearch))suras.add(s);


            }
            DataNeeded.suras=suras;
         if (suras.size()!=0) {
             Intent i = new Intent(getContext(), SearchResult.class);
             startActivity(i);
         }else getActivity().runOnUiThread(new Runnable() {
             @Override
             public void run() {
                 Toast.makeText(getContext(),"no result founded",Toast.LENGTH_LONG).show();
                 }
         });



            }





        }

        private String[] removepunct(String []input){

            Pattern p = Pattern.compile("[\\p{P}\\p{Mn}]");
            Matcher m;
        String []newarr=new String [input.length];
            for (int i=0;i<input.length;i++){
             m = p.matcher(input[i]);
             m.reset();
            //    Log.e("1",input[i]);
              newarr[i]= m.replaceAll("");
              //  Log.e("2",input[i]);
            }
return newarr;

        }

    }



