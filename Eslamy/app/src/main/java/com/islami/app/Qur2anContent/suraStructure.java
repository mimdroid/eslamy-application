package com.islami.app.Qur2anContent;

import android.util.Log;

import java.util.ArrayList;

/**
 * Created by Admin on 1/5/2016.
 */    public class suraStructure{
    String []sura;
    String [] original;
    int suraid;
    ArrayList<Integer> founded=new ArrayList<>();
    public suraStructure(String []original,String []s,int suraid){
        this.sura=s;
        this.original=original;
        this.suraid=suraid;
    }
    boolean finder(String s){
        for (int i=0;i<sura.length;i++){
            if (sura[i].contains(s)){
                founded.add(i);
                Log.e("ff",sura[i]);
            }
        }
        if (founded.size()==0)return false;
         return true;
    }

   String getsuraName(String Lang){
       if(Lang.equals("ar"))
       return DataNeeded.ArSuras[suraid];
       else return DataNeeded.EnSuras[suraid];

    }
    String getAya(int i){
        return original[i];
    }

    ArrayList<Integer> getFounded(){
        return founded;
    }



}


