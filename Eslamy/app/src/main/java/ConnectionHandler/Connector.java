package ConnectionHandler;

import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import Manage.Mosque;

/**
 * Created by Ibrahim on 11/11/2015.
 */
public class Connector {
    Context context;

    public Connector(Context c) {
        context = c;
    }

    public boolean isOnline() {
        ConnectivityManager cm =
                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }
    public ArrayList<RadioChannel> getenRadios(){
        String url="http://json.mp3quran.net/en/radios.json";
        Connection connection = new Connection();
        connection.execute(url);
        String s=null;
        ArrayList<RadioChannel>channels=new ArrayList<>();
        try {
            s=connection.get();
            JSONArray jsonArray=new JSONArray(s);
            for(int i=0;i<jsonArray.length();i++)
            {
                RadioChannel c=new RadioChannel();
                JSONObject jsonObject=jsonArray.getJSONObject(i);
                long l=jsonObject.getLong("radio_id");
                String s1=jsonObject.getString("radio_name");
                String s2=jsonObject.getString("radio_link");
                c.id=l;
                c.moqre2=s1;
                c.url=s2;
                channels.add(c);
                Log.e("r",l+"");
            }

        }catch (Exception e)
        {
//            Toast.makeText(context,"Check connection to internet",Toast.LENGTH_LONG).show();
            ((Activity)context).runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    Toast.makeText(context,"Check connection to internet",Toast.LENGTH_LONG).show();
                }
            });
        }
        return channels;
    
    }

    public ArrayList<Mos7afLanguage> getAllLanguages(){
        String url="http://www.mp3quran.net/api/get_json.php";
        Connection connection = new Connection();
        connection.execute(url);
        String s=null;
        ArrayList<Mos7afLanguage>channels=new ArrayList<>();
        try {
            s=connection.get();
JSONObject obj=new JSONObject(s);
            JSONArray jsonArray=obj.getJSONArray("language");
            for(int i=0;i<jsonArray.length();i++)
            {
                Mos7afLanguage c=new Mos7afLanguage();
                JSONObject jsonObject=jsonArray.getJSONObject(i);
                int id=jsonObject.getInt("id");
                String MURL=jsonObject.getString("json");
                String lang=jsonObject.getString("language");
                String suraname=jsonObject.getString("sura_name");
                c.id=id;
                c.Language=lang;
                c.URL=MURL;
                c.SurasURL=suraname;
                channels.add(c);
                Log.e("id",id+"");
            }

        }catch (Exception e)
        {
//            Toast.makeText(context,"Check connection to internet",Toast.LENGTH_LONG).show();
            ((Activity)context).runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    Toast.makeText(context,"Check connection to internet",Toast.LENGTH_LONG).show();
                }
            });

        }
        return channels;

    }

    public Map<Integer, String> getSurasNamebyLang(String SurasURL){
        String url=SurasURL;
        Connection connection = new Connection();
        connection.execute(url);
        String s=null;
        Map<Integer,String>SurasNames=new HashMap<>();
        ArrayList<String> arr=new ArrayList<>();
        try {
            s=connection.get();
            JSONObject jsonObje=new JSONObject(s);
            JSONArray jsonArray=jsonObje.getJSONArray("Suras_Name");
            for(int i=0;i<jsonArray.length();i++)
            {

                JSONObject jsonObject=jsonArray.getJSONObject(i);
                int id=Integer.valueOf(jsonObject.getString("id"));
                String suraname=jsonObject.getString("name");
                SurasNames.put(id,suraname);

                Log.e("suras",SurasNames.toString());
                arr.add(suraname);

            }
            Log.e("names",arr.toString());

        }catch (Exception e)
        {
//            Toast.makeText(context,"Check connection to internet",Toast.LENGTH_LONG).show();
            ((Activity)context).runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    Toast.makeText(context,"Check connection to internet",Toast.LENGTH_LONG).show();
                }
            });

        }
        return SurasNames;

    }

    public ArrayList<Qare2>getAllQura2byLangURL(String LangURl){
        String url=LangURl;
        Connection connection = new Connection();
        connection.execute(url);
        String s=null;
        ArrayList<Qare2>Qura2=new ArrayList<>();
        try {
            s=connection.get();
            JSONObject jsonObje=new JSONObject(s);
            JSONArray jsonArray=jsonObje.getJSONArray("reciters");
         /*"id":"49",
				"name":"Abdelbari Al-Toubayti",
				"Server":"http://server6.mp3quran.net/thubti",
				"rewaya":"Rewayat Hafs A'n Assem",
				"count":"114",
				"letter":"A",
				"suras":"1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60,61,62,63,64,65,66,67,68,69,70,71,72,73,74,75,76,77,78,79,80,81,82,83,84,85,86,87,88,89,90,91,92,93,94,95,96,97,98,99,100,101,102,103,104,105,106,107,108,109,110,111,112,113,114"
				*/
            for(int i=0;i<jsonArray.length();i++)
            {

                JSONObject jsonObject=jsonArray.getJSONObject(i);
                int id=Integer.valueOf(jsonObject.getString("id"));
                String name=jsonObject.getString("name");
                int count=Integer.valueOf(jsonObject.getString("count"));
                String suras=jsonObject.getString("suras");
                String rewaya=jsonObject.getString("rewaya");
                String Server=jsonObject.getString("Server");
                String letter=jsonObject.getString("letter");
                Qare2 q=new Qare2(id,name,count,suras,rewaya,Server,letter);
                Qura2.add(q);
            }

        }catch (Exception e)
        {
//            Toast.makeText(context,"Check connection to internet",Toast.LENGTH_LONG).show();
            ((Activity)context).runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    Toast.makeText(context,"Check connection to internet",Toast.LENGTH_LONG).show();
                }
            });

        }
        return Qura2;

    }
    public ArrayList<Mosque>getAllPlaces(double x, double y)
    {
     //   x=30.044281;
     //   y=31.340002;
        ArrayList<Mosque>Mosques=new ArrayList<>();
        Connection connection=new Connection();
        String url="https://maps.googleapis.com/maps/api/place/nearbysearch/json?location="+String.valueOf(x)+","+String.valueOf(y)+"&radius=500&types=mosque&key=AIzaSyAweyoJuW5BxS1_RIWn4Jf-5byACeqerkE";
        connection.execute(url);
        String response;
        try {
            response=connection.get();
            JSONObject jsonObject=new JSONObject(response);
            String s=jsonObject.getString("results");
            String status=jsonObject.getString("status");
            if(status.equals("OK")) {
                JSONArray jsonArray = new JSONArray(s);
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                    String geometry = jsonObject1.getString("geometry");
                    JSONObject geometryJson = new JSONObject(geometry);
                    JSONObject location = geometryJson.getJSONObject("location");
                    double lat = location.getDouble("lat");
                    double lng = location.getDouble("lng");
                    Mosque mosque = new Mosque();
                    mosque.lat = lat;
                    mosque.lng = lng;
                    mosque.name = jsonObject1.getString("name");
                    mosque.vicinity = jsonObject1.getString("vicinity");
                    Mosques.add(mosque);
                    Log.v("geometry", geometry);
                }
            }
            return Mosques;
            //   Log.v("response",s);
        } catch (Exception e) {
            e.printStackTrace();
            Log.v("response",e.getMessage());
        }
        return null;
    }
}