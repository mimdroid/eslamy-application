package ConnectionHandler;

/**
 * Created by Admin on 12/27/2015.
 */
public class Qare2 {
   public  int id;
    public String name;
    public  int count;
    public  int [] suras;
    public String rewaya;
    public String Server;
    public String letter;

    public Qare2(int id, String name, int count, String suras, String rewaya, String server,String letter) {
        this.id = id;
        this.name = name;
        this.count = count;
        this.rewaya = rewaya;
        this.letter=letter;
        Server = server;
        String arr[]=suras.split(",");
        this.suras=new  int [arr.length];
        for (int i=0;i<arr.length;i++){
            this.suras[i]=Integer.valueOf(arr[i]);
        }

    }
    public boolean ContainSura(int sid){
        int start=0,end=suras.length-1;
        int ind;
        while (start<=end){
            ind=(start+end)/2;
            if (sid==suras[ind])return true;
            else if(sid<suras[ind])end=ind-1;
            else if (sid>suras[ind])start=ind+1;
        }
        return false;

    }
}
