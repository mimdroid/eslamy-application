package ConnectionHandler;

import android.os.AsyncTask;
import android.util.Log;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Created by Ibrahim on 11/11/2015.
 */
public class Connection extends AsyncTask<String,Void,String> {
    @Override
    protected String doInBackground(String... params) {
        String response="";
        DefaultHttpClient defaultHttpClient=new DefaultHttpClient();
        String url=params[0];
        HttpGet httpGet=new HttpGet(url);
        try {
            HttpResponse HttpRes = defaultHttpClient.execute(httpGet);
            InputStream content = HttpRes.getEntity().getContent();

            BufferedReader buffer = new BufferedReader(new InputStreamReader(content));
            String s = "";
            while ((s = buffer.readLine()) != null) {
                response += s;
            }
            Log.v("testConnection", response);
            content.close();
        }
        catch (Exception e)
        {
            e.printStackTrace();
            Log.v("testConnection_error",e.toString());
        }
        return response;
    }
}
