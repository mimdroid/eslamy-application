package Manage;

import android.content.Context;
import android.content.res.AssetManager;
import android.util.Log;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

/**
 * Created by Ibrahim on 12/27/2015.
 */
public class Reader {
    Context context;
    public Reader(Context c)
    {
        context=c;
    }
    public String readFromFile(String name)
    {
        AssetManager am = context.getAssets();
        InputStream is=null;
        String ret = "";
        try {
            is = am.open(name);

            Log.v("test", "I'm Here");
            if ( is != null ) {
                InputStreamReader inputStreamReader = new InputStreamReader(is);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                String receiveString = "";
                StringBuilder stringBuilder = new StringBuilder();

                while ((receiveString = bufferedReader.readLine()) != null) {

                    //Log.v("testtt",receiveString);
                    stringBuilder.append(receiveString+"\n");
                }
                ret = stringBuilder.toString();
            }
            is.close();
            Log.v("test", ret);
            return ret;


        } catch (Exception e) {
            e.printStackTrace();
            Log.v("testerror", e.getMessage());
            return "";
        }

    }
    public void readAzakrSalah(String file,ArrayList<String>title_arabic,ArrayList<String>title_english,
                               ArrayList<String>azkar_arabic,ArrayList<String>azkar_english,ArrayList<Integer>num_secions)

    {
      //  int number_of_row=
        String arr_rows[]=file.split("\n");
         // int number_of_rows=Integer.valueOf(arr_rows[0]);
        for(int i=1;i<arr_rows.length;)
        {
            String title=arr_rows[i++];
            String titles[]=title.split("#");
            title_arabic.add(titles[0]);
            title_english.add(titles[1]);
            Log.v("title",title);
            int number_sections=Integer.valueOf(arr_rows[i++]);
            int stop=number_sections+i;
            Log.v("number_sections",number_sections+"");
            num_secions.add(number_sections);
            for(;i<stop;i++)
            {

                String section=arr_rows[i];
                String Sections[]=section.split("#");
                azkar_arabic.add(Sections[0]);
                azkar_english.add(Sections[1]);
                Log.v("section",section);
            }

        }


    }
    public String[] readSura(String name)
    {

        Log.e("name",name);
        String result="";
        result=readFromFile(name);
        Log.v("name",result);
        String arr[]=result.split("\n");

        Log.v("size",arr.length+" ");
        return arr;
    }
    public String readBlessingsArabic()
    {
        String result=readFromFile("Blessings bar arabic.txt");
        String arr[]=result.split("\n");
        String temp="";
        for(int i=0;i<arr.length;i++)
        {
            temp+=arr[i];
        }
        Log.v("temp",temp);
        return temp;
    }
    public String readBlessingEnglish()
    {
        String result=readFromFile("Blessings bar english.txt");
        String arr[]=result.split("\n");
        String temp="";
        for(int i=0;i<arr.length;i++)
        {
            temp+=arr[i]+"\t";
        }
        Log.v("temp",temp);
        return temp;
    }
}
