package Manage;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

import com.islami.app.SalahContent.AlarmService;

/**
 * Created by Ibrahim on 12/25/2015.
 */
public class BootReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        Log.v("broadcast","start");
        Toast.makeText(context,"on boot completed",Toast.LENGTH_LONG).show();
        Intent service1 = new Intent(context, AlarmService.class);
        context.startService(service1);
    }
}
