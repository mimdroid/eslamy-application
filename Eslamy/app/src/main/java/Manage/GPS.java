package Manage;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;
import android.provider.Settings;

import com.google.android.gms.maps.model.LatLng;

//import com.google.android.gms.maps.model.LatLng;

/**
 * Created by Ibrahim on 11/6/2015.
 */
public class GPS extends Service implements LocationListener {

    private LocationManager locationManager;
    private Location location;
    double latitude; // latitude
    double longitude; // longitude

    // The minimum distance to change Updates in meters
    private static final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 10; // 10 meters

    // The minimum time between updates in milliseconds
    private static final long MIN_TIME_BW_UPDATES = 1000 * 60 * 1; // 1 minute
    // final private Context context;
    public GPS(LocationManager c)
    {
        locationManager =c ;
        location=getLocation();
        // context=c;
    }
    public Location getLocation() {
        try {

            boolean isGPSEnabled=this.Check_GPS_Open();
            boolean isNetworkEnabled=this.Check_Network_Open();

            // getting network status

            boolean canGetLocation;

            if (!isGPSEnabled && !isNetworkEnabled) {
                // no network provider is enabled

            } else {
                canGetLocation =this.canGetLocation() ;
                if (isNetworkEnabled) {
                    locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER,MIN_TIME_BW_UPDATES,MIN_DISTANCE_CHANGE_FOR_UPDATES, this);
                    //  Log.d("Network", "Network");
                    if (locationManager != null) {
                        location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                        if (location != null) {
                            latitude = location.getLatitude();
                            longitude = location.getLongitude();
                            System.out.println("network");
                        }
                    }
                }
                // if GPS Enabled get lat/long using GPS Services
                if (isGPSEnabled) {
                    if (location == null) {
                        locationManager.requestLocationUpdates(
                                LocationManager.GPS_PROVIDER,
                                MIN_TIME_BW_UPDATES,
                                MIN_DISTANCE_CHANGE_FOR_UPDATES, this);
                        //       Log.d("GPS Enabled", "GPS Enabled");
                        if (locationManager != null) {
                            location = locationManager
                                    .getLastKnownLocation(LocationManager.GPS_PROVIDER);
                            if (location != null) {
                                latitude = location.getLatitude();
                                longitude = location.getLongitude();
                                System.out.println("GPS");
                            }
                        }
                    }
                }
            }

        } catch (Exception e) {
        }
        return location;
    }
    public boolean canGetLocation()
    {
        boolean GPS=this.Check_GPS_Open();
        boolean Network=this.Check_Network_Open();
        if(GPS==true||Network==true)return true;
        return false;
    }
    public boolean Check_GPS_Open()
    {

        // locationManager=(LocationManager)context.getSystemService(Context.LOCATION_SERVICE);
        boolean isGPSEnabled = false;
        isGPSEnabled =locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        return isGPSEnabled;
    }
    public boolean Check_Network_Open()
    {
        boolean isNetworkEnabled = false;
        isNetworkEnabled=locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        return isNetworkEnabled;
    }
    public double get_Latitude()
    {
        if (location != null) {
            latitude = location.getLatitude();
        }
        // return latitude
        return latitude;
    }
    public double get_Longitude()
    {
        if (location != null) {
            longitude = location.getLongitude();
        }
        // return latitude
        return longitude;
    }
    public double GetDistance(LatLng a,LatLng b)
    {
        Location locationA=new Location("A");
        Location locationB=new Location("B");
        locationA.setLatitude(a.latitude);
        locationA.setLongitude(a.longitude);
        locationB.setLatitude(b.latitude);
        locationB.setLongitude(b.longitude);
        return locationA.distanceTo(locationB)/1000;
    }
    public float getAngle(Location loc)
    {
        return location.bearingTo(loc);

    }
    public  void OpenGPS_Wifi(int x,Context activity)
    {

       // final AlertDialog.Builder builder =  new AlertDialog.Builder(activity);
        final String action = Settings.ACTION_LOCATION_SOURCE_SETTINGS;
        final String a=Settings.ACTION_WIRELESS_SETTINGS;
        if(x==1)
        activity.startActivity(new Intent(action));
        else
            activity.startActivity(new Intent(a));
       /* final String message = "Do you want open GPS setting?";

        builder.setMessage(message)
                .setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            publicimage void onClick(DialogInterface d, int id) {
                                activity.startActivity(new Intent(action));
                                d.dismiss();
                            }
                        })
                .setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            publicimage void onClick(DialogInterface d, int id) {
                                d.cancel();
                            }
                        });
        builder.create().show();*/
    }
    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

}
