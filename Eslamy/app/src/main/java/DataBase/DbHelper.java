package DataBase;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Ibrahim on 1/4/2016.
 */
public class DbHelper extends SQLiteOpenHelper {
    private static String DataBaseName="Eslamyapp";

    private String zakerTable="CREATE TABLE IF NOT EXISTS azkar ( id INTEGER PRIMARY KEY, value INTEGER, num INTEGER , date LONG ); ";
    /*******************************************************************************/
    private String QuranReadTable="CREATE TABLE IF NOT EXISTS QuranRead ( id INTEGER PRIMARY KEY,min INTEGER,Num_of_Ayas INTEGER, date LONG ); ";
    private String QuranListenTable="CREATE TABLE IF NOT EXISTS QuranListen ( id INTEGER PRIMARY KEY," +
            " Num_of_mins INTEGER, date LONG ); ";
    private String QuranHefzTable="CREATE TABLE IF NOT EXISTS" +
            " QuranHefz ( id INTEGER PRIMARY KEY, Sura_id INTEGER," +
            "Aya_id INTEGER , date LONG ); ";
/**********************************************************/
    private String SalahFard="CREATE TABLE IF NOT EXISTS SalahFard (id INTEGER PRIMARY KEY, fagr INTEGER, dahr INTEGER,"+
            " asr INTEGER, maghrb INTEGER, isha INTEGER, date LONG);";
    private String SalahSann="CREATE TABLE IF NOT EXISTS SalahSann (id INTEGER PRIMARY KEY , fagr INTEGER, dahr INTEGER, "+
            "maghrb INTEGER, isha INTEGER, date LONG);";
    private String SalahNawafl="CREATE TABLE IF NOT EXISTS SalahNawafel (id INTEGER PRIMARY KEY , dahha INTEGER, watr INTEGER, date LONG );";
    private String ZakahTable="CREATE TABLE IF NOT EXISTS Zakah (id INTEGER PRIMARY KEY , value DOUBLE, type INTEGER , year INTEGER); ";
    private String Soom="CREATE TABLE IF NOT EXISTS Elsom " +
            "( hijridate text primary key , geodate text , moharam INTEGER  DEFAULT 0, ashora INTEGER  DEFAULT 0 , " +
            "shaaban INTEGER DEFAULT 0 ,ramadan INTEGER  DEFAULT 0 ,shwal INTEGER  DEFAULT 0,arfa INTEGER  DEFAULT 0 ,other INTEGER  DEFAULT 0 )";


    public DbHelper(Context context) {
        super(context, DataBaseName, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(zakerTable);
        db.execSQL(SalahFard);
        db.execSQL(SalahSann);
        db.execSQL(SalahNawafl);
        db.execSQL(ZakahTable);
        db.execSQL(Soom);
        db.execSQL(QuranHefzTable);
        db.execSQL(QuranListenTable);
        db.execSQL(QuranReadTable);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
