package DataBase;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

/**
 * Created by Ibrahim on 1/4/2016.
 */
public class DBInterface {
    private DbHelper Helper;
    private SQLiteDatabase mydatabase;
    private HashMap hp;
    public DBInterface(Context con){
        Helper=new DbHelper(con);
        mydatabase=Helper.getWritableDatabase();
    }

    public HashSet<Integer>getAyasofSura(int suraid){

       if ( getAllHefz().containsKey(suraid)==false)return new HashSet<Integer>();
        return getAllHefz().get(suraid);
    }
    public boolean insertContact  (String hijriday, String geoday,int moharam,int ashora , int shaaban , int ramadan , int shwal, int arfa,int other)
    {
        SQLiteDatabase db = mydatabase;
        ContentValues contentValues = new ContentValues();
        contentValues.put("hijridate",hijriday);
        contentValues.put("geodate",geoday);
        contentValues.put("moharam",moharam);
        contentValues.put("ashora",ashora);
        contentValues.put("shaaban",shaaban);
        contentValues.put("ramadan",ramadan);
        contentValues.put("shwal",shwal);
        contentValues.put("arfa",arfa);
        contentValues.put("other",other);
        db.insert("Elsom", null, contentValues);
        return true;
    }
    public Cursor getDatabyhijri(String hijridate){

        SQLiteDatabase db = mydatabase;
        return db.rawQuery( "select * from Elsom where hijridate="+hijridate+"", null );

    }
    public boolean updateContact (String hijriday, String geoday,int moharam,int ashora , int shaaban , int ramadan , int shwal, int arfa,int other)
    {
        SQLiteDatabase db = mydatabase;
        ContentValues contentValues = new ContentValues();
        contentValues.put("geodate",geoday);
        contentValues.put("moharam",moharam);
        contentValues.put("ashora",ashora);
        contentValues.put("shaaban",shaaban);
        contentValues.put("ramadan",ramadan);
        contentValues.put("shwal",shwal);
        contentValues.put("arfa",arfa);
        contentValues.put("other",other);
        db.update("Elsom", contentValues, "hijridate = ? ", new String[] { hijriday } );
        return true;
    }

    public long insertQuranListen(int Num_of_mins,Long date)
    {   Log.e("inserting ", "Listen "+Num_of_mins+" "+date);
        ContentValues contentValues=new ContentValues();
        contentValues.put("Num_of_mins",Num_of_mins);
        contentValues.put("date",date);
        return mydatabase.insert("QuranListen",null,contentValues);
    }
    public long insertQuranRead(int Num_of_Ayas,int mins,Long date)
    {   ContentValues contentValues=new ContentValues();
        contentValues.put("Num_of_Ayas",Num_of_Ayas);
        contentValues.put("min",mins);
        contentValues.put("date",date);
        return mydatabase.insert("QuranRead",null,contentValues);
    }

    public long insertQuranHefz(int Sura_id,int Aya_id,Long date)
    {

        ContentValues contentValues=new ContentValues();
        contentValues.put("Sura_id",Sura_id);
        contentValues.put("Aya_id",Aya_id);
        contentValues.put("date",date);

        return mydatabase.insert("QuranHefz",null,contentValues);
    }

    public int getTotalListen(long d1,long d2){

        String q="Select Num_of_mins FROM QuranListen WHERE date >= "+d1+" AND date <= "+d2+ " ;";
        Cursor c=mydatabase.rawQuery(q,null);
        int total=0;
        Log.e("listen entered",c.getCount()+"");
        while (c.moveToNext()){
            Log.e("listen",total+"");
            total+=c.getInt(0);
        }

return total;
    }

    public int getTotalHefz(long d1,long d2){
Log.e("HEFZ ENTERED","");
       Map<Integer,HashSet<Integer>>all= getAllHefz(d1,d2);
        Log.e("HEFZ ENTERED",all.size()+"");

        int total=0;
        for (int i=1;i<=114;i++){
          // HashSet<Integer>all=getAyasofSura(i);
            if(all.containsKey(i)==false)continue;
              total+=all.get(i).size();
            Log.e("Hefz",total+"");

        }
        return total;

    }

    public int getTotalReadAyas(long d1,long d2){

        String q="Select Num_of_Ayas FROM QuranRead WHERE date >= "+d1+" AND date <= "+d2+ " ;";
        Cursor c=mydatabase.rawQuery(q,null);
        int total=0;
        while (c.moveToNext()){
            total+=c.getInt(0);
            Log.e("read",total+"");
        }

        return total;
    }
    public void unhefz(int Suraid,int Ayaid){

        String q="Delete  from QuranHefz where Sura_id = "+Suraid+" and Aya_id = "+Ayaid+" ;";
        mydatabase.execSQL(q);
}
    private void updateHefzTable(Map<Integer,HashSet<Integer> >AllHefz){
        mydatabase.delete("QuranHefz", null, null);
        mydatabase.execSQL("vacuum");
        String q="INSERT into QuranHefz (Sura_id , Aya_id , date) VALUES ( ";
        
        for (int i=1;i<=114;i++){

            if (AllHefz.containsKey(i)==false)continue;
           for (Integer ii:AllHefz.get(i))
            mydatabase.rawQuery(q+i+" , "+ii+" );",null);

        }

    }
    public Map<Integer,HashSet<Integer> > getAllHefz(long datefrom,long dateto){
        Map<Integer,HashSet<Integer> > hefz=new HashMap< Integer,HashSet<Integer>>();
        String stat="select * FROM QuranHefz ;";
        Cursor cursor=mydatabase.rawQuery(stat,null);

        while (cursor.moveToNext()){

            int sura=cursor.getInt(1);
            int aya=cursor.getInt(2);
            long d=cursor.getLong(3);
            Log.e("hnaaaa",datefrom+" " +d+" "+dateto);
            if (d>=datefrom&&d<=dateto)
            if (hefz.containsKey(sura)){
                hefz.get(sura).add(aya);
            }else {
                hefz.put(sura,new HashSet<Integer>());
                hefz.get(sura).add(aya);
            }
        }

     return hefz;
    }
    private Map<Integer,HashSet<Integer> > getAllHefz(){
        Map<Integer,HashSet<Integer> > hefz=new HashMap< Integer,HashSet<Integer>>();

        String stat="SELECT Sura_id , Aya_id FROM QuranHefz ;";

        Cursor cursor=mydatabase.rawQuery(stat,null);

        while (cursor.moveToNext()){
            int aya=cursor.getInt(1);
            int sura=cursor.getInt(0);

            if (hefz.containsKey(sura)){
                hefz.get(sura).add(aya);
            }else {
                hefz.put(sura,new HashSet<Integer>());
                hefz.get(sura).add(aya);
            }
        }

        return hefz;
    }




    public long InsertZaker(int value,int count,Long date)
    {
        ContentValues contentValues=new ContentValues();
        contentValues.put("value",value);
        contentValues.put("num",count);
        contentValues.put("date",date);
        return mydatabase.insert("azkar",null,contentValues);
    }
    public long Total_zaker(int value)
    {
        String s="select value , num " +
                "FROM azkar ;" ;
        Cursor cursor=mydatabase.rawQuery(s,null);
        int total=0;
        for (cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
        {
            long num=cursor.getInt(1);
            int v=cursor.getInt(0);
            if(v==value) total+=num;
        }
        return total;
    }
    public ArrayList<Long>All_Azkar(long from ,long to)
    {
        String s="select value , num " +
                "FROM azkar " +
                "WHERE date >= "+from+
                " AND date <= "
                +to+
                ";";

        ArrayList<Long> arrayList=new ArrayList<>();
        for(int i=0;i<7;i++)
        {
            arrayList.add(0l);
        }
        Cursor cursor=mydatabase.rawQuery(s,null);
        for (cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
        {
            long total=cursor.getLong(1);
            int v=cursor.getInt(0);
            arrayList.set(v,arrayList.get(v)+total);
        }
        return arrayList;
    }
    public long InsertSalahFard(int fagr,int dahr,int asr,int maghrb,int isha,long date)
    {
        String s= "select fagr FROM SalahFard WHERE date = " +date+" ;";
        Cursor cursor=mydatabase.rawQuery(s,null);
        Log.v("testl",cursor.getCount()+" ");
        try {
            cursor.moveToFirst();
            int f = cursor.getInt(0);
            Log.v("test",f+" ");
            s="UPDATE SalahFard "+
                    "SET fagr = " +fagr+
                    ", dahr = " +dahr+
                    ", asr = " +asr+
                    ", maghrb = " +maghrb+
                    ", isha = "+isha+
                    " WHERE date = "+date+" ;";
            mydatabase.execSQL(s);
        }
        catch (Exception e)
        {
            ContentValues contentValues=new ContentValues();
            contentValues.put("fagr",fagr);
            contentValues.put("dahr",dahr);
            contentValues.put("asr",asr);
            contentValues.put("maghrb",maghrb);
            contentValues.put("isha",isha);
            contentValues.put("date",date);

            Log.v("testl", e.getMessage()+ "");
            mydatabase.insert("SalahFard",null,contentValues);

        }
        Cursor cursor1=mydatabase.rawQuery("select fagr, dahr, asr, maghrb, isha FROM SalahFard ;",null);
        Log.v( "testcount",cursor1.getCount()+"♦♥◘•○♠♦☺☻♥");
        return -1;
    }
    public long InsertSalahSann(int fagr,int dahr,int maghrb,int isha,long date)
    {
        String s= "select fagr FROM SalahSann WHERE date =" +date;
        Cursor cursor=mydatabase.rawQuery(s,null);
        try {
            cursor.moveToFirst();
            int f = cursor.getInt(0);
            Log.v("test",f+" ");
            s="UPDATE SalahSann "+
                    "SET fagr = " +fagr+
                    ", dahr = " +dahr+
                    ", maghrb = " +maghrb+
                    ", isha = "+isha+
                    " WHERE date = "+date+" ;";
            mydatabase.execSQL(s);
        }
        catch (Exception e)
        {
            ContentValues contentValues=new ContentValues();
            contentValues.put("fagr",fagr);
            contentValues.put("dahr",dahr);
            contentValues.put("maghrb",maghrb);
            contentValues.put("isha",isha);
            contentValues.put("date",date);
            return mydatabase.insert("SalahSann",null,contentValues);
            //  Log.v("test", e.getMessage()+ "");

        }
        return -1;
    }
    public long getTotalOfSann(long from,long to)
    {
        String query="SELECT fagr, dahr, maghrb, isha FROM SalahSann WHERE date >= "+from+" AND date <= "+to+ " ;";
        Cursor cursor=mydatabase.rawQuery(query,null);
        Log.v("test", cursor.getCount()+" ");
        int count=0;
        try
        {
            for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
            {
                int f=cursor.getInt(0);
                int d=cursor.getInt(1);
                int m=cursor.getInt(2);
                int isha=cursor.getInt(3);
                count+=f+d+m+isha;
            }
            return count;
        }
        catch (Exception e)
        {
            Log.v("test",e.getMessage());
        }
        return 0;
    }
    public long InsertNawafl(int dahha,int watr,long date)
    {
        String s= "select dahha FROM SalahNawafel WHERE date =" +date;
        Cursor cursor=mydatabase.rawQuery(s,null);
        try {
            cursor.moveToFirst();
            int f = cursor.getInt(0);
            Log.v("test",f+" ");
            s="UPDATE SalahNawafel "+
                    "SET dahha = " +dahha+
                    ", watr = " +watr+
                    " WHERE date = "+date+" ;";
            mydatabase.execSQL(s);
        }
        catch (Exception e)
        {
            ContentValues contentValues=new ContentValues();
            contentValues.put("dahha",dahha);
            contentValues.put("watr",watr);
            contentValues.put("date",date);
            return mydatabase.insert("SalahNawafel",null,contentValues);
            //  Log.v("test", e.getMessage()+ "");
        }
        return -1;
    }
    public long getTotalNawafl(long from,long to)
    {
        String query="SELECT dahha, watr FROM SalahNawafel WHERE date <= "+from+" AND date >= "+to+" ;";
        Cursor cursor=mydatabase.rawQuery(query,null);
        Log.v("test", cursor.getCount()+" ");
        int count=0;
        try
        {
            for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
            {
                int f=cursor.getInt(0);
                int d=cursor.getInt(1);
                count+=f+d;
            }
            return count;
        }
        catch (Exception e)
        {
            Log.v("test",e.getMessage());
        }
        return 0;
    }
    public long getNumberOfFardInMosque(long from,long to)
    {
        String s="select fagr, dahr, asr, maghrb, isha FROM SalahFard WHERE date >= "+from+" AND date <= "+to+" ;";
        Cursor cursor=mydatabase.rawQuery(s,null);
        Log.v("test", cursor.getCount()+" ");
        int count=0;
        try
        {
            for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
            {
                Log.v("test",cursor.getInt(0)+" "+cursor.getInt(1));
                // count++;
                if(cursor.getInt(0)==2)count++;
                if(cursor.getInt(1)==2)count++;
                if(cursor.getInt(2)==2)count++;
                if(cursor.getInt(3)==2)count++;
                if(cursor.getInt(4)==2)count++;
            }
            return count;
        }
        catch (Exception e)
        {
            Log.v("test",e.getMessage());
        }
        return 0;
    }
    public long getNumberOfFardOutMosque(long from,long to)
    {
        String s="select fagr, dahr, asr, maghrb, isha FROM SalahFard WHERE date >= "+from+" AND date <= "+to+" ;";
        Cursor cursor=mydatabase.rawQuery(s,null);
        Log.v("test", cursor.getCount()+" ");
        int count=0;
        try
        {
            for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
            {
                Log.v("test",cursor.getInt(0)+" "+cursor.getInt(1));
                // count++;
                if(cursor.getInt(0)==1)count++;
                if(cursor.getInt(1)==1)count++;
                if(cursor.getInt(2)==1)count++;
                if(cursor.getInt(3)==1)count++;
                if(cursor.getInt(4)==1)count++;
            }
            return count;
        }
        catch (Exception e)
        {
            Log.v("test",e.getMessage());
        }
        return 0;
    }
    public long InsertZakah(double value,int type,int year)
    {
        String s="select value FROM Zakah WHERE year = "+year+" AND type = "+type+" ;";
        int count=0;
        Cursor cursor=mydatabase.rawQuery(s,null);
        count=cursor.getCount();
        if(count==0)
        {
            ContentValues contentValues=new ContentValues();
            contentValues.put("value",value);
            contentValues.put("type",type);
            contentValues.put("year",year);
            return mydatabase.insert("Zakah",null,contentValues);
        }
        else
        {
            String query="UPDATE Zakah SET "+
                    " value = " +value+
                    " ,type = " +type+
                    " WHERE year = "+year+" ;";
            mydatabase.rawQuery(query,null);
        }
        return -1;
    }

    public ArrayList<Double>SelectZakah(int year)
    {
        ArrayList<Double>arrayList=new ArrayList<>();
        for(int i=0;i<6;i++)arrayList.add(0.0);
        String s="SELECT value , type FROM Zakah WHERE year = "+year+" ;";
        Cursor cursor=mydatabase.rawQuery(s,null);
        if(cursor.getCount()==0)return arrayList;
        for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
        {
            double d=cursor.getDouble(0);
            int index=cursor.getInt(1);
            arrayList.set(index,d);
        }
        return arrayList;
    }
    public ArrayList<Double>SelectAllZakah()
    {
        ArrayList<Double>arrayList=new ArrayList<>();
        for(int i=0;i<6;i++)arrayList.add(0.0);
        String s="SELECT value , type FROM Zakah  ;";
        Cursor cursor=mydatabase.rawQuery(s,null);
        if(cursor.getCount()==0)return arrayList;
        for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext())
        {
            double d=cursor.getDouble(0);
            int index=cursor.getInt(1);
            arrayList.set(index,arrayList.get(index)+d);
        }
        return arrayList;
    }




}
